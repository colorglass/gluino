#include <vector>

#include <gluino/synchronized.h>
#include <gtest/gtest.h>

using namespace gluino;

using synchronized_vector = synchronized<std::vector<int>>;

TEST(synchronized, inheritance) {
    synchronized_vector vect{1, 2, 3};
    vect.reserve(10);
    EXPECT_EQ(false, vect.empty());
    EXPECT_EQ(true, vect.try_lock());
    vect.unlock();
}

