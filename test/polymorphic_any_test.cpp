#include <gluino/polymorphic_any.h>
#include <gluino/utility.h>
#include <gtest/gtest.h>

using namespace gluino;

namespace {
    struct Parent {
        virtual ~Parent() = default;
    };

    struct Child : public Parent {
    };

    struct OtherChild : public Parent {
    };

    struct OtherParent {
        virtual ~OtherParent() = default;
    };

    struct Static {
    };
}

TEST(polymorphic_any, static_create) {
    polymorphic_any val(20.0f);
    EXPECT_FALSE(val.is_polymorphic());
    EXPECT_THROW(discard(std::any_cast<int32_t>(val)), std::bad_any_cast);
    EXPECT_EQ(20.0f, std::any_cast<float_t>(val));
}

TEST(polymorphic_any, static_assign) {
    polymorphic_any val = 20.0f;
    EXPECT_FALSE(val.is_polymorphic());
    EXPECT_THROW(discard(std::any_cast<int32_t>(val)), std::bad_any_cast);
    EXPECT_EQ(20.0f, std::any_cast<float_t>(val));
}

TEST(polymorphic_any, polymorphic_create) {
    auto* child = new Child();
    polymorphic_any val(child);
    EXPECT_TRUE(val.is_polymorphic());
    EXPECT_THROW(discard(std::any_cast<int32_t>(val)), std::bad_any_cast);
    EXPECT_TRUE(child == std::any_cast<Child*>(val).get_raw());
    EXPECT_TRUE(child == std::any_cast<Parent*>(val).get_raw());
    EXPECT_TRUE(std::any_cast<OtherParent*>(val).is_null());
}
