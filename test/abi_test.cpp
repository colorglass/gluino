#include <gluino/abi/abi.h>
#include <gtest/gtest.h>

using namespace gluino;
using namespace gluino::abi;

namespace {
    class A {
    public:
        virtual ~A() = default;
    };

    struct B {
        virtual ~B() = default;
    };

    class C {
    public:
        virtual ~C() = default;
    };

    class D {
    };

    class E : private A, protected B {
    };

    class F : public C, public D {
    };

    class G : public E, public F {
    };
}

TEST(abi, class_info) {
    G g;
    typeid(E).name();
    typeid(F).name();
    auto info = class_info::from(g);
    EXPECT_EQ(typeid(g), info.type_descriptor());
    EXPECT_EQ(typeid(E), info.base_classes()[0].base_class().type_descriptor());
    EXPECT_EQ(typeid(A), info.base_classes()[0].base_class().base_classes()[0].base_class().type_descriptor());
    EXPECT_EQ(typeid(B), info.base_classes()[0].base_class().base_classes()[1].base_class().type_descriptor());
    EXPECT_EQ(typeid(F), info.base_classes()[1].base_class().type_descriptor());
    EXPECT_EQ(typeid(C), info.base_classes()[1].base_class().base_classes()[0].base_class().type_descriptor());
    EXPECT_EQ(typeid(D), info.base_classes()[1].base_class().base_classes()[1].base_class().type_descriptor());
}
