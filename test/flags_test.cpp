#include <gluino/flags.h>
#include <gtest/gtest.h>

using namespace gluino;

namespace {
    gluino_flags(test_flags, uint16_t,
                 (flag1, 1 << 0),
                 (flag2, 1 << 1),
                 (flag3, 1 << 2)
    );
}

TEST(flags, base_type) {
    static_assert(std::is_same_v<std::underlying_type_t<test_flags::enum_type>, uint16_t>);
    auto match = std::is_same_v<std::underlying_type_t<test_flags::enum_type>, uint16_t>;
    EXPECT_EQ(true, match);
}

TEST(flags, get_by_name) {
    EXPECT_EQ(test_flags("flag1"), test_flags::flag1);
    EXPECT_EQ(test_flags("flag2"), test_flags::flag2);
    EXPECT_EQ(test_flags("flag3"), test_flags::flag3);
    EXPECT_EQ(test_flags("flag2", "flag3"), test_flags::flag2 + test_flags::flag3);
}

TEST(flags, get_by_bad_name) {
    EXPECT_THROW(test_flags("bad"), std::invalid_argument);
}

TEST(flags, get_by_value) {
    EXPECT_EQ(test_flags(static_cast<uint16_t>(1 << 0)), test_flags::flag1);
    EXPECT_EQ(test_flags(static_cast<uint16_t>(1 << 1)), test_flags::flag2);
    EXPECT_EQ(test_flags(static_cast<uint16_t>(1 << 2)), test_flags::flag3);
    EXPECT_EQ(test_flags(static_cast<uint16_t>((1 << 1) | (1 << 2))), test_flags::flag2 + test_flags::flag3);
}

TEST(flags, get_by_bad_value) {
    EXPECT_THROW(test_flags(static_cast<uint16_t>(1 << 4)), std::invalid_argument);
}

TEST(flags, get_by_enum) {
    EXPECT_EQ(test_flags(test_flags::enum_type::flag1), test_flags::flag1);
    EXPECT_EQ(test_flags(test_flags::enum_type::flag2), test_flags::flag2);
    EXPECT_EQ(test_flags(test_flags::enum_type::flag3), test_flags::flag3);
    EXPECT_EQ(test_flags(static_cast<test_flags::enum_type>(static_cast<uint16_t>((1 << 1) | (1 << 2)))),
              test_flags::flag2 + test_flags::flag3);
}

TEST(flags, get_by_bad_enum) {
    EXPECT_THROW(test_flags(static_cast<test_flags::enum_type>(1 << 4)), std::invalid_argument);
}

TEST(flags, get_by_multiple_types) {
    EXPECT_EQ(test_flags(test_flags::enum_type::flag1, static_cast<uint16_t>(1 << 1), "flag3"),
              test_flags::flag1 + test_flags::flag2 + test_flags::flag3);
}

TEST(flags, get_names) {
    auto expected = std::vector<std::string_view>{"flag1", "flag2", "flag3"};
    EXPECT_EQ(test_flags(test_flags::enum_type::flag1, static_cast<uint16_t>(1 << 1), "flag3").names(), expected);
}

TEST(flags, get_set_flags) {
    auto expected = std::vector<test_flags>{test_flags::flag1, test_flags::flag3};
    EXPECT_EQ((test_flags::flag1 + test_flags::flag3).set_flags(), expected);
}

TEST(flags, get_values) {
    auto iter = test_flags::values().begin();
    EXPECT_EQ(*iter++, test_flags::flag1);
    EXPECT_EQ(*iter++, test_flags::flag2);
    EXPECT_EQ(*iter++, test_flags::flag3);
    EXPECT_EQ(iter, test_flags::values().end());
}

TEST(flags, enum_and) {
    EXPECT_EQ(test_flags::flag1 & test_flags::flag2,
              static_cast<test_flags::enum_type>(static_cast<uint16_t>(test_flags::flag1) &
                                                 static_cast<uint16_t>(test_flags::flag2)));
}

TEST(flags, enum_or) {
    EXPECT_EQ(test_flags::flag1 | test_flags::flag2,
              static_cast<test_flags::enum_type>(static_cast<uint16_t>(test_flags::flag1) |
                                                 static_cast<uint16_t>(test_flags::flag2)));
}

TEST(flags, enum_plus) {
    EXPECT_EQ(test_flags::flag1 + test_flags::flag2,
              static_cast<test_flags::enum_type>(static_cast<uint16_t>(test_flags::flag1) |
                                                 static_cast<uint16_t>(test_flags::flag2)));
}

TEST(flags, enum_minus) {
    EXPECT_EQ(test_flags::flag1 - test_flags::flag2,
              static_cast<test_flags::enum_type>(static_cast<uint16_t>(test_flags::flag1) &
                                                 ~static_cast<uint16_t>(test_flags::flag2)));
}

TEST(flags, enum_not) {
    EXPECT_TRUE((~test_flags::flag1) == (test_flags::flag2 + test_flags::flag3));
}

TEST(flags, flags_and_enum) {
    EXPECT_EQ(test_flags(test_flags::flag1) & test_flags::enum_type::flag2,
              test_flags(static_cast<test_flags::enum_type>(static_cast<uint16_t>(test_flags::flag1) &
                                                            static_cast<uint16_t>(test_flags::flag2))));
}

TEST(flags, flags_and_flags) {
    EXPECT_EQ(test_flags(test_flags::flag1) & test_flags(test_flags::enum_type::flag2),
              test_flags(static_cast<test_flags::enum_type>(static_cast<uint16_t>(test_flags::flag1) &
                                                            static_cast<uint16_t>(test_flags::flag2))));
}

TEST(flags, flags_or_enum) {
    EXPECT_EQ(test_flags(test_flags::flag1) | test_flags::enum_type::flag2,
              test_flags(static_cast<test_flags::enum_type>(static_cast<uint16_t>(test_flags::flag1) |
                                                            static_cast<uint16_t>(test_flags::flag2))));
}

TEST(flags, flags_or_flags) {
    EXPECT_EQ(test_flags(test_flags::flag1) | test_flags(test_flags::enum_type::flag2),
              test_flags(static_cast<test_flags::enum_type>(static_cast<uint16_t>(test_flags::flag1) |
                                                            static_cast<uint16_t>(test_flags::flag2))));
}

TEST(flags, flags_plus_enum) {
    EXPECT_EQ(test_flags(test_flags::flag1) + test_flags::enum_type::flag2,
              test_flags(static_cast<test_flags::enum_type>(static_cast<uint16_t>(test_flags::flag1) |
                                                            static_cast<uint16_t>(test_flags::flag2))));
}

TEST(flags, flags_plus_flags) {
    EXPECT_EQ(test_flags(test_flags::flag1) + test_flags(test_flags::enum_type::flag2),
              test_flags(static_cast<test_flags::enum_type>(static_cast<uint16_t>(test_flags::flag1) |
                                                            static_cast<uint16_t>(test_flags::flag2))));
}

TEST(flags, flags_minus_enum) {
    EXPECT_EQ(test_flags(test_flags::flag1) - test_flags::enum_type::flag2,
              test_flags(static_cast<test_flags::enum_type>(static_cast<uint16_t>(test_flags::flag1) &
                                                            ~static_cast<uint16_t>(test_flags::flag2))));
}

TEST(flags, flags_minus_flags) {
    EXPECT_EQ(test_flags(test_flags::flag1) - test_flags(test_flags::enum_type::flag2),
              test_flags(static_cast<test_flags::enum_type>(static_cast<uint16_t>(test_flags::flag1) &
                                                            ~static_cast<uint16_t>(test_flags::flag2))));
}

TEST(flags, flags_all) {
    EXPECT_EQ(test_flags(test_flags::flag1, test_flags::flag2).all(test_flags::flag1), true);
    EXPECT_EQ(test_flags(test_flags::flag1, test_flags::flag2).all(test_flags::flag2), true);
    EXPECT_EQ(test_flags(test_flags::flag1, test_flags::flag2).all(test_flags::flag1, test_flags::flag2), true);
    EXPECT_EQ(test_flags(test_flags::flag1, test_flags::flag2).all(test_flags::flag3), false);
    EXPECT_EQ(test_flags(test_flags::flag1, test_flags::flag2).all(test_flags::flag1, test_flags::flag3), false);
}

TEST(flags, flags_any) {
    EXPECT_EQ(test_flags(test_flags::flag1, test_flags::flag2).any(test_flags::flag1), true);
    EXPECT_EQ(test_flags(test_flags::flag1, test_flags::flag2).any(test_flags::flag2), true);
    EXPECT_EQ(test_flags(test_flags::flag1, test_flags::flag2).any(test_flags::flag1, test_flags::flag2), true);
    EXPECT_EQ(test_flags(test_flags::flag1, test_flags::flag2).any(test_flags::flag3), false);
    EXPECT_EQ(test_flags(test_flags::flag1, test_flags::flag2).any(test_flags::flag1, test_flags::flag3), true);
}

TEST(flags, flags_none) {
    EXPECT_EQ(test_flags(test_flags::flag1, test_flags::flag2).none(test_flags::flag1), false);
    EXPECT_EQ(test_flags(test_flags::flag1, test_flags::flag2).none(test_flags::flag2), false);
    EXPECT_EQ(test_flags(test_flags::flag1, test_flags::flag2).none(test_flags::flag1, test_flags::flag2), false);
    EXPECT_EQ(test_flags(test_flags::flag1, test_flags::flag2).none(test_flags::flag3), true);
    EXPECT_EQ(test_flags(test_flags::flag1, test_flags::flag2).none(test_flags::flag1, test_flags::flag3), false);
}

TEST(flags, copy_constructible) {
    EXPECT_EQ(test_flags(test_flags::flag1), test_flags::flag1);
}

TEST(flags, move_constructible) {
    test_flags source(test_flags::flag1);
    EXPECT_EQ(test_flags(::std::move(source)), test_flags::flag1);
    EXPECT_EQ(source, test_flags());
}

TEST(flags, copy_assignable) {
    test_flags source(test_flags::flag1);
    test_flags destination;
    destination = source;
    EXPECT_EQ(destination, test_flags::flag1);
    EXPECT_EQ(source, destination);
}

TEST(flags, move_assignable) {
    test_flags source(test_flags::flag1);
    test_flags destination;
    destination = ::std::move(source);
    EXPECT_EQ(destination, test_flags::flag1);
    EXPECT_EQ(source, test_flags());
}
