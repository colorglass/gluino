#define GLUINO_WCHAR_MIN_FAST_FIND 36

#include <gluino/case_insensitive_string_view.h>
#include <gtest/gtest.h>

using namespace gluino;

TEST(case_insensitive_string_view, equality) {
    EXPECT_EQ(case_insensitive_string_view("TEST"), "TeSt");
}

TEST(case_insensitive_string_view, contains) {
    EXPECT_TRUE(case_insensitive_string_view("xx TEST xx").contains("TeSt"));
}

TEST(case_insensitive_string_view, starts_with) {
    EXPECT_TRUE(case_insensitive_string_view("xx TEST yy").starts_with("XX"));
}

TEST(case_insensitive_string_view, ends_with) {
    EXPECT_TRUE(case_insensitive_string_view("xx TEST yy").ends_with("YY"));
}

TEST(case_insensitive_string_view, basic_find) {
    EXPECT_EQ(3, case_insensitive_string_view("xx TEST TEST yy").find("TeSt"));
}

TEST(case_insensitive_string_view, basic_rfind) {
    EXPECT_EQ(8, case_insensitive_string_view("xx TEST TEST yy").rfind("TeSt"));
}

TEST(case_insensitive_string_view, advanced_find) {
    EXPECT_EQ(3, case_insensitive_string_view("xx TESTTESTTESTTESTTESTTESTTESTTESTTEST TEST yy")
                      .find("TeStTeStTeStTeStTeStTeStTeStTeStTeSt"));
}

TEST(case_insensitive_string_view, advanced_rfind) {
    EXPECT_EQ(8, case_insensitive_string_view("xx TEST TESTTESTTESTTESTTESTTESTTESTTESTTEST yy")
                      .rfind("TeStTeStTeStTeStTeStTeStTeStTeStTeSt"));
}

TEST(case_insensitive_string_view, advanced_hash_find) {
    EXPECT_EQ(3, case_insensitive_wstring_view(L"xx TESTTESTTESTTESTTESTTESTTESTTESTTESTTEST TEST yy")
                     .find(L"TeStTeStTeStTeStTeStTeStTeStTeStTeStTeSt"));
}

TEST(case_insensitive_string_view, advanced_hash_rfind) {
    EXPECT_EQ(8, case_insensitive_wstring_view(L"xx TEST TESTTESTTESTTESTTESTTESTTESTTESTTESTTEST yy")
                     .rfind(L"TeStTeStTeStTeStTeStTeStTeStTeStTeStTeSt"));
}
