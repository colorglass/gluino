#include <thread>

#include <gluino/null_mutex.h>
#include <gtest/gtest.h>

using namespace gluino;

TEST(null_mutex, never_blocks) {
    null_mutex mutex;
    mutex.lock();

    std::thread other_thread([&mutex]() {
        mutex.lock();
    });
    other_thread.join();
}

TEST(null_mutex, try_lock_always_true) {
    null_mutex mutex;
    EXPECT_EQ(true, mutex.try_lock());
    EXPECT_EQ(true, mutex.try_lock());

    std::thread other_thread([&mutex]() {
        EXPECT_EQ(true, mutex.try_lock());
    });
    other_thread.join();
}

TEST(null_mutex, unlock_always_succeeds) {
    null_mutex mutex;
    mutex.lock();
    mutex.unlock();
    mutex.unlock();

    std::thread other_thread([&mutex]() {
        mutex.unlock();
    });
    other_thread.join();
}
