#include <gluino/autorun.h>
#include <gtest/gtest.h>

namespace {
    int testx{0};
    int testy{0};

    gluino_autorun {
        testx = 20;
    };

    gluino_autorun {
        testy = 40;
    };
}

TEST(autorun, constructor) {
    EXPECT_EQ(testx, 20);
}

TEST(autorun, macro) {
    EXPECT_EQ(testy, 40);
}