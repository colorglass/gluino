#include <mutex>

#include <gluino/spin_mutex.h>
#include <gtest/gtest.h>

using namespace gluino;

TEST(spin_mutex, mutex) {
    spin_mutex<> mutex;
    volatile int value = 1;
    std::thread t([&]() {
        std::unique_lock lock(mutex);
        std::this_thread::sleep_for(std::chrono::milliseconds(20));
        value = 0;
    });
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    mutex.lock();
    EXPECT_EQ(value, 0);
    t.join();
}

TEST(spin_mutex, try_lock) {
    spin_mutex<> mutex;
    volatile int value = 1;
    std::thread t([&]() {
        std::unique_lock lock(mutex);
        std::this_thread::sleep_for(std::chrono::milliseconds(20));
        value = 0;
    });
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    EXPECT_FALSE(mutex.try_lock());
    std::this_thread::sleep_for(std::chrono::milliseconds(30));
    EXPECT_TRUE(mutex.try_lock());
    t.join();
}
