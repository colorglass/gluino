#include <string>

#include <gluino/extension.h>
#include <gtest/gtest.h>

using namespace gluino;

namespace {
    struct functor {
        [[nodiscard]] typename std::string::size_type operator()(const std::string& self) const noexcept {
            return self.size();
        }

        [[nodiscard]] std::string operator()(const std::string& self, const ::std::string& other,
                const std::string& third) const {
            std::string new_string = self + other;
            new_string += third;
            return new_string;
        }
    };

    constexpr extension<functor> ext;
    constexpr extension<functor, extension_ops::pipe> pipe_ext;
    constexpr extension<functor, extension_ops::double_pipe> double_pipe_ext;
    constexpr extension<functor, extension_ops::forward_slash> forward_slash_ext;
    constexpr extension<functor, extension_ops::shift_left> shift_left_ext;
    constexpr extension<functor, extension_ops::shift_right> shift_right_ext;
    constexpr extension<functor, extension_ops::shift_right + extension_ops::shift_left> shift_ext;
}

TEST(extension, invokes_extension) {
    std::string str = "Hello, World";

    EXPECT_EQ(str.size(), str->*ext());
}

TEST(extension, forwards_args) {
    std::string str1 = "Hello";
    std::string str2 = ", ";
    std::string str3 = "World";

    EXPECT_EQ(str1 + str2 + str3, str1->*ext(str2, str3));
}

TEST(extension, operator_pipe) {
    std::string str = "Hello, World";

    EXPECT_EQ(str.size(), str | pipe_ext());
}

TEST(extension, operator_double_pipe) {
    std::string str = "Hello, World";

    EXPECT_EQ(str.size(), str || double_pipe_ext());
}

TEST(extension, operator_forward_slash) {
    std::string str = "Hello, World";

    EXPECT_EQ(str.size(), str / forward_slash_ext());
}

TEST(extension, operator_shift_left) {
    std::string str = "Hello, World";

    EXPECT_EQ(str.size(), str << shift_left_ext());
}

TEST(extension, operator_shift_right) {
    std::string str = "Hello, World";

    EXPECT_EQ(str.size(), str >> shift_right_ext());
}

TEST(extension, multiple_operators) {
    std::string str = "Hello, World";

    EXPECT_EQ(str.size(), str >> shift_ext());
    EXPECT_EQ(str.size(), str << shift_ext());
}
