#pragma warning(push)
#pragma warning(disable: 4127)
#include <pcg_random.hpp>

#include <gluino/random_engine.h>
#include <gtest/gtest.h>

using namespace gluino;

TEST(random_engine, range_check_int) {
    random_engine engine;
    for (int i = 0; i < 1000; i++) {
        auto result = engine.next(0, 1);
        EXPECT_LE(result, 1);
        EXPECT_GE(result, 0);
    }
}

TEST(random_engine, range_check_float) {
    random_engine engine;
    for (int i = 0; i < 1000; i++) {
        auto result = engine.next(0.0f, 1.0f);
        EXPECT_LE(result, 1.0f);
        EXPECT_GE(result, 0.0f);
    }
}
#pragma warning(pop)
