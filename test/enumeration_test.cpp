#include <gluino/enumeration.h>
#include <gtest/gtest.h>

namespace {
    gluino_enum(test_enum, uint16_t,
                (first, 2),
                (second, 4),
                (third, 5));
}

TEST(enumeration, default_value) {
    test_enum x;
    EXPECT_EQ(x, test_enum::first);
}

TEST(enumeration, underlying_type) {
    static_assert(std::is_same_v<typename test_enum::underlying_type, uint16_t>);
    static_assert(std::is_same_v<std::underlying_type_t<typename test_enum::enum_type>, uint16_t>);
}

TEST(enumeration, get_name) {
    EXPECT_EQ(test_enum::first.name(), "first");
    EXPECT_EQ(test_enum::second.name(), "second");
    EXPECT_EQ(test_enum::third.name(), "third");
}

TEST(enumeration, get_by_underlying_value) {
    EXPECT_EQ(test_enum(2), test_enum::first);
    EXPECT_EQ(test_enum(4), test_enum::second);
    EXPECT_EQ(test_enum(5), test_enum::third);
}

TEST(enumeration, get_by_bad_underlying_value) {
    EXPECT_THROW(test_enum(0), std::invalid_argument);
}

TEST(enumeration, get_by_enum) {
    EXPECT_EQ(test_enum(test_enum::enum_type::first), test_enum::first);
    EXPECT_EQ(test_enum(test_enum::enum_type::second), test_enum::second);
    EXPECT_EQ(test_enum(test_enum::enum_type::third), test_enum::third);
}

TEST(enumeration, get_by_bad_enum) {
    EXPECT_THROW(test_enum(static_cast<test_enum::enum_type>(0)), std::invalid_argument);
}

TEST(enumeration, get_by_name) {
    EXPECT_EQ(test_enum("first"), test_enum::first);
    EXPECT_EQ(test_enum("second"), test_enum::second);
    EXPECT_EQ(test_enum("third"), test_enum::third);
}

TEST(enumeration, get_by_bad_name) {
    EXPECT_THROW(test_enum("bad"), std::invalid_argument);
}

TEST(enumeration, values) {
    auto iter = test_enum::values().begin();
    EXPECT_EQ(*iter++, test_enum::first);
    EXPECT_EQ(*iter++, test_enum::second);
    EXPECT_EQ(*iter++, test_enum::third);
    EXPECT_EQ(iter, test_enum::values().end());
}

TEST(enumeration, current_iterator) {
    auto iter = test_enum::second.current();
    EXPECT_EQ(*(iter - 1), test_enum::first);
    EXPECT_EQ(*iter, test_enum::second);
    EXPECT_EQ(*(iter + 1), test_enum::third);
}

TEST(enumeration, next) {
    EXPECT_EQ(test_enum::second.next().value(), test_enum::third);
    EXPECT_FALSE(test_enum::third.next().has_value());
}

TEST(enumeration, previous) {
    EXPECT_EQ(test_enum::second.previous().value(), test_enum::first);
    EXPECT_FALSE(test_enum::first.previous().has_value());
}

TEST(enumeration, copy_constructible) {
    EXPECT_EQ(test_enum(test_enum::first), test_enum::first);
}

TEST(enumeration, move_constructible) {
    test_enum source(test_enum::second);
    EXPECT_EQ(test_enum(::std::move(source)), test_enum::second);
    EXPECT_EQ(source, test_enum());
}

TEST(enumeration, copy_assignable) {
    test_enum source(test_enum::second);
    test_enum destination;
    destination = source;
    EXPECT_EQ(destination, test_enum::second);
    EXPECT_EQ(source, destination);
}

TEST(enumeration, move_assignable) {
    test_enum source(test_enum::second);
    test_enum destination;
    destination = ::std::move(source);
    EXPECT_EQ(destination, test_enum::second);
    EXPECT_EQ(source, test_enum());
}
