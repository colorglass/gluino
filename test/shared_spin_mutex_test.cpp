#include <shared_mutex>

#include <gluino/container.h>
#include <gluino/shared_spin_mutex.h>
#include <gtest/gtest.h>

using namespace gluino;

TEST(shared_spin_mutex, mutex) {
    shared_spin_mutex<> mutex;
    volatile int value = 1;
    std::thread t([&]() {
        std::unique_lock lock(mutex);
        std::this_thread::sleep_for(std::chrono::milliseconds(20));
        value = 0;
    });
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    mutex.lock();
    EXPECT_EQ(value, 0);
    t.join();
}

TEST(shared_spin_mutex, try_lock) {
    shared_spin_mutex<> mutex;
    volatile int value = 1;
    std::thread t([&]() {
        std::unique_lock lock(mutex);
        std::this_thread::sleep_for(std::chrono::milliseconds(20));
        value = 0;
    });
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    EXPECT_FALSE(mutex.try_lock());
    std::this_thread::sleep_for(std::chrono::milliseconds(30));
    EXPECT_TRUE(mutex.try_lock());
    t.join();
}

TEST(shared_spin_mutex, multiple_readers) {
    shared_spin_mutex<> mutex;
    volatile int value = 0;
    std::thread t1([&]() {
        EXPECT_EQ(++value, 1);
        std::shared_lock lock(mutex);
        std::this_thread::sleep_for(std::chrono::milliseconds(20));
        --value;
    });
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    std::thread t2([&]() {
        EXPECT_EQ(++value, 2);
        std::shared_lock lock(mutex);
        std::this_thread::sleep_for(std::chrono::milliseconds(20));
        --value;
    });
    std::this_thread::sleep_for(std::chrono::milliseconds(30));
    mutex.lock();
    EXPECT_EQ(value, 0);
    t1.join();
    t2.join();
}

TEST(shared_spin_mutex, writer_blocks_readers) {
    shared_spin_mutex<> mutex;
    volatile int value = 1;
    std::thread t([&]() {
        std::unique_lock lock(mutex);
        std::this_thread::sleep_for(std::chrono::milliseconds(20));
        value = 0;
    });
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    mutex.lock_shared();
    EXPECT_EQ(value, 0);
    t.join();
}
