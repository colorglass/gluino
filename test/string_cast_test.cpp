#include <type_traits>

#include <gluino/string_cast.h>
#include <gtest/gtest.h>

using namespace gluino;

// TODO: Google Test currently does not support char8_t in the Vcpkg builds, so those tests which invoke EXPECT_EQ are
// disabled. Reenable them when this is fixed in the Vcpkg port.

TEST(string_cast, char_to_char_yields_view) {
    auto result = string_cast<char>("Test");
    static_assert(std::is_same_v<std::string_view, decltype(result)>);
}

TEST(string_cast, wchar_to_wchar_yields_view) {
    auto result = string_cast<wchar_t>(L"Test");
    static_assert(std::is_same_v<std::wstring_view, decltype(result)>);
}

TEST(string_cast, char8_to_char8_yields_view) {
    auto result = string_cast<char8_t>(u8"Test");
    static_assert(std::is_same_v<std::u8string_view, decltype(result)>);
}

TEST(string_cast, char16_to_char16_yields_view) {
    auto result = string_cast<char16_t>(u"Test");
    static_assert(std::is_same_v<std::u16string_view, decltype(result)>);
}

TEST(string_cast, char32_to_char32_yields_view) {
    auto result = string_cast<char32_t>(U"Test");
    static_assert(std::is_same_v<std::u32string_view, decltype(result)>);
}

TEST(string_cast, char_to_char8_yields_view) {
    auto result = string_cast<char8_t>("Test");
    static_assert(std::is_same_v<std::u8string_view, decltype(result)>);
}

TEST(string_cast, char8_to_char_yields_view) {
    auto result = string_cast<char>(u8"Test");
    static_assert(std::is_same_v<std::string_view, decltype(result)>);
}

TEST(string_cast, wchar_to_char16_yields_view) {
    auto result = string_cast<char16_t>(L"Test");
    static_assert(std::is_same_v<std::u16string_view, decltype(result)>);
}

TEST(string_cast, char16_to_wchar_yields_view) {
    auto result = string_cast<wchar_t>(u"Test");
    static_assert(std::is_same_v<std::wstring_view, decltype(result)>);
}

TEST(string_cast, char_to_wchar_yields_string) {
    auto result = string_cast<wchar_t>("Test");
    static_assert(std::is_same_v<std::wstring, decltype(result)>);
}

TEST(string_cast, char_to_char16_yields_string) {
    auto result = string_cast<char16_t>("Test");
    static_assert(std::is_same_v<std::u16string, decltype(result)>);
}

TEST(string_cast, char_to_char32_yields_string) {
    auto result = string_cast<char32_t>("Test");
    static_assert(std::is_same_v<std::u32string, decltype(result)>);
}

TEST(string_cast, wchar_to_char_yields_string) {
    auto result = string_cast<char>(L"Test");
    static_assert(std::is_same_v<std::string, decltype(result)>);
}

TEST(string_cast, wchar_to_char8_yields_string) {
    auto result = string_cast<char8_t>(L"Test");
    static_assert(std::is_same_v<std::u8string, decltype(result)>);
}

TEST(string_cast, wchar_to_char32_yields_string) {
    auto result = string_cast<char32_t>(L"Test");
    static_assert(std::is_same_v<std::u32string, decltype(result)>);
}

TEST(string_cast, char16_to_char_yields_string) {
    auto result = string_cast<char>(u"Test");
    static_assert(std::is_same_v<std::string, decltype(result)>);
}

TEST(string_cast, char16_to_char8_yields_string) {
    auto result = string_cast<char8_t>(u"Test");
    static_assert(std::is_same_v<std::u8string, decltype(result)>);
}

TEST(string_cast, char16_to_char32_yields_string) {
    auto result = string_cast<char32_t>(u"Test");
    static_assert(std::is_same_v<std::u32string, decltype(result)>);
}

TEST(string_cast, char32_to_char_yields_string) {
    auto result = string_cast<char>(U"Test");
    static_assert(std::is_same_v<std::string, decltype(result)>);
}

TEST(string_cast, char32_to_char8_yields_string) {
    auto result = string_cast<char8_t>(U"Test");
    static_assert(std::is_same_v<std::u8string, decltype(result)>);
}

TEST(string_cast, char32_to_wchar_yields_string) {
    auto result = string_cast<wchar_t>(U"Test");
    static_assert(std::is_same_v<std::wstring, decltype(result)>);
}

TEST(string_cast, char32_to_char16_yields_string) {
    auto result = string_cast<char16_t>(U"Test");
    static_assert(std::is_same_v<std::u16string, decltype(result)>);
}

TEST(string_cast, ascii_char_to_char) {
    EXPECT_EQ(string_cast<char>("Test"), "Test");
}

TEST(string_cast, char_to_wchar) {
    EXPECT_EQ(string_cast<wchar_t>("Test"), L"Test");
}

//TEST(string_cast, ascii_char_to_char8) {
//    EXPECT_EQ(string_cast<char8_t>("Test"), u8"Test");
//}
//
//TEST(string_cast, unicode_char_to_char8) {
//    auto input = string_cast<char>(u8"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ");
//    EXPECT_EQ(string_cast<char8_t>(input), u8"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ");
//}

TEST(string_cast, ascii_char_to_char16) {
    EXPECT_EQ(string_cast<char16_t>("Test"), u"Test");
}

TEST(string_cast, unicode_char_to_char16) {
    auto input = string_cast<char>(u8"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ");
    EXPECT_EQ(string_cast<char16_t>(input), u"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ");
}

TEST(string_cast, ascii_char_to_char32) {
    EXPECT_EQ(string_cast<char32_t>("Test"), U"Test");
}

TEST(string_cast, unicode_char_to_char32) {
    auto input = string_cast<char>(u8"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ");
    EXPECT_EQ(string_cast<char32_t>(input), U"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ");
}

TEST(string_cast, wchar_to_char) {
    EXPECT_EQ(string_cast<char>(L"Test"), "Test");
}

TEST(string_cast, wchar_to_wchar) {
    EXPECT_EQ(string_cast<wchar_t>(L"Test"), L"Test");
}

//TEST(string_cast, ascii_wchar_to_char8) {
//    EXPECT_EQ(string_cast<char8_t>(L"Test"), u8"Test");
//}
//
//TEST(string_cast, unicode_wchar_to_char8) {
//    auto input = string_cast<wchar_t>(u8"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ");
//    EXPECT_EQ(string_cast<char8_t>(input), u8"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ");
//}

TEST(string_cast, ascii_wchar_to_char16) {
    EXPECT_EQ(string_cast<char16_t>(L"Test"), u"Test");
}

TEST(string_cast, unicode_wchar_to_char16) {
    auto input = string_cast<wchar_t>(u8"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ");
    EXPECT_EQ(string_cast<char16_t>(input), u"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ");
}

TEST(string_cast, ascii_wchar_to_char32) {
    EXPECT_EQ(string_cast<char32_t>(L"Test"), U"Test");
}

TEST(string_cast, unicode_wchar_to_char32) {
    auto input = string_cast<wchar_t>(u8"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ");
    EXPECT_EQ(string_cast<char32_t>(input), U"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ");
}

TEST(string_cast, char8_to_char) {
    EXPECT_EQ(string_cast<char>(u8"Test"), "Test");
}

TEST(string_cast, char8_to_wchar) {
    EXPECT_EQ(string_cast<wchar_t>(u8"Test"), L"Test");
}

//TEST(string_cast, char8_to_char8) {
//    EXPECT_EQ(string_cast<char8_t>(u8"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ"), u8"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ");
//}

TEST(string_cast, char8_to_char16_t) {
    EXPECT_EQ(string_cast<char16_t>(u8"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ"), u"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ");
}

TEST(string_cast, char8_to_char32_t) {
    EXPECT_EQ(string_cast<char32_t>(u8"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ"), U"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ");
}

TEST(string_cast, char16_to_char) {
    EXPECT_EQ(string_cast<char>(u"Test"), "Test");
}

TEST(string_cast, char16_to_wchar) {
    EXPECT_EQ(string_cast<wchar_t>(u"Test"), L"Test");
}

//TEST(string_cast, char16_to_char8) {
//    EXPECT_EQ(string_cast<char8_t>(u"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ"), u8"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ");
//}

TEST(string_cast, char16_to_char16_t) {
    EXPECT_EQ(string_cast<char16_t>(u"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ"), u"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ");
}

TEST(string_cast, char16_to_char32_t) {
    EXPECT_EQ(string_cast<char32_t>(u"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ"), U"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ");
}

TEST(string_cast, char32_to_char) {
    EXPECT_EQ(string_cast<char>(U"Test"), "Test");
}

TEST(string_cast, char32_to_wchar) {
    EXPECT_EQ(string_cast<wchar_t>(U"Test"), L"Test");
}

//TEST(string_cast, char32_to_char8) {
//    EXPECT_EQ(string_cast<char8_t>(U"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ"), u8"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ");
//}

TEST(string_cast, char32_to_char16_t) {
    EXPECT_EQ(string_cast<char16_t>(U"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ"), u"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ");
}

TEST(string_cast, char32_to_char32_t) {
    EXPECT_EQ(string_cast<char32_t>(U"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ"), U"Test ∮ Σὲ ᚻᛖ ⡌⠁ あ");
}
