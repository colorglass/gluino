#include <gluino/preprocessor.h>
#include <gtest/gtest.h>

TEST(preprocessor, first) {
    EXPECT_EQ(GLUINO_FIRST(1, 2, 3), 1);
}

TEST(preprocessor, rest) {
    std::vector value{GLUINO_REST(1, 2, 3)};
    std::vector expected{2, 3};
    EXPECT_EQ(value, expected);
}

TEST(preprocessor, empty) {
    EXPECT_TRUE(GLUINO_IS_EMPTY()(true, false));
    EXPECT_FALSE(GLUINO_IS_EMPTY(1)(true, false));
}

TEST(preprocessor, true_macro) {
    EXPECT_TRUE(GLUINO_TRUE(true, false));
}

TEST(preprocessor, false_macro) {
    EXPECT_FALSE(GLUINO_FALSE(true, false));
}

TEST(preprocessor, flatten) {
    std::vector value{GLUINO_FLATTEN((1, 2))};
    std::vector expected{1, 2};
    EXPECT_EQ(value, expected);
}

TEST(preprocessor, if_macro) {
    auto value = true;
    GLUINO_IF(GLUINO_FALSE, value = false;)
    EXPECT_TRUE(value);
    GLUINO_IF(GLUINO_TRUE, value = false;)
    EXPECT_FALSE(value);
}

TEST(preprocessor, if_else_macro) {
    EXPECT_EQ(GLUINO_IF_ELSE(GLUINO_TRUE, 1, 2), 1);
    EXPECT_EQ(GLUINO_IF_ELSE(GLUINO_FALSE, 1, 2), 2);
}

//TEST(preprocessor, count) {
//    EXPECT_EQ(GLUINO_COUNT(), 0);
//    EXPECT_EQ(GLUINO_COUNT(foo), 1);
//    EXPECT_EQ(GLUINO_COUNT(foo, bar, baz), 3);
//}

#define __ADD(x, c) (x + c)

TEST(preprocessor, map) {
    std::vector value{GLUINO_MAP(__ADD, 1, 1, 2, 3)};
    std::vector expected{2, 3, 4};
    EXPECT_EQ(value, expected);

    value = {GLUINO_MAP(__ADD, 3, 1, 2, 3)};
    expected = {4, 5, 6};
    EXPECT_EQ(value, expected);
}

#define __SET_VALUE(x, c) x = c

TEST(preprocessor, map_join) {
    auto x = false;
    auto y = false;
    GLUINO_MAP_JOIN(__SET_VALUE, true, ;, x, y);
    EXPECT_TRUE(x);
    EXPECT_TRUE(y);

    GLUINO_MAP_JOIN(__SET_VALUE, false, ;, x, y);
    EXPECT_FALSE(x);
    EXPECT_FALSE(y);
}

#define __NOOP(x, c) x

TEST(preprocessor, map_concat) {
    GLUINO_MAP_CONCAT(__NOOP, _, auto, x, =, true);
    EXPECT_TRUE(x);
}
