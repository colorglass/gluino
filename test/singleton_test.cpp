#include <exception>

#include <gluino/singleton.h>
#include <gtest/gtest.h>

using namespace gluino;

namespace {
    struct TestStruct {
        int value{0};
        bool initialized{false};

        TestStruct() = default;

        explicit TestStruct(int value) : value(value) {
            if (initialized) {
                throw std::runtime_error("");
            }
            initialized = true;
        }
    };
}

TEST(singleton, instantiates_once) {
    EXPECT_EQ(&singleton<TestStruct>::value, &singleton<TestStruct>::value);
}

TEST(lazy_singleton, instantiates_once_per_ctor) {
    EXPECT_EQ(&lazy_singleton<TestStruct>::value(), &lazy_singleton<TestStruct>::value());
    EXPECT_EQ(&lazy_singleton<TestStruct>::value(10), &lazy_singleton<TestStruct>::value(10));
    EXPECT_EQ(&lazy_singleton<TestStruct>::value(10), &lazy_singleton<TestStruct>::value(20));
}

TEST(lazy_singleton, passes_ctor_args) {
    EXPECT_EQ(10, lazy_singleton<TestStruct>::value(10).value);
    EXPECT_EQ(10, lazy_singleton<TestStruct>::value(20).value);
}
