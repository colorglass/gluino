#include <gluino/ptr.h>
#include <gluino/utility.h>
#include <gtest/gtest.h>

using namespace gluino;

TEST(raw_ptr, zero_space_overhead) {
    static_assert(sizeof(void*) == sizeof(raw_ptr<void>));
}

TEST(raw_ptr, allows_nullptr) {
    raw_ptr<void> opt = nullptr;
    EXPECT_TRUE(opt.is_null());
    EXPECT_EQ(opt.get(), nullptr);
}

TEST(raw_ptr, defaults_to_empty) {
    raw_ptr<void> opt;
    EXPECT_TRUE(opt.is_null());
}

TEST(raw_ptr, value_or) {
    int value = 5;
    raw_ptr<int> empty;
    raw_ptr<int> full = &value;
    int result = 10;
    EXPECT_TRUE(empty.get_or(&result) == &result);
    EXPECT_TRUE(full.get_or(&result) == &value);
}

TEST(safe_ptr, zero_space_overhead) {
    static_assert(sizeof(void*) == sizeof(safe_ptr<void>));
}

TEST(safe_ptr, disallows_nullptr) {
    EXPECT_THROW(safe_ptr<void>(static_cast<void*>(nullptr)), std::invalid_argument);
}

TEST(optional_ptr, zero_space_overhead) {
    static_assert(sizeof(void*) == sizeof(optional_ptr<void>));
}

TEST(optional_ptr, allows_nullptr) {
    optional_ptr<void> opt = nullptr;
    EXPECT_TRUE(opt.is_null());
    EXPECT_THROW(discard(opt.get()), std::bad_optional_access);
}

TEST(optional_ptr, defaults_to_empty) {
    optional_ptr<void> opt;
    EXPECT_TRUE(opt.is_null());
}

TEST(optional_ptr, throw_on_empty_value_access) {
    optional_ptr<void> opt;
    EXPECT_THROW(discard(opt.get()), std::bad_optional_access);
    EXPECT_NO_THROW(discard(opt.get_raw()));
}

TEST(optional_ptr, value_or) {
    int value = 5;
    optional_ptr<int> empty;
    optional_ptr<int> full = &value;
    int result = 10;
    EXPECT_TRUE(empty.get_or(&result) == &result);
    EXPECT_TRUE(full.get_or(&result) == &value);
}
