#include <mutex>

#include <gluino/recursive_spin_mutex.h>
#include <gtest/gtest.h>

using namespace gluino;

TEST(recursive_spin_mutex, mutex) {
    recursive_spin_mutex<> mutex;
    volatile int value = 0;
    std::thread t([&]() {
        value = 1;
        std::unique_lock lock(mutex);
        std::this_thread::sleep_for(std::chrono::milliseconds(30));
        value = 0;
    });
    std::this_thread::sleep_for(std::chrono::milliseconds(20));
    mutex.lock();
    EXPECT_EQ(value, 0);
    t.join();
}

TEST(recursive_spin_mutex, try_lock) {
    recursive_spin_mutex<> mutex;
    volatile int value = 0;
    std::thread t([&]() {
        value = 1;
        std::unique_lock lock(mutex);
        std::this_thread::sleep_for(std::chrono::milliseconds(20));
        value = 0;
    });
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    EXPECT_FALSE(mutex.try_lock());
    std::this_thread::sleep_for(std::chrono::milliseconds(30));
    EXPECT_TRUE(mutex.try_lock());
    t.join();
}

TEST(recursive_spin_mutex, allow_reentry) {
    recursive_spin_mutex<> mutex;
    volatile int value = 0;
    std::thread t([&]() {
        value = 1;
        std::unique_lock lock1(mutex);
        std::unique_lock lock2(mutex);
        std::unique_lock lock3(mutex);
        EXPECT_TRUE(mutex.try_lock());
        std::this_thread::sleep_for(std::chrono::milliseconds(20));
        value = 0;
        mutex.unlock();
    });
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    mutex.lock();
    EXPECT_EQ(value, 0);
    t.join();
}
