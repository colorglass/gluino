#include <gluino/mix.h>
#include <gtest/gtest.h>

using namespace gluino;

struct person {
    std::string name;

    [[nodiscard]] const std::string& get_name() const noexcept {
        return name;
    }
};

struct employed {
    std::string employer;
};

struct ssn {
    std::string ssn;
};

TEST(mix, simple) {
    mix<person, employed, ssn> combined;

    EXPECT_EQ(combined.name, "");
    combined.name = "Bob";
    EXPECT_EQ(combined.get_name(), "Bob");
    EXPECT_EQ(combined.employer, "");
    EXPECT_EQ(combined.ssn, "");
}
