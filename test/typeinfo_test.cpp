#include <gluino/typeinfo.h>
#include <gtest/gtest.h>

using namespace gluino;

namespace {
    struct Structural {
    };

    struct Parent {
        virtual ~Parent() = default;
    };

    struct Child : public Parent {
    };

    struct OtherParent {
        virtual ~OtherParent() = default;
    };

    struct OtherChild : public OtherParent {
    };
}

namespace gluino {
    template <class Char, class Traits>
    struct rtti<std::basic_string<Char, Traits>> {
        static constexpr const std::type_info& type_info = typeid(std::basic_string_view<Char, Traits>);
    };

    template <>
    struct rtti<OtherParent> {
        static constexpr const std::type_info& type_info = typeid(OtherParent);

        template <class T>
        static inline const ::std::type_info& dynamic_type_info(T&&) noexcept {
            return typeid(Structural);
        }
    };
}

TEST(typeinfo, basic_static_typeid) {
    EXPECT_EQ(std::type_index(gluino::type_info<Structural>()), std::type_index(typeid(Structural)));
}

TEST(typeinfo, specialized_static_typeid) {
    EXPECT_EQ(std::type_index(gluino::type_info<std::string>()), std::type_index(typeid(std::string_view)));
    EXPECT_EQ(std::type_index(gluino::type_info<std::wstring>()), std::type_index(typeid(std::wstring_view)));
}

TEST(typeinfo, basic_static_type_index) {
    EXPECT_EQ(gluino::type_index<Structural>(), std::type_index(typeid(Structural)));
}

TEST(typeinfo, specialized_static_type_index) {
    EXPECT_EQ(gluino::type_index<std::string>(), std::type_index(typeid(std::string_view)));
    EXPECT_EQ(gluino::type_index<std::wstring>(), std::type_index(typeid(std::wstring_view)));
}

TEST(typeinfo, basic_polymorphic_typeid) {
    Parent* object = new Child();
    EXPECT_EQ(std::type_index(gluino::type_info(*object)), std::type_index(typeid(Child)));
}

TEST(typeinfo, specialized_polymorphic_typeid) {
    OtherParent* object = new OtherChild();
    EXPECT_EQ(std::type_index(gluino::type_info(*object)), std::type_index(typeid(Structural)));
}

TEST(typeinfo, basic_polymorphic_type_index) {
    Parent* object = new Child();
    EXPECT_EQ(gluino::type_index(*object), std::type_index(typeid(Child)));
}

TEST(typeinfo, specialized_polymorphic_type_index) {
    OtherParent* object = new OtherChild();
    EXPECT_EQ(gluino::type_index(*object), std::type_index(typeid(Structural)));
}
