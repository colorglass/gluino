#include <gluino/virtual_cast.h>
#include <gtest/gtest.h>

using namespace gluino;

namespace {
    struct Parent {
        virtual ~Parent() = default;
    };

    struct Child : public Parent {
    };

    struct OtherChild : public Parent {
    };

    struct OtherParent {
        virtual ~OtherParent() = default;
    };

    struct Static {
    };
}

TEST(virtual_cast, upcast) {
    auto* child = new Child();
    auto* parent = virtual_cast<Parent*>(child).get_raw();
    EXPECT_TRUE(child == parent);
}

TEST(virtual_cast, successful_downcast) {
    auto* original = new Child();
    auto* cast = virtual_cast<Child*>(static_cast<Parent*>(original)).get_raw();
    EXPECT_TRUE(original == cast);
}

TEST(virtual_cast, unsuccessful_downcast) {
    auto* original = new Child();
    auto cast = virtual_cast<OtherChild*>(static_cast<Parent*>(original));
    EXPECT_TRUE(cast.is_null());
}

TEST(virtual_cast, successful_crosscast) {
    auto* original = reinterpret_cast<OtherParent*>(new Child());
    auto cast = virtual_cast<Child*>(original);
    EXPECT_FALSE(cast.is_null());
    EXPECT_EQ(reinterpret_cast<uintptr_t>(cast.get_raw()), reinterpret_cast<uintptr_t>(original));
}

TEST(virtual_cast, unsuccessful_crosscast) {
    auto* original = new Child();
    auto cast = virtual_cast<OtherParent*>(original);
    EXPECT_TRUE(cast.is_null());
}

TEST(virtual_cast, successful_force_crosscast) {
    void* original = reinterpret_cast<OtherParent*>(new Child());
    auto cast = force_virtual_cast<Child*>(original);
    EXPECT_FALSE(cast.is_null());
    EXPECT_EQ(reinterpret_cast<uintptr_t>(cast.get_raw()), reinterpret_cast<uintptr_t>(original));
}

TEST(virtual_cast, unsuccessful_force_crosscast) {
    void* original = new Child();
    auto cast = force_virtual_cast<OtherParent*>(original);
    EXPECT_TRUE(cast.is_null());
}
