#include <algorithm>

#include <city.h>
#include <farmhash.h>
#include <metrohash.h>
#include <xxhash.h>
#include <gluino/map.h>
#include <gtest/gtest.h>

using namespace gluino;

TEST(str_less, case_sensitive_char_cstrings) {
    gluino::str_less<> less;
    EXPECT_TRUE(less("Hello", "hello"));
    EXPECT_FALSE(less("hello", "Hello"));
    EXPECT_FALSE(less("hello", "hello"));
}

TEST(str_less, case_sensitive_wchar_cstrings) {
    gluino::str_less<> less;
    EXPECT_TRUE(less(L"Hello", L"hello"));
    EXPECT_FALSE(less(L"hello", L"Hello"));
    EXPECT_FALSE(less(L"hello", L"hello"));
}

TEST(str_less, case_sensitive_char8_cstrings) {
    gluino::str_less<> less;
    EXPECT_TRUE(less(u8"Hello", u8"hello"));
    EXPECT_FALSE(less(u8"hello", u8"Hello"));
    EXPECT_FALSE(less(u8"hello", u8"hello"));
}

TEST(str_less, case_sensitive_char16_cstrings) {
    gluino::str_less<> less;
    EXPECT_TRUE(less(u"Hello", u"hello"));
    EXPECT_FALSE(less(u"hello", u"Hello"));
    EXPECT_FALSE(less(u"hello", u"hello"));
}

TEST(str_less, case_sensitive_char32_cstrings) {
    gluino::str_less<> less;
    EXPECT_TRUE(less(U"Hello", U"hello"));
    EXPECT_FALSE(less(U"hello", U"Hello"));
    EXPECT_FALSE(less(U"hello", U"hello"));
}

TEST(str_less, case_sensitive_char_string_views) {
    gluino::str_less<> less;
    EXPECT_TRUE(less(std::string_view("Hello"), std::string_view("hello")));
    EXPECT_FALSE(less(std::string_view("hello"), std::string_view("Hello")));
    EXPECT_FALSE(less(std::string_view("hello"), std::string_view("hello")));
}

TEST(str_less, case_sensitive_wchar_string_views) {
    gluino::str_less<> less;
    EXPECT_TRUE(less(std::wstring_view(L"Hello"), std::wstring_view(L"hello")));
    EXPECT_FALSE(less(std::wstring_view(L"hello"), std::wstring_view(L"Hello")));
    EXPECT_FALSE(less(std::wstring_view(L"hello"), std::wstring_view(L"hello")));
}

TEST(str_less, case_sensitive_char8_string_views) {
    gluino::str_less<> less;
    EXPECT_TRUE(less(std::u8string_view(u8"Hello"), std::u8string_view(u8"hello")));
    EXPECT_FALSE(less(std::u8string_view(u8"hello"), std::u8string_view(u8"Hello")));
    EXPECT_FALSE(less(std::u8string_view(u8"hello"), std::u8string_view(u8"hello")));
}

TEST(str_less, case_sensitive_char16_string_views) {
    gluino::str_less<> less;
    EXPECT_TRUE(less(std::u16string_view(u"Hello"), std::u16string_view(u"hello")));
    EXPECT_FALSE(less(std::u16string_view(u"hello"), std::u16string_view(u"Hello")));
    EXPECT_FALSE(less(std::u16string_view(u"hello"), std::u16string_view(u"hello")));
}

TEST(str_less, case_sensitive_char32_string_views) {
    gluino::str_less<> less;
    EXPECT_TRUE(less(std::u32string_view(U"Hello"), std::u32string_view(U"hello")));
    EXPECT_FALSE(less(std::u32string_view(U"hello"), std::u32string_view(U"Hello")));
    EXPECT_FALSE(less(std::u32string_view(U"hello"), std::u32string_view(U"hello")));
}

TEST(str_less, case_sensitive_char_strings) {
    gluino::str_less<> less;
    EXPECT_TRUE(less(std::string("Hello"), std::string("hello")));
    EXPECT_FALSE(less(std::string("hello"), std::string("Hello")));
    EXPECT_FALSE(less(std::string("hello"), std::string("hello")));
}

TEST(str_less, case_sensitive_wchar_strings) {
    gluino::str_less<> less;
    EXPECT_TRUE(less(std::wstring(L"Hello"), std::wstring(L"hello")));
    EXPECT_FALSE(less(std::wstring(L"hello"), std::wstring(L"Hello")));
    EXPECT_FALSE(less(std::wstring(L"hello"), std::wstring(L"hello")));
}

TEST(str_less, case_sensitive_char8_strings) {
    gluino::str_less<> less;
    EXPECT_TRUE(less(std::u8string(u8"Hello"), std::u8string(u8"hello")));
    EXPECT_FALSE(less(std::u8string(u8"hello"), std::u8string(u8"Hello")));
    EXPECT_FALSE(less(std::u8string(u8"hello"), std::u8string(u8"hello")));
}

TEST(str_less, case_sensitive_char16_strings) {
    gluino::str_less<> less;
    EXPECT_TRUE(less(std::u16string(u"Hello"), std::u16string(u"hello")));
    EXPECT_FALSE(less(std::u16string(u"hello"), std::u16string(u"Hello")));
    EXPECT_FALSE(less(std::u16string(u"hello"), std::u16string(u"hello")));
}

TEST(str_less, case_sensitive_char32_strings) {
    gluino::str_less<> less;
    EXPECT_TRUE(less(std::u32string(U"Hello"), std::u32string(U"hello")));
    EXPECT_FALSE(less(std::u32string(U"hello"), std::u32string(U"Hello")));
    EXPECT_FALSE(less(std::u32string(U"hello"), std::u32string(U"hello")));
}

TEST(str_less, cstring_and_string) {
    gluino::str_less<> less;
    EXPECT_TRUE(less("Hello", std::string("hello")));
    EXPECT_FALSE(less("hello", std::string("Hello")));
    EXPECT_FALSE(less("hello", std::string("hello")));
}

TEST(str_less, cstring_and_string_view) {
    gluino::str_less<> less;
    EXPECT_TRUE(less("Hello", std::string_view("hello")));
    EXPECT_FALSE(less("hello", std::string_view("Hello")));
    EXPECT_FALSE(less("hello", std::string_view("hello")));
}

TEST(str_less, string_and_cstring) {
    gluino::str_less<> less;
    EXPECT_TRUE(less(std::string("Hello"), "hello"));
    EXPECT_FALSE(less(std::string("hello"), "Hello"));
    EXPECT_FALSE(less(std::string("hello"), "hello"));
}

TEST(str_less, string_and_string_view) {
    gluino::str_less<> less;
    EXPECT_TRUE(less(std::string("Hello"), std::string_view("hello")));
    EXPECT_FALSE(less(std::string("hello"), std::string_view("Hello")));
    EXPECT_FALSE(less(std::string("hello"), std::string_view("hello")));
}

TEST(str_less, string_view_and_cstring) {
    gluino::str_less<> less;
    EXPECT_TRUE(less(std::string_view("Hello"), "hello"));
    EXPECT_FALSE(less(std::string_view("hello"), "Hello"));
    EXPECT_FALSE(less(std::string_view("hello"), "hello"));
}

TEST(str_less, string_view_and_string) {
    gluino::str_less<> less;
    EXPECT_TRUE(less(std::string_view("Hello"), std::string("hello")));
    EXPECT_FALSE(less(std::string_view("hello"), std::string("Hello")));
    EXPECT_FALSE(less(std::string_view("hello"), std::string("hello")));
}

TEST(str_less, case_insensitive_char) {
    gluino::str_less<false> less;
    EXPECT_FALSE(less("Hello", "hello"));
    EXPECT_FALSE(less("hello", "Hello"));
    EXPECT_TRUE(less("Hello", "Hello, World"));
    EXPECT_FALSE(less("Hello, World", "Hello"));
    EXPECT_TRUE(less("Hello", "Jello"));
    EXPECT_FALSE(less("Jello", "Hello"));
}

TEST(str_less, case_insensitive_wchar) {
    gluino::str_less<false> less;
    EXPECT_FALSE(less(L"Hello", L"hello"));
    EXPECT_FALSE(less(L"hello", L"Hello"));
    EXPECT_TRUE(less(L"Hello", L"Hello, World"));
    EXPECT_FALSE(less(L"Hello, World", L"Hello"));
    EXPECT_TRUE(less(L"Hello", L"Jello"));
    EXPECT_FALSE(less(L"Jello", L"Hello"));
}

TEST(str_less, case_insensitive_char8) {
    gluino::str_less<false> less;
    EXPECT_FALSE(less(u8"Hello", u8"hello"));
    EXPECT_FALSE(less(u8"hello", u8"Hello"));
    EXPECT_TRUE(less(u8"Hello", u8"Hello, World"));
    EXPECT_FALSE(less(u8"Hello, World", u8"Hello"));
    EXPECT_TRUE(less(u8"Hello", u8"Jello"));
    EXPECT_FALSE(less(u8"Jello", u8"Hello"));
}

TEST(str_less, case_insensitive_char16) {
    gluino::str_less<false> less;
    EXPECT_FALSE(less(u"Hello", u"hello"));
    EXPECT_FALSE(less(u"hello", u"Hello"));
    EXPECT_TRUE(less(u"Hello", u"Hello, World"));
    EXPECT_FALSE(less(u"Hello, World", u"Hello"));
    EXPECT_TRUE(less(u"Hello", u"Jello"));
    EXPECT_FALSE(less(u"Jello", u"Hello"));
}

TEST(str_less, case_insensitive_char32) {
    gluino::str_less<false> less;
    EXPECT_FALSE(less(U"Hello", U"hello"));
    EXPECT_FALSE(less(U"hello", U"Hello"));
    EXPECT_TRUE(less(U"Hello", U"Hello, World"));
    EXPECT_FALSE(less(U"Hello, World", U"Hello"));
    EXPECT_TRUE(less(U"Hello", U"Jello"));
    EXPECT_FALSE(less(U"Jello", U"Hello"));
}

template <bool CaseSensitive, class Char>
void test_str_equal_to(const Char* a, const Char* b) {
    gluino::str_equal_to<CaseSensitive> equals;
    std::basic_string<Char> str = a;
    EXPECT_TRUE(equals(a, str.c_str()));
    EXPECT_FALSE(equals(a, b));

    std::transform(str.begin(), str.end(), str.begin(), [](int c) { return static_cast<char>(std::toupper(c)); });
    EXPECT_NE(CaseSensitive, equals(a, str.c_str()));
}

template <bool CaseSensitive, template <class> class StrClass, class Char>
void test_str_equal_to_class(const Char* a, const Char* b) {
    gluino::str_equal_to<CaseSensitive> equals;
    StrClass<Char> a_wrapper = a;
    StrClass<Char> b_wrapper = b;
    std::basic_string<Char> str = a;
    StrClass<Char> str_copy = str;
    EXPECT_TRUE(equals(a_wrapper, str_copy));
    EXPECT_FALSE(equals(a_wrapper, b_wrapper));

    std::transform(str.begin(), str.end(), str.begin(), [](int c) { return static_cast<char>(std::toupper(c)); });
    EXPECT_NE(CaseSensitive, equals(a_wrapper, StrClass<Char>(str)));
}

TEST(str_equal_to, case_sensitive_char_cstrings) {
    test_str_equal_to<true>("Hello", "world");
}

TEST(str_equal_to, case_sensitive_wchar_cstrings) {
    test_str_equal_to<true>(L"Hello", L"world");
}

TEST(str_equal_to, case_sensitive_char8_cstrings) {
    test_str_equal_to<true>(u8"Hello", u8"world");
}

TEST(str_equal_to, case_sensitive_char16_cstrings) {
    test_str_equal_to<true>(u"Hello", u"world");
}

TEST(str_equal_to, case_sensitive_char32_cstrings) {
    test_str_equal_to<true>(U"Hello", U"world");
}

TEST(str_equal_to, case_sensitive_char_string_views) {
    test_str_equal_to_class<true, ::std::basic_string_view>("Hello", "world");
}

TEST(str_equal_to, case_sensitive_wchar_string_views) {
    test_str_equal_to_class<true, ::std::basic_string_view>(L"Hello", L"world");
}

TEST(str_equal_to, case_sensitive_char8_string_views) {
    test_str_equal_to_class<true, ::std::basic_string_view>(u8"Hello", u8"world");
}

TEST(str_equal_to, case_sensitive_char16_string_views) {
    test_str_equal_to_class<true, ::std::basic_string_view>(u"Hello", u"world");
}

TEST(str_equal_to, case_sensitive_char32_string_views) {
    test_str_equal_to_class<true, ::std::basic_string_view>(U"Hello", U"world");
}

TEST(str_equal_to, case_sensitive_char_strings) {
    test_str_equal_to_class<true, ::std::basic_string>("Hello", "world");
}

TEST(str_equal_to, case_sensitive_wchar_strings) {
    test_str_equal_to_class<true, ::std::basic_string>(L"Hello", L"world");
}

TEST(str_equal_to, case_sensitive_char8_strings) {
    test_str_equal_to_class<true, ::std::basic_string>(u8"Hello", u8"world");
}

TEST(str_equal_to, case_sensitive_char16_strings) {
    test_str_equal_to_class<true, ::std::basic_string>(u"Hello", u"world");
}

TEST(str_equal_to, case_sensitive_char32_strings) {
    test_str_equal_to_class<true, ::std::basic_string>(U"Hello", U"world");
}

TEST(str_equal_to, case_insensitive_char_cstrings) {
    test_str_equal_to<false>("Hello", "world");
}

TEST(str_equal_to, case_insensitive_wchar_cstrings) {
    test_str_equal_to<false>(L"Hello", L"world");
}

TEST(str_equal_to, case_insensitive_char8_cstrings) {
    test_str_equal_to<false>(u8"Hello", u8"world");
}

TEST(str_equal_to, case_insensitive_char16_cstrings) {
    test_str_equal_to<false>(u"Hello", u"world");
}

TEST(str_equal_to, case_insensitive_char32_cstrings) {
    test_str_equal_to<false>(U"Hello", U"world");
}

TEST(str_equal_to, case_insensitive_char_string_views) {
    test_str_equal_to_class<true, ::std::basic_string_view>("Hello", "world");
}

TEST(str_equal_to, case_insensitive_wchar_string_views) {
    test_str_equal_to_class<true, ::std::basic_string_view>(L"Hello", L"world");
}

TEST(str_equal_to, case_insensitive_char8_string_views) {
    test_str_equal_to_class<true, ::std::basic_string_view>(u8"Hello", u8"world");
}

TEST(str_equal_to, case_insensitive_char16_string_views) {
    test_str_equal_to_class<true, ::std::basic_string_view>(u"Hello", u"world");
}

TEST(str_equal_to, case_insensitive_char32_string_views) {
    test_str_equal_to_class<true, ::std::basic_string_view>(U"Hello", U"world");
}

TEST(str_equal_to, case_insensitive_char_strings) {
    test_str_equal_to_class<true, ::std::basic_string>("Hello", "world");
}

TEST(str_equal_to, case_insensitive_wchar_strings) {
    test_str_equal_to_class<true, ::std::basic_string>(L"Hello", L"world");
}

TEST(str_equal_to, case_insensitive_char8_strings) {
    test_str_equal_to_class<true, ::std::basic_string>(u8"Hello", u8"world");
}

TEST(str_equal_to, case_insensitive_char16_strings) {
    test_str_equal_to_class<true, ::std::basic_string>(u"Hello", u"world");
}

TEST(str_equal_to, case_insensitive_char32_strings) {
    test_str_equal_to_class<true, ::std::basic_string>(U"Hello", U"world");
}

TEST(str_equal_to, cstring_and_string_view) {
    str_equal_to<> equal;
    EXPECT_TRUE(equal("Hello", std::string_view("Hello")));
}

TEST(str_equal_to, cstring_and_string) {
    str_equal_to<> equal;
    EXPECT_TRUE(equal("Hello", std::string("Hello")));
}

TEST(str_equal_to, string_view_and_cstring) {
    str_equal_to<> equal;
    EXPECT_TRUE(equal(std::string_view("Hello"), "Hello"));
}

TEST(str_equal_to, string_view_and_string) {
    str_equal_to<> equal;
    EXPECT_TRUE(equal(std::string_view("Hello"), std::string("Hello")));
}

TEST(str_equal_to, string_and_cstring) {
    str_equal_to<> equal;
    EXPECT_TRUE(equal(std::string("Hello"), "Hello"));
}

TEST(str_equal_to, string_and_string_view) {
    str_equal_to<> equal;
    EXPECT_TRUE(equal(std::string("Hello"), std::string_view("Hello")));
}

template <bool CaseSensitive, class Char>
void test_str_hash(const Char* value) {
    gluino::str_hash<CaseSensitive> h;
    std::basic_string<Char> str = value;
    EXPECT_EQ(h(value), h(str.c_str()));
    str[0] = '_';
    EXPECT_NE(h(value), h(str.c_str()));

    str = value;
    std::transform(str.begin(), str.end(), str.begin(), [](int c) { return static_cast<char>(std::toupper(c)); });
    EXPECT_NE(CaseSensitive, h(value) == h(str.c_str()));
}

template <bool CaseSensitive, template <class> class StrClass, class Char>
void test_str_hash_class(const Char* value) {
    gluino::str_hash<CaseSensitive> h;

    std::basic_string<Char> str = value;
    StrClass<Char> value_wrapped = value;
    StrClass<Char> copy_wrapped = str;
    EXPECT_EQ(h(value_wrapped), h(copy_wrapped));

    str[0] = '_';
    copy_wrapped = str;
    EXPECT_NE(h(value_wrapped), h(copy_wrapped));

    str = value;
    std::transform(str.begin(), str.end(), str.begin(), [](int c) { return static_cast<char>(std::toupper(c)); });
    EXPECT_NE(CaseSensitive, h(value_wrapped) == h(StrClass<Char>(str)));
}

TEST(str_hash, case_sensitive_char_cstring_boost) {
    test_str_hash<true>("Hello");
}

TEST(str_hash, case_sensitive_char_cstring_cityhash) {
    test_str_hash<true>("Hello");
}

TEST(str_hash, case_sensitive_wchar_cstring) {
    test_str_hash<true>(L"Hello");
}

TEST(str_hash, case_sensitive_char8_cstring_boost) {
    test_str_hash<true>(u8"Hello");
}

TEST(str_hash, case_sensitive_char8_cstring_cityhash) {
    test_str_hash<true>(u8"Hello");
}

TEST(str_hash, case_sensitive_char16_cstring) {
    test_str_hash<true>(u"Hello");
}

TEST(str_hash, case_sensitive_char32_cstring) {
    test_str_hash<true>(U"Hello");
}

TEST(str_hash, case_sensitive_char_string_view_boost) {
    test_str_hash_class<true, std::basic_string_view>("Hello");
}

TEST(str_hash, case_sensitive_char_string_view_cityhash) {
    test_str_hash_class<true, std::basic_string_view>("Hello");
}

TEST(str_hash, case_sensitive_wchar_string_view) {
    test_str_hash_class<true, std::basic_string_view>(L"Hello");
}

TEST(str_hash, case_sensitive_char8_string_view_boost) {
    test_str_hash_class<true, std::basic_string_view>(u8"Hello");
}

TEST(str_hash, case_sensitive_char8_string_view_cityhash) {
    test_str_hash_class<true, std::basic_string_view>(u8"Hello");
}

TEST(str_hash, case_sensitive_char16_string_view) {
    test_str_hash_class<true, std::basic_string_view>(u"Hello");
}

TEST(str_hash, case_sensitive_char32_string_view) {
    test_str_hash_class<true, std::basic_string_view>(U"Hello");
}

TEST(str_hash, case_sensitive_char_string_boost) {
    test_str_hash_class<true, std::basic_string>("Hello");
}

TEST(str_hash, case_sensitive_char_string_cityhash) {
    test_str_hash_class<true, std::basic_string>("Hello");
}

TEST(str_hash, case_sensitive_wchar_string) {
    test_str_hash_class<true, std::basic_string>(L"Hello");
}

TEST(str_hash, case_sensitive_char8_string_boost) {
    test_str_hash_class<true, std::basic_string>(u8"Hello");
}

TEST(str_hash, case_sensitive_char8_string_cityhash) {
    test_str_hash_class<true, std::basic_string>(u8"Hello");
}

TEST(str_hash, case_sensitive_char16_string) {
    test_str_hash_class<true, std::basic_string>(u"Hello");
}

TEST(str_hash, case_sensitive_char32_string) {
    test_str_hash_class<true, std::basic_string>(U"Hello");
}

TEST(str_hash, case_insensitive_char_cstring) {
    test_str_hash<false>("Hello");
}

TEST(str_hash, case_insensitive_wchar_cstring) {
    test_str_hash<false>(L"Hello");
}

TEST(str_hash, case_insensitive_char8_cstring) {
    test_str_hash<false>(u8"Hello");
}

TEST(str_hash, case_insensitive_char16_cstring) {
    test_str_hash<false>(u"Hello");
}

TEST(str_hash, case_insensitive_char32_cstring) {
    test_str_hash<false>(U"Hello");
}

TEST(str_hash, case_insensitive_char_string_view) {
    test_str_hash_class<false, std::basic_string_view>("Hello");
}

TEST(str_hash, case_insensitive_wchar_string_view) {
    test_str_hash_class<false, std::basic_string_view>(L"Hello");
}

TEST(str_hash, case_insensitive_char8_string_view) {
    test_str_hash_class<false, std::basic_string_view>(u8"Hello");
}

TEST(str_hash, case_insensitive_char16_string_view) {
    test_str_hash_class<false, std::basic_string_view>(u"Hello");
}

TEST(str_hash, case_insensitive_char32_string_view) {
    test_str_hash_class<false, std::basic_string_view>(U"Hello");
}

TEST(str_hash, case_insensitive_char_string) {
    test_str_hash_class<false, std::basic_string>("Hello");
}

TEST(str_hash, case_insensitive_wchar_string) {
    test_str_hash_class<false, std::basic_string>(L"Hello");
}

TEST(str_hash, case_insensitive_char8_string) {
    test_str_hash_class<false, std::basic_string>(u8"Hello");
}

TEST(str_hash, case_insensitive_char16_string) {
    test_str_hash_class<false, std::basic_string>(u"Hello");
}

TEST(str_hash, case_insensitive_char32_string) {
    test_str_hash_class<false, std::basic_string>(U"Hello");
}
