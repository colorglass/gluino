#include <deque>
#include <forward_list>
#include <list>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <gluino/container.h>
#include <gtest/gtest.h>

using namespace gluino;

TEST(container, reserve_deque) {
    std::deque<int> cont;
    reserve(cont, 10);
}

TEST(container, reserve_forward_list) {
    std::forward_list<int> cont;
    reserve(cont, 10);
}

TEST(container, reserve_list) {
    std::list<int> cont;
    reserve(cont, 10);
}

TEST(container, reserve_map) {
    std::map<int, int> cont;
    reserve(cont, 10);
}

TEST(container, reserve_priority_queue) {
    std::priority_queue<int> cont;
    reserve(cont, 10);
}

TEST(container, reserve_queue) {
    std::queue<int> cont;
    reserve(cont, 10);
}

TEST(container, reserve_set) {
    std::set<int> cont;
    reserve(cont, 10);
}

TEST(container, reserve_stack) {
    std::stack<int> cont;
    reserve(cont, 10);
}

TEST(container, reserve_unordered_map) {
    std::unordered_map<int, int> cont;
    EXPECT_LE(cont.bucket_count(), 128);
    reserve(cont, 1024);
    EXPECT_GE(cont.bucket_count(), 1024);
}

TEST(container, reserve_unordered_set) {
    std::unordered_set<int> cont;
    EXPECT_LE(cont.bucket_count(), 128);
    reserve(cont, 1024);
    EXPECT_GE(cont.bucket_count(), 1024);
}

TEST(container, reserve_vector) {
    std::vector<int> cont;
    EXPECT_LE(cont.capacity(), 128);
    reserve(cont, 1024);
    EXPECT_GE(cont.capacity(), 1024);
}

TEST(container, insert_deque) {
    std::deque<int> cont;
    EXPECT_EQ(*insert(cont, 1), 1);
    EXPECT_EQ(*insert(cont, 2), 2);
    EXPECT_EQ(cont, (std::deque<int>{1, 2}));
}

TEST(container, insert_forward_list) {
    std::forward_list<int> cont;
    EXPECT_EQ(*insert(cont, 1), 1);
    EXPECT_EQ(*insert(cont, 2), 2);
    EXPECT_EQ(cont, (std::forward_list<int>{2, 1}));
}

TEST(container, insert_list) {
    std::list<int> cont;
    EXPECT_EQ(*insert(cont, 1), 1);
    EXPECT_EQ(*insert(cont, 2), 2);
    EXPECT_EQ(cont, (std::list<int>{1, 2}));
}

TEST(container, insert_map) {
    std::map<int, int> cont;
    auto iter = insert(cont, 1, 2);
    EXPECT_EQ(iter->first, 1);
    EXPECT_EQ(iter->second, 2);
    iter = insert(cont, 2, 3);
    EXPECT_EQ(iter->first, 2);
    EXPECT_EQ(iter->second, 3);
    EXPECT_EQ(cont, (std::map<int, int>{{1, 2}, {2, 3}}));
}

//TEST(container, insert_priority_queue) {
//    std::priority_queue<int> cont;
//    insert(cont, 1);
//    insert(cont, 2);
//    EXPECT_EQ(cont.top(), 1);
//    cont.pop();
//    EXPECT_EQ(cont.top(), 2);
//    cont.pop();
//    EXPECT_TRUE(cont.empty());
//}
//
//TEST(container, insert_queue) {
//    std::queue<int> cont;
//    insert(cont, 1);
//    insert(cont, 2);
//    EXPECT_EQ(cont.front(), 1);
//    cont.pop();
//    EXPECT_EQ(cont.front(), 2);
//    cont.pop();
//    EXPECT_TRUE(cont.empty());
//}

TEST(container, insert_set) {
    std::set<int> cont;
    EXPECT_EQ(*insert(cont, 1), 1);
    EXPECT_EQ(*insert(cont, 2), 2);
    EXPECT_EQ(cont, (std::set<int>{1, 2}));
}

//TEST(container, insert_stack) {
//    std::stack<int> cont;
//    insert(cont, 1);
//    insert(cont, 2);
//    EXPECT_EQ(cont.top(), 2);
//    cont.pop();
//    EXPECT_EQ(cont.top(), 1);
//    cont.pop();
//    EXPECT_TRUE(cont.empty());
//}

TEST(container, insert_unordered_map) {
    std::unordered_map<int, int> cont;
    auto iter = insert(cont, 1, 2);
    EXPECT_EQ(iter->first, 1);
    EXPECT_EQ(iter->second, 2);
    iter = insert(cont, 2, 3);
    EXPECT_EQ(iter->first, 2);
    EXPECT_EQ(iter->second, 3);
    EXPECT_EQ(cont, (std::unordered_map<int, int>{{1, 2}, {2, 3}}));
}

TEST(container, insert_unordered_set) {
    std::unordered_set<int> cont;
    EXPECT_EQ(*insert(cont, 1), 1);
    EXPECT_EQ(*insert(cont, 2), 2);
    EXPECT_EQ(cont, (std::unordered_set<int>{1, 2}));
}

TEST(container, insert_vector) {
    std::vector<int> cont;
    EXPECT_EQ(*insert(cont, 1), 1);
    EXPECT_EQ(*insert(cont, 2), 2);
    EXPECT_EQ(cont, (std::vector<int>{1, 2}));
}

TEST(container, emplace_deque) {
    std::deque<int> cont;
    emplace(cont, 1);
    emplace(cont, 2);
    EXPECT_EQ(cont, (std::deque<int>{1, 2}));
}

TEST(container, emplace_forward_list) {
    std::forward_list<int> cont;
    emplace(cont, 1);
    emplace(cont, 2);
    EXPECT_EQ(cont, (std::forward_list<int>{2, 1}));
}

TEST(container, emplace_list) {
    std::list<int> cont;
    emplace(cont, 1);
    emplace(cont, 2);
    EXPECT_EQ(cont, (std::list<int>{1, 2}));
}

TEST(container, emplace_map) {
    std::map<int, int> cont;
    emplace(cont, 1, 2);
    emplace(cont, 2, 3);
    EXPECT_EQ(cont, (std::map<int, int>{{1, 2}, {2, 3}}));
}

TEST(container, emplace_priority_queue) {
    std::priority_queue<int> cont;
    emplace(cont, 1);
    emplace(cont, 2);
    EXPECT_EQ(cont.top(), 2);
    cont.pop();
    EXPECT_EQ(cont.top(), 1);
    cont.pop();
    EXPECT_TRUE(cont.empty());
}

TEST(container, emplace_queue) {
    std::queue<int> cont;
    emplace(cont, 1);
    emplace(cont, 2);
    EXPECT_EQ(cont.front(), 1);
    cont.pop();
    EXPECT_EQ(cont.front(), 2);
    cont.pop();
    EXPECT_TRUE(cont.empty());
}

TEST(container, emplace_set) {
    std::set<int> cont;
    emplace(cont, 1);
    emplace(cont, 2);
    EXPECT_EQ(cont, (std::set<int>{1, 2}));
}

TEST(container, emplace_stack) {
    std::stack<int> cont;
    emplace(cont, 1);
    emplace(cont, 2);
    EXPECT_EQ(cont.top(), 2);
    cont.pop();
    EXPECT_EQ(cont.top(), 1);
    cont.pop();
    EXPECT_TRUE(cont.empty());
}

TEST(container, emplace_unordered_map) {
    std::unordered_map<int, int> cont;
    emplace(cont, 1, 2);
    emplace(cont, 2, 3);
    EXPECT_EQ(cont, (std::unordered_map<int, int>{{1, 2}, {2, 3}}));
}

TEST(container, emplace_unordered_set) {
    std::unordered_set<int> cont;
    emplace(cont, 1);
    emplace(cont, 2);
    EXPECT_EQ(cont, (std::unordered_set<int>{1, 2}));
}

TEST(container, emplace_vector) {
    std::vector<int> cont;
    emplace(cont, 1);
    emplace(cont, 2);
    EXPECT_EQ(cont, (std::vector<int>{1, 2}));
}

TEST(container, clear_deque) {
    std::deque<int> cont{1, 2, 3};
    clear(cont);
    EXPECT_TRUE(cont.empty());
}

TEST(container, clear_forward_list) {
    std::forward_list<int> cont{1, 2, 3};
    clear(cont);
    EXPECT_TRUE(cont.empty());
}

TEST(container, clear_list) {
    std::list<int> cont{1, 2, 3};
    clear(cont);
    EXPECT_TRUE(cont.empty());
}

TEST(container, clear_map) {
    std::map<int, int> cont{{1, 1}, {2, 2}, {3, 3}};
    clear(cont);
    EXPECT_TRUE(cont.empty());
}

TEST(container, clear_priority_queue) {
    std::priority_queue<int> cont;
    cont.push(1);
    cont.push(2);
    cont.push(3);
    clear(cont);
    EXPECT_TRUE(cont.empty());
}

TEST(container, clear_queue) {
    std::queue<int> cont;
    cont.push(1);
    cont.push(2);
    cont.push(3);
    clear(cont);
    EXPECT_TRUE(cont.empty());
}

TEST(container, clear_set) {
    std::set<int> cont{1, 2, 3};
    clear(cont);
    EXPECT_TRUE(cont.empty());
}

TEST(container, clear_stack) {
    std::stack<int> cont;
    cont.push(1);
    cont.push(2);
    cont.push(3);
    clear(cont);
    EXPECT_TRUE(cont.empty());
}

TEST(container, clear_unordered_map) {
    std::unordered_map<int, int> cont{{1, 1}, {2, 2}, {3, 3}};
    clear(cont);
    EXPECT_TRUE(cont.empty());
}

TEST(container, clear_unordered_set) {
    std::unordered_set<int> cont{1, 2, 3};
    clear(cont);
    EXPECT_TRUE(cont.empty());
}

TEST(container, clear_vector) {
    std::vector<int> cont{1, 2, 3};
    clear(cont);
    EXPECT_TRUE(cont.empty());
}
