#include <gluino/semantic_version.h>
#include <gtest/gtest.h>

using namespace gluino;

TEST(semantic_version, parse) {
    semantic_version semver("3.50.777-10.foo-bar+123");
    EXPECT_EQ(semver.major(), 3);
    EXPECT_EQ(semver.minor(), 50);
    EXPECT_EQ(semver.patch(), 777);
}
