# Gluino
Gluino is a header-only utility library for C++20 and later, intended to enhance the standard libraries. It can be
thought of as providing STL++. All functionality is tailored for modern C++ patterns and to blend into STL-using code,
while filling in its gaps.

# Tables of Contents
* [Minimum Requirements](#minimum-requirements)
* [Dependencies](#dependencies)
* [Extension Methods](#extension-methods)
* [Advanced Polymorphism and RTTI](#advanced-polymorphism-and-rtti)
  * [Extensible RTTI System and Portability](#extensible-rtti-system-and-portability) 
* [Smart Enums](#smart-enums)
  * [Single-valued Enums](#single-valued-enums)
  * [Multi-valued Enums](#multi-valued-enums)
* [Generic Container Building](#generic-container-building)
* ["Dumb" Pointer Types](#dumb-pointer-types)
* [Complete RNG Engines](#complete-rng-engines)
* [Synchronization Primitives](#synchronization-primitives)
* [Advanced String Map/Set Containers](#advanced-string-mapset-containers)
* [Minimum-Copy String Conversion](#minimum-copy-string-conversion)
* [Utilities](#utilities)
  * [Singletons](#singletons)
  * [Arbitrary Type Mixing](#arbitrary-type-mixing)
  * [Synchronized Types](#synchronized-types)
  * [Clearer Intentional Discard](#clearer-intentional-discard)
  * [Semantic (and Optimized) Value Claiming/Moving](#semantic-and-optimized-value-claimingmoving)
* [Type Traits and Concepts](#type-traits-and-concepts)
* [Preprocessor Metaprogramming](#preprocessor-metaprogramming)

## Minimum Requirements
* Gluino requires a minimum C++ version of C++20. It is recommended to use `c++latest` on MSVC due to certain C++20
  features only being enabled in C++23 mode.
* Some features only portable to Itanium and MSVC ABIs (validated on GCC, Clang, and MSVC).

## Installation and Use
### Manual Install
Gluino is a header-only library, and can be included in a project by simply copying all headers into the project.

### Conan
Gluino builds are released on Conan. You must add a GitLab remote for Gluino's repository.

```console
conan remote add gitlab https://gitlab.com/api/v4/projects/33102285/packages/conan
```

You can then add Gluino to your project's `conanfile.txt` recipe:

```toml
[requires]
gluino/0.0.1@colorglass+gluino/stable

[generators]
cmake
```

### Vcpkg
Vcpkg is supported via the Color-Glass Studios repository. To add Gluino to your project, add `vcpkg-configuration.json`
in the project root (next to `vcpkg.json`) and add the following content:

```json
{
  "registries": [
    {
      "kind": "git",
      "repository": "https://gitlab.com/colorglass/vcpkg-colorglass",
      "baseline": "df8f0e11d525041f622654d48127ca5bd69f1832",
      "packages": [
        "gluino"
      ]
    }
  ]
}
```

You can then add `gluino` to your list of dependencies in `vcpkg.json`. You can also add Gluino globally (Vcpkg classic
mode) by adding the `vcpkg-configuration.json` file in your Vcpkg root.

## Extension Methods
Gluino implements a form of extension methods for C++, which use near-native syntax to attach new functions to types
in a way that appears similar to member functions. By default, extension methods are invoked using the pointer-to-member
operator (`->*`), although other operators can be used (or combinations of multiple operators).

Extension methods are defined by first creating a function object. The apply operator (`operator()`) is overridden on
the type and the first parameter will be treated as the `this` object on the left hand side of the invocation operator.
The function object is bound to the extension system by declaring a `constexpr gluino::extension<T>` variable with the
function name.

```c++
#include <gluino/extension.h>

namespace detail {
    struct not_empty {
        template <class Char, class Traits, class Alloc>
        [[nodiscard]] inline bool operator()(const ::std::basic_string_view<Char, Traits, Alloc>& str) const noexcept {
            return !str.empty();
        }
    };
}

constexpr gluino::extension<detail::not_empty> not_empty;

void example() {
    std::string str = "Hello, World!";
    str->*not_empty();
}
```

Multiple overloads can be implemented in the function object and they will apply to the extension invocation. To control
the operator that can be used for invocation, a second template parameter on `extension` is available, which a non-type
parameter of type `gluino::extension_ops`, which is in turn a bit-flag enum from Gluino's bit-flag system. You can
therefore use Gluino bit-flag operators to define combinations of operators that will be permitted, e.g.:

```c++
constexpr gluino::extension<detail::not_empty,
                            gluino::extension_ops::pointer_to_member + gluino::extension_ops::pipe> not_empty;
```

The permitted operators are pointer-to-member (`->*`), pipe (`|`), double-pipe (`||`), forward-slash (`/`), shift-left
(`<<`), and shift-right (`>>`), or any combination thereof. Order of operations must be taken into account depending on
the operator being used.

## Advanced Polymorphism and RTTI
Gluino supports working with unknown types, or mistyped objects, polymorphically. The `virtual_cast` is similar to the
built-in `dynamic_cast`, but with the crucial difference: `dynamic_cast` rules state that when casting value `v` to type
`T`, if `v` is of a type which is neither a base class of `T` nor a derived class of `T`, then the compiler shall simply
produce `nullptr`. `virtual_cast` allows this kind of cast, provided that the type of `v` is polymorphic or (in the case
of `force_virtual_cast`) a void pointer. All cases for which `dynamic_cast` already is sufficient will compile down to a
`dynamic_cast`, making `virtual_cast` no additional overhead except where it's behavior differs.

```c++
#include <gluino/virtual_cast.h>

class parent {
    virtual ~parent() = default;
};

class child : public parent {
};

class other_parent {
    virtual ~other_parent() = default;
};

void example() {
    other_parent* p = reinterpret_cast<other_parent*>(new child());
    child* c = virtual_cast<child*>(p); // Successfully casts to child*.
    
    c = force_virtual_cast<child*>(reinterpret_cast<void*>(p)); // Also succeeds.
}
```

Note that `force_virtual_cast` behavior is undefined (likely a crash) if the void pointer passed to it does not
actually point to a polymorphic object, hence why it is separated into a different function.

It may seem pointless to perform such a cast, since it should never succeed. However allowing this behavior makes it
possible to cast an object of totally unknown type. This is done with Gluino's `polymorphic_any`, an alternative to
`std::any` which allows polymorphic retrieval of its value. Consider the following example:

```c++
#include <gluino/polymorphic_any.h>

class parent {
    virtual ~parent() = default;
};

class child : public parent {
};

void example() {
    parent* p = new child();
    
    std::any val(p);
    std::any_cast<child*>(val); // throws std::bad_any_cast because static value at time of insertion was parent*.
    
    gluino::polymorphic_any val(p);
    std::any_cast<child*>(val); // Succeeds, returns the pointer as child*.
}
```

The `polymorphic_any` behaves identically to `std::any` for non-polymorphic values (indeed in this case it is a
zero-overhead wrapper for `std::any`). However it gains its polymorphic behavior for polymorphic objects. Whether the
contained value is polymorphic can be checked with the `is_polymorphic()` function. The value is retrieved via the
standard `std::any_cast` with the same overloads and behavior, save for the fact that the type may be a base type or
derived type of the actual stored pointer.

Setting the value of `polymorphic_any` to something polymorphic must be done through a pointer. Passing by reference
results in a static compile-time error. This is done to prevent ambiguity; passing by reference implies attempting to
copy the referenced value entirely into the `polymorphic_any`, which cannot be done safely for a polymorphic type.

More generally, the RTTI system in Gluino allows for determining the relationship between two types dynamically, which
is not supported by the RTTI system in standard C++.

#### Extensible RTTI System and Portability
The capabilities represented by `virtual_cast`, `force_virtual_cast`, and `polymorphic_any` are not possible using
portable standard C++ functionality; instead it is handled by directly reading compiled RTTI information. As a result,
these features can only be supported on platforms using the Itanium C++ ABI specification, or Microsoft's Visual Studio
(although this represents almost all modern C++ code).

To handle potentially odd RTTI cases, such as reverse engineering, it is possible to override how type information is
looked up in Gluino. This is handled with the `gluino::rtti` struct, which can be specialized for certain types. The
struct includes an `Enable` template parameter for advanced type trait use. This functionality can be utilized more
generally through the `typeinfo.h` header, which provides the functions `gluino::type_info<T>()` and
`gluino::type_index<T>()` functions for static type lookup, and `gluino::type_info<T>(T&&)` and
`gluino::type_index<T>(T&&)` functions for polymorphic lookup. Note that to eliminate ambiguity, the polymorphic lookup
is not allowed on types which do not support dynamic type lookup (note that this is a Gluino concept that does not
strictly map to being polymorphic -- see below for further explanation).

To override RTTI lookup for these functions, provide your own `gluino::rtti` struct specialization:

```c++
#include <gluino/typeinfo.h>

namespace gluino {
    // Example of overriding typeid lookup for all strings, so they claim to be string views instead.
    template <class Char, class Traits, class Alloc>
    struct rtti<std::basic_string<Char, Traits, Alloc>> {
        static constexpr const std::type_info& type_info = typeid(std::basic_string_view<Char, Traits>);
    };
}
```

The polymorphic type lookup versions of these functions depend on the `dynamically_typed` concept. This concept applies
if the `rtti` specialization also includes a function `dynamic_type_info(T&&)`. The default `rtti` struct includes this
function only on the condition that the type is polymorphic, but adding this function into your specialization allows
the RTTI system to treat the type as polymorphic regardless.

```c++
namespace gluino {
    template <class Char, class Traits, class Alloc>
    struct rtti<std::basic_string<Char, Traits, Alloc>> {
        static constexpr const std::type_info& type_info = typeid(std::basic_string_view<Char, Traits>);
        
        // Now we can get dynamic type info for strings; here it returns string view type info for empty
        // strings but the regular string type info for all others.
        static inline const std::type_info& dynamic_type_info(
                const std::basic_string<Char, Traits, Alloc>& str) noexcept {
            return str.empty() ? typeid(std::basic_string_view<Char, Traits>) : typeid(str);
        }
    };
}
```

## Smart Enums
### Single-valued Enums
Gluino implements enhanced enumerators with more operations and reflection capabilities. To define an enhanced enum, use
the `gluino_enum` macro. This macro takes a first argument of the enum name, and a second of the base type of the enum.
Any number of subsequent arguments can be given, which must be a parenthesized pair of the constant name followed by its
value. The enum is defined in the namespace in which the macro appears.

```c++
#include <gluino/enumeration.h>

gluino_enum(my_enum, uint16_t, (foo, 1), (bar, 2));
```

The enumeration declared is a class rather than a standard C++ enum. For compatibility purposes, a standard enum is
defined with the member type `enum_type` (e.g. in the example above it can be accessed with `my_enum::enum_type`).
However, all constants are declared on the class itself and return constant expression instances of the class. Because
the enumeration class is structural, and it's major operations are constant expressions, it can be used normally in
non-type template parameters. The default value of the enumeration will be the first member declared.

The enumeration allows for access to a values string name, or access to a value via a string name. When creating an
instance via a string, the string is validated. Attempting to use a string that does not name a constant will result in
`std::invalid_argument` being thrown. Attempting to set the value to an integer that does not map to a valid value, or
to the compatibility enum type when that is not a valid value, will also result in an exception.

```c++
#include <gluino/enumeration.h>

gluino_enum(my_enum, uint16_t, (foo, 1), (bar, 2));

void example() {
    std::cout << my_enum::foo.name() << std::endl; // Prints "foo".
    my_enum e("foo"); // Equal to my_enum::foo.
    my_enum e2("bad_name"); // Throws exception.
    my_enum e3(0); // Throws exception.
}
```

It is also possible to iterate over values of an enumeration. Given an enum value, calling `next()` will return an
`std::optional` for the next enum value, in the order of their declaration. If the enum is the last declared value, the
result is empty. Likewise, `previous()` will return an `std::optional` with the previous declared value. The static
function `values()` will return an `std::span` of all declared enum values in the order of their declaration. This
allows introspection and iteration over the values. Calling `current()` on an enum value will return the iterator for
the position in that span where the value is located.

Internally, a static map is used to keep information about the valid values and names of members of the enumeration. The
map type used can be controlled by the macro `GLUINO_OVERRIDE_ENUM_MAP_TYPE`. If not set it will default to
`std::unordered_map`. Any alternative type should be compatible with the template interface of `std::unordered_map` to
two parameters, i.e. it's first two template parameters should be type parameters indicating the key and value type
respectively. The actual interface used to interact with the map does not need to be compatible with
`std::unordered_map` so long as it can be accessed via the generic container manipulation system in Gluino (see below
for more explanation of that feature).

### Multi-valued Enums
The `gluino_flags` macro declares a smart-enumeration class similar to `gluino_enum`, but specialized for use in
bit-flag cases. These enums can hold multiple values simultaneously. They're functionality is similar but adapted for
this crucial difference. Instead of a `name()` function which returns the name of the current value, `names()` returns a
`std::vector<std::string_view>` with the names of all declared values which are set. Calling `set_flags()` returns a
vector of the atomic declared values themselves. The multi-valued flag enum can be set to any combination of its flags,
but can never contain a value that is not declared (i.e. only bits set in any of the declared constants can be set on
the value).

Flag enums also come with convenience methods and operators for checking multiple values. `all(...)` checks that all
provided values (which can be specified as the string name, integer value, compatibility enum type, or another instance
of the smart enum) are set. `none(...)` checks that none are set. `any(...)` checks that one or more are set. The
`has(...)` function is identical to `all(...)` but takes only one argument and is more readable for that use case.

The typical operators needed for bit flag operation are provided, such as `&`, `|`, `^`, and `~`. In addition, two
operators are added for readability, `+`, and `-`. For two enum values `x` and `y`, `x + y` is identical in meaning to
`x | y`, and `x - y` is identical in meaning to `x & ~y`, thus setting or unsetting flags. All operators are constant
expressions and therefore can be used even in non-type template parameters.

Multi-valued enums also use `GLUINO_OVERRIDE_ENUM_MAP_TYPE` to determine internal map types used to store implementation
data (see the single-valued enum section for details).

```c++
#include <gluino/flags.h>

gluino_flags(my_flags, uint16_t, (foo, 1 << 0), (bar, 1 << 1));

void example() {
    my_flags f = my_flags::foo + my_flags::bar;
    f.any(my_flags::foo, my_flags::bar); // => true
    my_flags.set_flags(); // => {my_flags::foo, my_flags::bar}
    my_flags.names(); // {"foo", "bar"}
    my_flags x = f - my_flags::foo; // x == my:flags::bar
    my_flags.has(my:flags::foo); // => false
    f.unset(my:flags::bar); // f now == my_flags::foo
}

template <class T, my_flags F = my_flags::foo + my_flags::bar>
void template_example() { }
```

## Generic Container Building
Gluino includes a type-traits- and concept-based system for generically working with containers to a limited degree.
This system lets you work with numerous types of containers without knowing the specific details of each one. The
following operations are supported:

* `reserve`: Attempts to reserve space in a container. Containers which lack reservation capability should treat this as
  a no-op.
* `insert`: Emplaces (rather than inserts) a new value into the container. For ordered containers, this should generally
  emplace at the end, unless that is impossible. Returns an iterator to the new entry. Requires the container to be
  emplaceable and iterable.
* `emplace`: Emplaces a new value into the container, similar to `insert`. However, returns `bool` rather than an
  iterator, and therefore does not require the container to be iterable, but does require arbitrary insertion (e.g.
  excludes `std::array`, which needs an index hint -- see `emplace_nth`).
* `emplace_nth`: Emplaces a new value into the container, similar to `emplace`, but takes an additional index value. The
  index indicates that it is the *n*th known element to be inserted, and can act as a hint as to placement or sizing.
* `clear`: Clears all contents from the container, if possible. If there is no `clear()` function on the container,
  alternative means of clearing are allowed (e.g. assigning to an empty container).
* `find`: For set/map containers, performs a lookup. Returns an `std::optional` of the result, which is empty if no
  result was found.

Gluino provides a default overload for all of these functions for containers which do not support the given operations.
For most, this means a `static_assert` which fails, giving a compilation error, with the exception of `reserve` which is
defined as a no-op for unsupported containers. Overloads are also provided via concepts which match all STL and STL-like
containers. This has been validated on all of the following container libraries:
* [All STL (C++ standard library) containers.](https://en.cppreference.com/w/cpp/container).
* [Intel's Thread Building Blocks'](https://www.intel.com/content/www/us/en/develop/documentation/onetbb-documentation/top.html)
  `tbb::concurrent_hash_map` and `tbb::concurrent_vector`.
* [Gregory Popovitch's parallel_hashmap types](https://github.com/greg7mdp/parallel-hashmap) `phmap::flat_hash_map`,
  `phmap::node_hash_map`, `phmap::parallel_flat_hash_map`, `phmap::parallel_node_hash_map`, `phmap::flat_hash_set`,
  `phmap::node_hash_set`, `phmap::parallel_flat_hash_set`, `phmap::parallel_node_hash_set`, `phmap::btree_map`, and
  `phmap::btree_set`.
* [Tessil's](https://github.com/Tessil) `tsl::robin_map`, `tsl::robin_set`, `tsl::sparse_map`, `tsl::sparse_set`,
  `tsl::hopscotch_map`, `tsl::hopscotch_set`, `tsl::bhopscotch_map`, `tsl::bhopscotch_set`, `tsl::htrie_map`, and
  `tsl::htrie_set`.
* [Martin Leitner-Anker's](https://github.com/martinus/robin-hood-hashing) `robin_hood::unordered_map`.

Support for new containers can be added by overloading the given functions and providing a more specialized C++
concept to match the given container.

The purpose of the generic container handling is to allow container types to be substituted in your API. For example,
the smart enumerations in Gluino allow the map type used internally for implementation data to be exchanged via a macro.
Even maps that don't conform to the STL's map interfaces can be supported by overloading these functions. Another
example would be allowing a consumer of your API to specify, via template parameter, what container they want to get
results in.

## "Dumb" Pointer Types
To assist with working with raw pointers, Gluino includes three types, the `raw_ptr`, `safe_ptr`, and `optional_ptr`.
All three are "dumb" in that they do not manage the memory pointed to. The `raw_ptr` looks similar to the STL smart
pointers but without the smarts.

```c++
#include <gluino/ptr.h>

void example() {
    gluino::raw_ptr<Person> p = new Person();
    p->set_name("Bob");
    Person p1 = *p;
}
```

One useful feature of the `raw_ptr` is that it default-initializes the pointer value to `nullptr`. It allows clear null
checking with the `is_null()` function. It also allows fallback retrieval of the pointer with `get_or(T*)`.

Two enhanced versions of the raw pointers are `safe_ptr` and `optional_ptr`. These are intended to add safety and make
explicit to API consumers the guarantees you are putting on pointers to point to valid memory. A `safe_ptr` is a
declaration that any reader of the pointer can assume the safety of the read. Therefore `nullptr` is never an acceptable
value on `safe_ptr`. It cannot be default-initialized, and throws `std::invalid_argument` on any attempt to write a
value of `nullptr` to it. Because writes to the value are checked, reads are assumed safe.

The `optional_ptr` is the inverse of `safe_ptr`, making a statement to API consumers that the pointer can definitely be
`nullptr`. This type is default-initializable (to `nullptr`), and does no validation on writes, but does validate on
reads. Attempting to dereference a `nullptr` will result in `std::bad_optional_access` being thrown. The `is_null()`
function should be checked before any access. The `get_raw()` function is provided as an unchecked read, which can
return even `nullptr`, when the consumer of the pointer can ensure the safety of handling it.

## Complete RNG Engines
Implementing proper random number generation in C++ can be challenging as it requires the combination of several
concepts, including the random engine itself (which provides the PRNG algorithm), the seed device, and a "distribution"
to control how generated values from the engine are mapped across the range of the output type. The `random_engine` is a
single class which combines all of these in a convenient package.

A `random_engine` takes two template parameters. The first is the underlying engine. The second is a mutex which can be
used to enable synchronized access to the same engine. The default mutex type is `gluino::null_mutex`, which does no
synchronization and imposes zero overhead. The underlying engine normally defaults to the STL
`std::default_random_engine` type alias, which is typically `std::mt19937`. Due to the lack of a quality random number
engine in STL, Gluino also has special support for [PCG](https://www.pcg-random.org). If you want all random engines to
default to using PCG, you can define the `GLUINO_OVERRIDE_RANDOM_DEFAULT` macro. If this macro is defined but empty all
engines will default to using `pcg64` (requires you to add PCG to your project). You can also set this macro to specific
engine name to use, e.g. by defining it as `pcg64_k1024` that will become the default engine (this of course works with
any engine, not only PCG ones).

The random engine is designed to properly seed by default. If no parameters are given, the random engine will use
`std::random_device` to generate 8 true random numbers into a `std::seed_seq` and use that to seed the underlying
engine. You can also provide a seed device yourself, and a count of times it should be used to generate a seed sequence.
Note that this requires no heap allocation for seed sequences up to length 32, after which seeds are accumulated on the
heap. You can also provide a variable number of seed arguments which will placed into a seed sequence, or provide a
seed sequence of your own. The engine can be reseeded at any time with the `seed(...)` functions which act identically
to the constructors. Note that the behavior of seeded-well-by-default means that default initialization of a Gluino
random engine is heavyweight. If you intend to seed the engine after construction and do not want this overhead you can
pass a single `0` to the constructor, seeding with a single hard-coded value.

```c++
#include <gluino/random_engine.h>

using namespace gluino;

void example() {
    random_engine<> engine; // Constructs engine seeded with sequence of 8 seeds from std::random_device.
    random_engine<> engine2(1, 2, 3); // Seeds an engine with a seed sequence of 1, 2, and 3.
    random_engine<> engine3(std::random_device(), 4); // Seeds an engine with four seeds from std::random_device.
    std::seed_seq seeds{1, 2, 3, 4};
    random_engine<> engine4(seeds); // Seeds an engine with the given seed sequence.
    random_engine<> trivial(0); // Cheapest way to initialize an engine, use only if you want to seed later with seed().
    
    trivial.seed(); // Seeds the trivial engine with 8 seeds from std::random_device, like default constructor.
    // All the other constructor parameter patterns can also be used...
    trivial.seed(1, 2, 3);
    trivial.seed(std::random_device(), 4);
    trivial.seed(seeds);
}
```

Obtaining random numbers is done with the `next(...)` functions or by the apply operator. The default `next()` and
`operator()()` functions will return a random number from the min to the max of the underlying engine, in the default
result type of the underlying engine, with a uniform distribution. A template parameter can be added to the `next()`
call to get the result as an integer or floating point type. You can add a single parameter to these calls to set a
maximum value. When only a maximum value is provided, a minimum of zero is assumed, rather than the underlying engine's
minimum. You can also provided two values, a minimum and a maximum to get a uniform result between those two values. If
you want to use a specific distribution, you can pass the distribution itself to the call instead.

```c++
#include <gluino/random_engine.h>

using namespace gluino;

void example() {
    random_engine<std::mt19937> engine;
    engine.next(); // Return an int (mt19937 result type) from engine.min() to engine.max().
    engine.next<float_t>(); // Get result as a float this time.
    engine.next(10); // Returns an int from 0 to 10.
    engine.next(-10, 10); // Returns an int from -10 to 10.
    engine.next(0.0, 3.5); // Returns a double from 0 to 3.5.
    engine.next(std::normal_distribution<float_t>()); // Returns a float with default normal distribution.
    
    engine(); // Equivalent to engine.next().
    engine(-10, 10); // Equivalent to engine.next(-10, 10).
    engine(std::normal_distribution<float_t>()); // Equivalent to engine.next(std::normal_distribution<float_t>()).
}
```

Special support has been given for PCG-style backstepping and multistream engines. If the underlying engine supports
a `backstep(int)` operation then that will be exposed on the Gluino random engine as well, in addition to the `advance`
operation which is always present (equivalent to the standard C++ engine `discard` operation). Multistream support will
be present if your underlying engine matches the patterns for PCG stream APIs, i.e. it defines a `stream_state` type
alias and has `stream()` and `set_stream(stream_state)` operations. When these are available `stream()` and
`set_stream(stream_state)` are present on the Gluino random engine. In addition, a new overload for getting the next
random number is available, which takes both a distribution and also a stream state. This operation will get the next
random number for the given stream without permanently altering the default stream. If you wish to use an engine which
supports backstepping or multistreaming, but it does not match the requirements given above, you can implement a
specialization of the `gluino::is_multistreaming_engine` and/or `gluino::is_backstepping_engine` type traits to add
support.

```c++
#include <gluino/random_engine.h>

using namespace gluino;

void example() {
    gluino::random_engine<pcg64> engine; // Create and seed PCG64 random engine, with stream state 0.
    engine.backstep(10); // Backstep by 10.
    engine.set_stream(4); // Switch default stream to stream state 4.
    engine.next(); // Generate uniform random number from stream state 4.
    engine.next(std::uniform_int_distribution(0, 10), 1); // Generate uniform random number from 0 to 10 on stream 1.
    engine.next(); // Generate the next random number on stream 4, since default stream was unchanged in the last call.
}
```

## Synchronization Primitives
Gluino includes several additional synchronization primitives. The `null_mutex` satisfies the requirements of a timed,
shared mutex but all operations are a no-op, allowing it to be used where no synchronization is required. All operations
are constant expressions so they will be omitted entirely by the compiler.

Additionally, three spin locks are provided, the `spin_mutex`, `recursive_spin_mutex`, and `shared_spin_mutex`, which
maps to the STL `std::mutex`, `std::recursive_mutex`, and `std::shared_mutex` respectively. All three provide template
parameters which customize backoff behavior of the mutex. The spin locks will spin for a given amount of cycles before
yielding; after which they will cut their spin time for the next series of wait period by some factor. The cycle count
starts at a particular value and ends at a minimum, as specified by the template.

The `recursive_spin_mutex` and `shared_spin_mutex` can take a customizable data size as a template parameter. Shrinking
the size of these mutexes keeps them smaller, but limits the maximum recursion depth in the `recursive_spin_mutex` and
the maximum number of simultaneous readers in the `shared_spin_mutex`.

## Advanced String Map/Set Containers
Gluino includes the `str_less`, `str_hash`, and `str_equal_to` structures which implement the concepts of a comparer,
hasher, and equality checker for map containers. These structures all take a `bool` template parameter to indicate if
they should treat strings as case-sensitive or not (defaulting to `true`, indicating they are case-sensitive). When
treating strings case-insensitively the comparison is done with no copying.

All comparison is *transparent*, meaning for supported data structures they can compare unlike string types. Supported
here are all strings of the same encoding, whether they be in the form of a C-style character pointer,
`std::basic_string`, or `std::basic_string_view`. This allows e.g. lookup of a value in a `std::set<std::string>` using
a `std::string_view`, without needing to copy the view's data into a new string.

The `str_hash` includes a second template parameter indicating whether to hash using Google's CityHash algorithm. This
is a faster algorithm than the default one (which is based on Boost), and results in better distribution of hash values.
However using it requires the user of Gluino also link against CityHash, which is not a header-only library. CityHash is
only usable for case-sensitive hashing; the template parameter is ignored for case-insensitive hashing. The default
choice of whether to use CityHash or not is determined by the `GLUINO_USE_CITYHASH_BY_DEFAULT` macro. If not customized,
it will default to `false`.

## Minimum-Copy String Conversion
A common concern with string handling in C++ is supporting various encoding options. It can be difficult to support
encoding conversion without also risking unnecessary copying of data for the most common cases where conversion is not
required. Gluino solves this with the `string_cast`, which allows for converting between Unicode strings while avoiding
copying except when required.

Gluino assumes Unicode encoding, and that the character size correlates to the encoding. In other words, it is assumed
that `std::string` and `std::u8string`, and `std::wstring` and `std::u16string`, should be treated as the same. When
converting between these strings of the same size it is handled by returning a reinterpreted reference to the original
string. Likewise, for string views, a reinterpreted view is returned. For C-style strings a string view is created,
which views the C-string reinterpreted.  Whenever converting to a different size, e.g. converting `std::string` to
`std::u32string`, a new string must be created and returned.

```c++
#include <gluino/string_cast.h>

void example() {
    gluino::string_cast<char>(u8"Hello, World!"); // Returns std::string_view.
    gluino::string_cast<char>(L"Hello, World!"); // Returns std::string.
    std::u16string str(u"Hello, World!");
    gluino::string_cast<wchar_t>(str); // Returns std::wstring&.
}
```

Because the string cast may return either a value (a new `basic_string` or `basic_string_view`) or a reference
(`basic_string_view&`), if assigning the result to a variable you should use `decltype(auto)` to handle both value and
reference cases.

```c++
decltype(auto) str = gluino::string_cast<char16_t>("Hello World!");
```

When it comes time to use the resulting data care must still be taken to avoid unnecessary copying. One way to handle
this is to only use operations common between a string value/reference and a string view, e.g. reading the string
content by calling `data()`. If it is necessary to get a `std::basic_string`, you can use the utilities from Gluino's
`utility.h` header. The `gluino::claim()` function takes an argument and claims it as a value or reference that can be
owned. For lvalue references this will simply return the reference. For rvalue references a copy-constructed object is
returned, and for string views and spans a string or vector is returned respectively. For string casting purposes, this
means claiming a string which did not produce a copy when cast will still not result in a copy.  When moving an object
is required, the `gluino::try_move` function can be used. This will move both string values and references, while
claiming and then moving string views (or spans).

```c++
// Example performing absolute minimum number of copies required to cast string and pass it as std::string.
template <class T>
void claim_example(T&& str) {
    decltype(auto) result = gluino::string_cast<char>(std::forward<T>(str));
    call_that_accepts_string(gluino::claim(result));
}

// Example performing absolute minimum copying to cast string to move it somewhere.
template <class T>
void try_move_example(T&& str, std::string& out) {
    decltype(auto) result = gluino::string_cast<char>(std::forward<T>(str));
    out = gluino::try_move(result);
}
```

A `string_cast` defaults to throwing an exception if there is an error during the conversion, e.g. if there is an
incomplete Unicode code point in the source string. On failure it will throw `gluino::bad_string_cast`, which is
derived from `std::bad_cast`. An optional second argument to `string_cast` is a pointer to a boolean value which can
store the success or failure of the call. If a non-`nullptr` value is passed, this value will be set to the success or
failure state of the call (`true` for success, `false` for failure), rather than throwing an exception. The converted
string that is returned will be incomplete, only containing characters up to the point of failure.

## Utilities
### Singletons
Gluino provides a simple singleton implementation using its `singleton` and `lazy_singleton` classes. The `singleton`
statically creates a single, default-initialized instance of an object.

```c++
#include <gluino/singleton.h>

void example() {
    std::string str = gluino::singleton<std::string>::value; // Returns "".
}
```

The `lazy_singleton` accesses values via a function, and lazily creates the object, allowing exceptions to be caught and
avoiding problems with types which cannot be statically initialized at startup. The `value()` function which retrieves
the value can also take arguments, which are passed to the constructor of the type. A unique singleton can exist for
every constructor (but not for different arguments to the same constructor).

```c++
#include <gluino/singleton.h>

void example() {
    std::string str = gluino::lazy_singleton<std::string>::value(); // Returns "".
    str = gluino::lazy_singleton<std::string>::value("Hello, World!"); // Returns "Hello, World!".
    str = gluino::lazy_singleton<std::string>::value("foobar"); // Returns "Hello, World!".
}
```

### Arbitrary Type Mixing
The `mix` class takes a variable number of type parameters and creates a type that inherits publically from all of them.
The result also includes all of the combined constructors. Note that where there may be ambiguous inheritance this
generic type mix may not produce a valid type.

```c++
#include <gluino/mix.h>

void example() {
    gluino::mix<person, employment_data, ssn_data> combined;
    combined.set_name("Bob Smith");
    combined.set_employer("Acme, Inc.");
    combined.set_ssn("123-456-7890");
}
```

### Synchronized Types
The Gluino `synchronized` type allows embedding a mutex into existing non-final types. By default a `std::mutex` is used
but any mutex type can be mixed in. It is in fact a special case of the `mix` class.

```c++
#include <gluino/synchronized.h>

template <class Key, class Value>
using synchronized_map = gluino::synchronized<std::map<Key, Value>>;

template <class Key, class Value>
using spin_synchronized_map = gluino::synchronized<std::map<Key, Value>, gluino::spin_mutex>;
```

The resulting class inherits from the original type, including inheriting all constructors, as well as inheriting from
the mutex and thus having the mutex state and lock/unlock functions built-in to the type.

### Clearer Intentional Discard
When you want to discard a value returned from a function annotated `[[nodiscard]]`, the C++ standard way is to cast the
result to `void`. This is somewhat verbose and unclear. This is clarified with `discard()`.

```c++
gluino::discard(vector.size());
```

### Semantic (and Optimized) Value Claiming/Moving
As previously discussed in the section on `string_cast`, Gluino offers the `claim()` and `try_move()` functions for
dealing with accepting ownership, or trying to move, objects of unknown type and semantics. The `claim()` function will
normally return an lvalue reference if one is passed in. For rvalue references, the rvalue reference is moved to the
resulting return. However, for types with non-owning view semantics (by default including `std::basic_string_view` and
`std::span`), a new copy of their data is returned (`std::basic_string` and `std::vector` respectively). This ensures
that the returned result can be legitimately a type for which ownership can be assumed, with minimal copying. The
`try_move()` function attempts to move a type of unknown ownership. For lvalue references and rvalue references, the
original object is moved, but for non-owning views, a new copy of the contents is returned in the same manner as
`claim()`.

## Type Traits and Concepts
Gluino includes a very large expansion upon the bundled type traits and concepts from the C++ standard library, in every
case providing both a trait and concept for the same idea so that it can be used in either fashion.

# Preprocessor Metaprogramming
Gluino includes some basic preprocessor metaprogramming features. Boolean logic is possible, with conditions. The basic
boolean values are `GLUINO_TRUE` and `GLUINO_FALSE`. These can produced from macros that need to return a boolean, and
provided to the conditionals `GLUINO_IF` and `GLUINO_IF_ELSE`. The additional predicate `GLUINO_IS_EMPTY(...)` returns
`GLUINO_TRUE` if its variadic argument count is zero, or `GLUINO_FALSE` otherwise. `GLUINO_NOT` reverse a true to false
or false to true.

```c++
#include <gluino/preprocessor.h>

GLUINO_IF(GLUINO_TRUE, foo) // Expands to foo.
GLUINO_IF(GLUINO_FALSE, foo) // Expands to nothing.
GLUINO_IF(GLUINO_NOT(GLUINO_FALSE), foo) // Expands to foo.
GLUINO_IF_ELSE(GLUINO_TRUE, foo, bar) // Expands to foo.
GLUINO_IF_ELSE(GLUINO_FALSE, foo, bar) // Expands to bar.
GLUINO_IF_ELSE(GLUINO_IS_EMPTY(), foo, bar) // Expands to foo.
GLUINO_IF_ELSE(GLUINO_IS_EMPTY(1, 2, 3), foo, bar) // Expands to bar.

#define MY_PREDICATE() GLUINO_TRUE
GLUINO_IF(MY_PREDICATE(), foo, bar) // Expands to foo.
```

It is also possible to run function-like macros over variadic argument lists. `GLUINO_MAP` takes a variadic argument
list, preceded by a function macro and a context argument (passed to each callback), and produces a comma-separated
list of each element in the original argument list applied to the callback macro.

```c++
#define ADD(x, c) (x + c)
GLUINO_MAP(ADD, 2, 1, 2, 3, 4) // Expands to 3, 4, 5, 6, because it runs ADD(x, 2) on each element.
```

For separators other than commas, `GLUINO_MAP_JOIN` can be used. This accepts an additional delimiter argument.

```c++
#define ADD(x, c) (x + c)
GLUINO_MAP_JOIN(ADD, 2, ;, 1, 2, 3, 4) // Expands to 3; 4; 5; 6.
```

The `GLUINO_MAP_CONCAT` macro has no delimiter argument, and simply has empty space between each output of the callback.

It is also possible to return the length of a variadic argument list with `GLUINO_COUNT`.

```c++
GLUINO_COUNT() // Expands to 0.
GLUINO_COUNT(foo) // Expands to 1.
GLUINO_COUNT(foo, bar, baz) // Expands to 3.
```

Note that all variadic argument operations are limited to a maximum variadic argument length of 2048. It is impossible
to handle arbitrary-length lists of arguments since the C/C++ preprocessor is not Turing-complete.

Other list operations, which allow recursive behavior, are `GLUINO_FIRST` and `GLUINO_REST`. This are similar to the
LISP `car` and `cdr` operations.

```c++
GLUINO_FIRST(1, 2, 3, 4) // Expands to 1.
GLUINO_REST(1, 2, 3, 4) // Expands to 2, 3, 4.
GLUINO_REST(4) // Expands to nothing.
GLUINO_REST() // Expands to nothing.
```

Another useful function is to flatten a nested parenthized list of tokens with `GLUINO_FLATTEN`. These can then be run
against another macro with `GLUINO_APPLY`.

```c++
#define ADD(x, y) x + y

GLUINO_FLATTEN((1, 2)) // Expands to 1, 2.
GLUINO_APPLY(ADD, GLUINO_FLATTEN((1, 2))) // Expands to 1 + 2.
```

Various combinations of these functions can be used to create extremely powerful and complex code generation templates,
as the example of Gluino's smart enumerations shows.
