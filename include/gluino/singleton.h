#pragma once

#include <concepts>

namespace gluino {
    /**
     * Defines a simple, globally-accessible singleton instance of a class, which is eagerly created on module
     * initialization.
     *
     * @tparam T The type of object to obtain a singleton instance of.
     */
    template <class T>
    class singleton {
    public:
        static inline T value{};
    };

    /**
     * Defines a simple, globally-accessible singleton instance of a class, which is lazily created on first access.
     *
     * <p>
     * Note that it is possible to provide constructor arguments to the request for the object. Multiple singletons,
     * unique to each tuple of argument types, can be created for a given instantiated lazy singleton class, however
     * these will not be unique by the argument <i>values</i>. If you need to separate two instances with different
     * argument values the <code>Tag</code> parameter can be used to distinguish between two unique instantiations of
     * the lazy singleton template.
     * </p>
     *
     * @tparam T The type of object to obtain a singleton instance of.
     * @tparam Tag A type which can be used to instantiate a separate instance of the singleton class. This defaults to
     * <code>void</code>, but all lazy singleton object instances will be unique for a given type provided here. To
     * distinguish between two singleton instances, you can provide a custom type as a tag.
     */
    template <class T, class Tag = void>
    class lazy_singleton {
    public:
        [[nodiscard]] static inline T& value() {
            static T value;
            return value;
        }

        template <class... Args>
        [[nodiscard]] static inline T& value(Args... args) {
            static T value(args...);
            return value;
        }
    };
}
