#pragma once

#include "concepts.h"
#include "ptr.h"
#include "typeinfo.h"
#include "abi/abi.h"

namespace gluino {
    namespace detail {
        class polymorphic {
        public:
            virtual ~polymorphic() = default;
        };

#if defined(GLUINO_ABI_USE_ITANIUM)
        [[nodiscard]] inline bool itanium_search(
                const ::std::type_info* source_type, const ::std::type_info* dest_type) noexcept {
            while (source_type != dest_type) {
                if (::gluino::type_info<>(source_type) == ::gluino::type_info<::gluino::abi::itanium::detail::si_class>()) {
                    source_type = reinterpret_cast<const ::gluino::abi::itanium::si_class_type_info*>(source_type)->base_type();
                } else if (::gluino::type_info<>(source_type) == ::gluino::type_info<::gluino::abi::itanium::detail::vmi_class>()) {
                    auto* vmi_type = reinterpret_cast<const ::gluino::abi::itanium::vmi_class_type_info*>(source_type);
                    for (unsigned int i = 0; i < vmi_type->base_count(); ++i) {
                        const auto* base = vmi_type->base_info(i);
                        if (base->flags().none(::gluino::abi::itanium::offset_flags::enum_type::is_public)) {
                            continue;
                        }
                        if (itanium_search(base->base_type(), dest_type)) {
                            return true;
                        }
                    }
                } else {
                    return false;
                }
            }
            return true;
        }
#endif
    }

    template <::gluino::polymorphic_pointer Out, ::gluino::polymorphic In>
    requires ::gluino::related<Out, In>
    [[nodiscard]] constexpr ::gluino::optional_ptr<::std::remove_pointer_t<Out>> virtual_cast(In* source) noexcept {
        return dynamic_cast<Out>(source);
    }

    template <::gluino::polymorphic_pointer Out, ::gluino::polymorphic In>
    requires ::gluino::related<Out, In>
    [[nodiscard]] constexpr ::gluino::optional_ptr<::std::remove_pointer_t<Out>> virtual_cast(In& source) noexcept {
        return dynamic_cast<Out>(&source);
    }

    template <::gluino::polymorphic_reference Out, ::gluino::polymorphic In>
    requires ::gluino::related<Out, In>
    [[nodiscard]] constexpr Out virtual_cast(In* source) noexcept {
        return dynamic_cast<Out>(source);
    }

    template <::gluino::polymorphic_reference Out, ::gluino::polymorphic In>
    requires ::gluino::related<Out, In>
    [[nodiscard]] constexpr Out virtual_cast(In& source) noexcept {
        return dynamic_cast<Out>(&source);
    }

    template <::gluino::polymorphic_pointer Out, ::gluino::polymorphic In>
    [[nodiscard]] ::gluino::optional_ptr<::std::remove_pointer_t<Out>> virtual_cast(In* source) noexcept {
#if defined(GLUINO_ABI_USE_ITANIUM)
        const auto* source_type = &::gluino::type_info<>(*source);
        const auto* dest_type = &::gluino::type_info<::std::remove_cv_t<::std::remove_pointer_t<Out>>>();
        return ::gluino::detail::itanium_search(source_type, dest_type) ? reinterpet_cast<Out>(source) : nullptr;
#elif defined(GLUINO_ABI_USE_MSVC)
        const auto* locator = ::gluino::abi::msvc::complete_object_locator::from(source);
        ::std::type_index to_index(::gluino::type_index<::std::remove_pointer_t<::std::remove_cv_t<Out>>>());
        const auto class_descriptor = locator->class_hierarchy_descriptor();
        for (uint32_t i = 0; i < class_descriptor.base_class_descriptor_count(); ++i) {
            const auto base_class = class_descriptor.base_class_descriptor(i);
            const auto& base_type = base_class.type_descriptor();
            if (to_index == ::std::type_index(base_type) &&
                base_class.attributes().none(
                         ::gluino::abi::msvc::base_class_flags::enum_type::not_visible,
                         ::gluino::abi::msvc::base_class_flags::enum_type::is_private,
                         ::gluino::abi::msvc::base_class_flags::enum_type::private_or_protected_base)) {
                return reinterpret_cast<Out>(source);
            }
        }
        return nullptr;
#else
        static_assert(false, "Unsupported ABI for virtual_cast.");
#endif
    }

    template <::gluino::polymorphic_pointer Out, ::gluino::polymorphic In>
    [[nodiscard]] inline ::gluino::optional_ptr<Out> virtual_cast(In& source) noexcept {
        return virtual_cast<Out>(&source);
    }

    template <::gluino::polymorphic_reference Out, ::gluino::polymorphic In>
    [[nodiscard]] inline Out virtual_cast(In* source) {
        auto result = virtual_cast<::std::add_pointer_t<::std::remove_reference_t<Out>>>(source);
        if (result.empty()) {
            throw ::std::bad_cast();
        }
        return result.value_raw();
    }

    template <::gluino::polymorphic_reference Out, ::gluino::polymorphic In>
    [[nodiscard]] inline Out virtual_cast(In& source) {
        return virtual_cast<Out>(&source);
    }

    template <::gluino::polymorphic_pointer Out, class T>
    requires (::std::is_pointer_v<T> && ::std::is_void_v<::std::decay_t<::std::remove_pointer_t<T>>>)
    [[nodiscard]] inline ::gluino::optional_ptr<::std::remove_pointer_t<Out>> force_virtual_cast(T source) noexcept {
        return virtual_cast<Out>(reinterpret_cast<::gluino::match_cv_t<::gluino::detail::polymorphic*, T>>(source));
    }

    template <::gluino::polymorphic_reference Out, class T>
    requires (::std::is_pointer_v<T> && ::std::is_void_v<::std::decay_t<::std::remove_pointer_t<T>>>)
    [[nodiscard]] inline Out force_virtual_cast(T source) {
        return virtual_cast<Out>(reinterpret_cast<::gluino::match_cv_t<::gluino::detail::polymorphic*, T>>(source));
    }
}
