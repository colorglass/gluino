#pragma once

#include <string>
#include <string_view>

#include "concepts.h"

// Define the rules for using particular hashing algorithms, if available.
#ifdef GLUINO_USE_XXHASH
#define __GLUINO_USE_XXHASH
#define __GLUINO_USE_XXHASH_CASEINSENSITIVE
#elifdef GLUINO_USE_METROHASH
#define __GLUINO_USE_METROHASH
#define __GLUINO_USE_METROHASH_CASEINSENSITIVE
#elifdef GLUINO_USE_FARMHASH
#define __GLUINO_USE_FARMHASH
#elifdef GLUINO_USE_CITYHASH
#define __GLUINO_USE_CITYHASH
#elifdef XXHASH_H_5627135585666179
#define __GLUINO_USE_XXHASH
#ifdef METROHASH_METROHASH_64_H
#define __GLUINO_USE_METROHASH_CASEINSENSITIVE
#else
#define __GLUINO_USE_XXHASH_CASEINSENSITIVE
#endif
#elifdef METROHASH_METROHASH_64_H
#define __GLUINO_USE_METROHASH
#define __GLUINO_USE_METROHASH_CASEINSENSITIVE
#elifdef FARM_HASH_H_
#define __GLUINO_USE_FARMHASH
#elifdef CITY_HASH_H_
#define __GLUINO_USE_CITYHASH
#endif

namespace gluino {
    /**
     * Defines a comparison function-like object specifically for handling strings, both case-sensitive and not.
     *
     * <p>
     * This comparator is fully transparent for all C++ string types (strings, string views, and C-style strings),
     * allowing comparison across types.
     * </p>
     *
     * @tparam CaseSensitive Whether the string comparison is case-sensitive.
     */
    template <bool CaseSensitive = true>
    struct str_less {
        using is_transparent [[maybe_unused]] = void;

        template <::gluino::char_like Char, class Traits = ::std::char_traits<Char>,
                class AllocA = ::std::allocator<Char>, class AllocB = ::std::allocator<Char>>
        [[nodiscard]] inline bool operator()(const ::std::basic_string<Char, Traits, AllocA>& a,
                                             const ::std::basic_string<Char, Traits, AllocB>& b) const noexcept {
            return operator()(a.c_str(), b.c_str());
        }

        template <::gluino::char_like Char, class Traits = ::std::char_traits<Char>,
                class Alloc = ::std::allocator<Char>>
        [[nodiscard]] inline bool operator()(const char* a,
                                             const ::std::basic_string<Char, Traits, Alloc>& b) const noexcept {
            return operator()(a, b.c_str());
        }

        template <::gluino::char_like Char, class Traits = ::std::char_traits<Char>,
                class Alloc = ::std::allocator<Char>>
        [[nodiscard]] inline bool operator()(const ::std::basic_string<Char, Traits, Alloc>& a,
                                             const Char* b) const noexcept {
            return operator()(a.c_str(), b);
        }

        template <::gluino::char_like Char, class Traits = ::std::char_traits<Char>,
                class Alloc = ::std::allocator<Char>>
        [[nodiscard]] inline bool operator()(const ::std::basic_string<Char, Traits, Alloc>& a,
                                             ::std::basic_string_view<Char, Traits> b) const noexcept {
            return operator()(a.c_str(), b.data());
        }

        template <::gluino::char_like Char, class Traits = ::std::char_traits<Char>,
                class Alloc = ::std::allocator<Char>>
        [[nodiscard]] inline bool operator()(const ::std::basic_string_view<Char, Traits> a,
                                             const ::std::basic_string<Char, Traits, Alloc>& b) const noexcept {
            return operator()(a.data(), b.c_str());
        }

        template <::gluino::char_like Char, class Traits = ::std::char_traits<Char>>
        [[nodiscard]] inline bool operator()(::std::basic_string_view<Char, Traits> a,
                                             ::std::basic_string_view<Char, Traits> b) const noexcept {
            return operator()(a.data(), b.data());
        }

        template <::gluino::char_like Char, class Traits = ::std::char_traits<Char>>
        [[nodiscard]] inline bool operator()(const char* a,
                                             ::std::basic_string_view<Char, Traits> b) const noexcept {
            return operator()(a, b.data());
        }

        template <::gluino::char_like Char, class Traits = ::std::char_traits<Char>>
        [[nodiscard]] inline bool operator()(::std::basic_string_view<Char, Traits> a,
                                             const Char* b) const noexcept {
            return operator()(a.data(), b);
        }

        template <::gluino::char_like Char>
        [[nodiscard]] inline bool operator()(const Char* a, const Char* b) const noexcept {
            for (; *a != Char() && *b != Char(); ++a, ++b) {
                if constexpr (CaseSensitive) {
                    if (*a < *b) {
                        return true;
                    } else if (*a > *b) {
                        return false;
                    }
                } else {
                    auto upperA = ::std::toupper(*a);
                    auto upperB = ::std::toupper(*b);
                    if (upperA < upperB) {
                        return true;
                    } else if (upperA > upperB) {
                        return false;
                    }
                }
            }
            return *a == *b ? false : *a == Char();
        }
    };

    template <bool CaseSensitive = true>
    struct str_equal_to {
        using is_transparent [[maybe_unused]] = void;

        template <::gluino::char_like Char, class Traits = ::std::char_traits<Char>,
                class AllocA = ::std::allocator<Char>, class AllocB = ::std::allocator<Char>>
        [[nodiscard]] inline bool operator()(const ::std::basic_string<Char, Traits, AllocA>& a,
                                             const ::std::basic_string<Char, Traits, AllocB>& b) const noexcept {
            return operator()(a.c_str(), b.c_str());
        }

        template <::gluino::char_like Char, class Traits = ::std::char_traits<Char>,
                class Alloc = ::std::allocator<Char>>
        [[nodiscard]] inline bool operator()(const Char* a,
                                             const ::std::basic_string<Char, Traits, Alloc>& b) const noexcept {
            return operator()(a, b.c_str());
        }

        template <::gluino::char_like Char, class Traits = ::std::char_traits<Char>,
                class Alloc = ::std::allocator<Char>>
        [[nodiscard]] inline bool operator()(const ::std::basic_string<Char, Traits, Alloc>& a,
                                             const Char* b) const noexcept {
            return operator()(a.c_str(), b);
        }

        template <::gluino::char_like Char, class Traits = ::std::char_traits<Char>,
                class Alloc = ::std::allocator<Char>>
        [[nodiscard]] inline bool operator()(const ::std::basic_string<Char, Traits, Alloc>& a,
                                             ::std::basic_string_view<Char, Traits> b) const noexcept {
            return operator()(a.c_str(), b.data());
        }

        template <::gluino::char_like Char, class Traits = ::std::char_traits<Char>,
                class Alloc = ::std::allocator<Char>>
        [[nodiscard]] inline bool operator()(::std::basic_string_view<Char, Traits> a,
                                             const ::std::basic_string<Char, Traits, Alloc>& b) const noexcept {
            return operator()(a.data(), b.c_str());
        }

        template <::gluino::char_like Char, class Traits = ::std::char_traits<Char>>
        [[nodiscard]] inline bool operator()(::std::basic_string_view<Char, Traits> a,
                                             ::std::basic_string_view<Char, Traits> b) const noexcept {
            return operator()(a.data(), b.data());
        }

        template <::gluino::char_like Char, class Traits = ::std::char_traits<Char>>
        [[nodiscard]] inline bool operator()(const Char* a,
                                             ::std::basic_string_view<Char, Traits> b) const noexcept {
            return operator()(a, b.data());
        }

        template <::gluino::char_like Char, class Traits = ::std::char_traits<Char>>
        [[nodiscard]] inline bool operator()(::std::basic_string_view<Char, Traits> a,
                                             const Char* b) const noexcept {
            return operator()(a.data(), b);
        }

        template <::gluino::char_like Char>
        [[nodiscard]] bool operator()(const Char* a, const Char* b) const noexcept {
            if (!a || !b) {
                return a == b;
            }
            if (a == b) {
                return true;
            }
            for (; *a != Char() && *b != Char(); ++a, ++b) {
                if constexpr (CaseSensitive) {
                    if (*a != *b) {
                        return false;
                    }
                } else {
                    if (::std::toupper(*a) != ::std::toupper(*b)) {
                        return false;
                    }
                }
            }
            return *a == *b;
        }
    };

    template <bool CaseSensitive = true>
    struct str_hash {
        using is_transparent [[maybe_unused]] = void;
        using transparent_key_equal [[maybe_unused]] = ::gluino::str_equal_to<CaseSensitive>;

        template <::gluino::char_like Char, class Traits = ::std::char_traits<Char>>
        [[nodiscard]] inline ::std::size_t operator()(::std::basic_string_view<Char, Traits> value) const noexcept {
            return hash(value.data(), value.size());
        }

        template <::gluino::char_like Char, class Traits = ::std::char_traits<Char>,
                class Alloc = ::std::allocator<Char>>
        [[nodiscard]] inline ::std::size_t operator()(
                const ::std::basic_string<Char, Traits, Alloc>& value) const noexcept {
            return hash(value.c_str(), value.size());
        }

        template <class Traits>
        [[nodiscard]] inline ::std::size_t operator()(::std::basic_string_view<char, Traits> value) const noexcept {
            return hash(value.data(), value.size());
        }

        template <class Traits>
        [[nodiscard]] inline ::std::size_t operator()(::std::basic_string_view<char8_t, Traits> value) const noexcept {
            return operator()(reinterpret_cast<::std::basic_string_view<char>&>(value));
        }

        template <class Traits, class Alloc = ::std::allocator<char>>
        [[nodiscard]] inline ::std::size_t operator()(
                const ::std::basic_string<char, Traits, Alloc>& value) const noexcept {
            return hash(value.data(), value.size());
        }

        template <class Traits, class Alloc = ::std::allocator<char8_t>>
        [[nodiscard]] inline ::std::size_t operator()(
                const ::std::basic_string<char8_t, Traits, Alloc>& value) const noexcept {
            return operator()(reinterpret_cast<const char*>(value.c_str()));
        }

        template <::gluino::char_like Char>
        [[nodiscard]] inline ::std::size_t operator()(const Char* value) const noexcept {
            return hash(value, ::std::basic_string_view(value).size());
        }

        [[nodiscard]] inline ::std::size_t operator()(const char* value) const noexcept {
            return hash(value, strlen(value));
        }

        [[nodiscard]] inline ::std::size_t operator()(const char8_t* value) const noexcept {
            return operator()(reinterpret_cast<const char*>(value));
        }

    private:
        template <class Char>
        [[nodiscard]] inline ::std::size_t hash(const Char* value, [[maybe_unused]] ::std::size_t size) const noexcept {
            if constexpr (CaseSensitive) {
#ifdef __GLUINO_USE_XXHASH
                return XXH64(value, size);
#elif __GLUINO_USE_METROHASH
                ::std::size_t result;
                MetroHash64::Hash(value, size, reinterpret_cast<uint8_t*>(&result));
                return result;
#elif __GLUINO_USE_FARMHASH
                return NAMESPACE_FOR_HASH_FUNCTIONS::Hash64(value, size);
#elifdef __GLUINO_USE_CITYHASH
                return CityHash64(value, size);
#else
                return basic_hash(value);
#endif
            } else {
#ifdef __GLUINO_USE_METROHASH_CASEINSENSITIVE
                ::std::size_t result;
                MetroHash64 hash;
                hash.Initialize();
                Char buffer[256];
                ::std::size_t pos = 0;
                while (pos < size) {
                    ::std::size_t buffer_pos = 0;
                    while (pos < size && buffer_pos < 256) {
                        buffer[buffer_pos++] = ::std::toupper(value[pos++]);
                    }
                    hash.Update(buffer, buffer_pos);
                }
                hash.Finalize(reinterpret_cast<uint8_t*>(&result));
                return result;
#elifdef __GLUINO_USE_XXHASH_CASEINSENSITIVE
                auto* const state = XXH64_createState();
                XXH64_reset(state, 0);
                Char buffer[256];
                ::std::size_t pos = 0;
                while (pos < size) {
                    ::std::size_t buffer_pos = 0;
                    while (pos < size && buffer_pos < 256) {
                        buffer[buffer_pos++] = ::std::toupper(value[pos++]);
                    }
                    XXH64_update(state, buffer, buffer_pos);
                }
                return static_cast<::std::size_t>(XXH64_digest(state));
#else
                return basic_hash(value);
#endif
            }
        }

        template <class Char>
        [[nodiscard]] ::std::size_t basic_hash(const Char* value) const noexcept {
            ::std::size_t seed = 0;
            if (!value) {
                return seed;
            }
            for (; *value != Char(); ++value) {
                // Based on the hash function from Boost.
                if constexpr (CaseSensitive) {
                    seed ^= *value + 0x9e3779b9 + (seed << 6) + (seed >> 2);
                } else {
                    seed ^= ::std::toupper(*value) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
                }
            }
            return seed;
        }
    };
}
