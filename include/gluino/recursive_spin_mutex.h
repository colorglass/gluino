#pragma once

#include <atomic>

#include "sleep_policy.h"

namespace gluino {
    template <::gluino::sleep_policy SleepPolicy = ::gluino::null_sleep_policy,
            ::std::unsigned_integral Count = uint32_t>
    class recursive_spin_mutex {
    public:
        inline recursive_spin_mutex() noexcept = default;

        recursive_spin_mutex(const recursive_spin_mutex&) = delete;

        recursive_spin_mutex(recursive_spin_mutex&&) = delete;

        void lock() noexcept {
            auto this_id = ::std::this_thread::get_id();
            SleepPolicy sleeper;
            while (true) {
                auto owner = _owner.load(::std::memory_order::relaxed);
                if (owner == this_id) {
                    ++_count;
                    return;
                }
                if (owner == ::std::thread::id() && _owner.compare_exchange_strong(owner, this_id)) {
                    ++_count;
                    return;
                }
                sleeper();
            }
        }

        [[nodiscard]] bool try_lock() noexcept {
            auto this_id = ::std::this_thread::get_id();
            auto owner = _owner.load(::std::memory_order::relaxed);
            if (owner == this_id) {
                return true;
            }
            if (owner == ::std::thread::id()) {
                auto result = _owner.compare_exchange_strong(owner, this_id);
                ++_count;
                return result;
            }
            return false;
        }

        inline void unlock() noexcept {
            if (--_count == 0) {
                _owner.store(::std::thread::id(), ::std::memory_order_release);
            }
        }

        [[nodiscard]] static constexpr ::std::size_t max_recursions() noexcept {
            return ~Count(0) - 1;
        }

    private:
        ::std::atomic<::std::thread::id> _owner;
        Count _count{0};
    };
}
