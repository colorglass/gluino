#pragma once

#include "concepts.h"

namespace gluino {
    template <class T>
    constexpr void reserve(T&, ::std::size_t) noexcept {
    }

    template <::gluino::reservable T>
    constexpr void reserve(T& object, ::std::size_t size) noexcept {
        object.reserve(size);
    }

    template <class T, class... Args>
    requires (::gluino::back_emplaceable<T, Args...> && ::gluino::iterable<T>)
    inline typename T::iterator insert(T& object, Args&&... args) noexcept {
        object.emplace_back(::std::forward<Args>(args)...);
        auto iter = object.end();
        return --iter;
    }

    template <class T, class... Args>
    requires (::gluino::front_emplaceable<T, Args...> && !::gluino::back_emplaceable<T, Args...> &&
            ::gluino::iterable<T>)
    inline typename T::iterator insert(T& object, Args&&... args) noexcept {
        object.emplace_front(::std::forward<Args>(args)...);
        return object.begin();
    }

    template <class T, class... Args>
    requires (::gluino::set_emplaceable<T, Args...> && ::gluino::iterable<T>)
    inline typename T::iterator insert(T& object, Args&&... args) noexcept {
        return object.emplace(::std::forward<Args>(args)...).first;
    }

    template <class T, class... Args>
    requires ::gluino::back_emplaceable<T, Args...>
    inline void emplace(T& object, Args&&... args) noexcept {
        object.emplace_back(::std::forward<Args>(args)...);
    }

    template <class T, class... Args>
    requires (::gluino::front_emplaceable<T, Args...> && !::gluino::back_emplaceable<T, Args...>)
    inline void emplace(T& object, Args&&... args) noexcept {
        object.emplace_front(::std::forward<Args>(args)...);
    }

    template <class T, class... Args>
    requires ::gluino::set_emplaceable<T, Args...>
    inline void emplace(T& object, Args&&... args) noexcept {
        object.emplace(::std::forward<Args>(args)...);
    }

    template <::gluino::clearable T>
    inline void clear(T& object) noexcept {
        object.clear();
    }

    template <class T>
    requires (!::gluino::clearable<T> && ::std::is_assignable_v<T, T> && ::std::default_initializable<T>)
    inline void clear(T& object) noexcept {
        object = T();
    }
}
