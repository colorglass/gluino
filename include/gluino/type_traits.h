#pragma once

#include <functional>
#include <memory>
#include <string>
#include <type_traits>

namespace gluino {
    template <auto V, class... T>
    constexpr auto dependent_value = V;

    template <class... T>
    constexpr bool dependent_false = ::gluino::dependent_value<false, T...>;

    namespace detail {
        template <class T>
        concept has_value_type = requires {
            typename T::value_type;
        };

        template <class T>
        concept has_element_type = requires {
            typename T::element_type;
        };

        template <class T>
        void value_type_impl() {
        }

        template <::gluino::detail::has_value_type T>
        typename T::value_type value_type_impl() {
        }

        template <::gluino::detail::has_element_type T>
        requires (!::gluino::detail::has_value_type<T>)
        typename T::element_type value_type_impl() {
        }

        template <class T>
        void element_type_impl() {
        }

        template <::gluino::detail::has_element_type T>
        typename T::element_type element_type_impl() {
        }

        template <::gluino::detail::has_value_type T>
        requires (!::gluino::detail::has_element_type<T>)
        typename T::value_type element_type_impl() {
        }
    }


    // TODO:
    template <class T>
    struct type_exists {
        static constexpr bool value = true;
    };

    template <class T>
    static constexpr bool type_exists_v = ::gluino::type_exists<T>::value;


    // Properties of structures.
    template <class Base, class T>
    struct is_subtype {
        static constexpr bool value = ::std::is_base_of_v<Base, T> || ::std::is_same_v<Base, T>;
    };

    template <class Base, class T>
    static constexpr bool is_subtype_v = ::gluino::is_subtype<Base, T>::value;

    template <class T>
    struct value_type {
        using type = decltype(::gluino::detail::value_type_impl<T>());
    };

    template <class Char>
    struct value_type<Char*> {
        using type = Char;
    };

    template <class Char, ::std::size_t Length>
    struct value_type<Char[Length]> {
        using type = Char;
    };

    template <class T>
    using value_type_t = typename ::gluino::value_type<T>::type;


    template <class T>
    struct element_type {
        using type = decltype(::gluino::detail::element_type_impl<T>());
    };

    template <class T>
    using element_type_t = typename ::gluino::element_type<T>::type;


    template <class T>
    struct traits_type {
        using type = typename T::traits_type;
    };

    template <class Char>
    struct traits_type<Char*> {
        using type = ::std::char_traits<Char>;
    };

    template <class Char, ::std::size_t Length>
    struct traits_type<Char[Length]> {
        using type = ::std::char_traits<Char>;
    };

    template <class T>
    using traits_type_t = typename ::gluino::traits_type<T>::type;


    template <class T>
    struct allocator_type {
        using type = typename T::allocator_type;
    };

    template <class Char>
    struct allocator_type<Char*> {
        using type = ::std::allocator<Char>;
    };

    template <class Char, ::std::size_t Length>
    struct allocator_type<Char[Length]> {
        using type = ::std::allocator<Char>;
    };

    template <class T>
    using allocator_type_t = typename ::gluino::allocator_type<T>::type;


    template <class T>
    struct key_type {
        using type = typename T::key_type;
    };

    template <class T>
    using key_type_t = typename ::gluino::key_type<T>::type;


    template <class T>
    struct key_equal_type {
        using type = typename T::key_equal;
    };

    template <class T>
    using key_equal_type_t = typename ::gluino::key_equal_type<T>::type;


    template <class T>
    struct key_compare_type {
        using type = typename T::key_compare;
    };

    template <class T>
    using key_compare_type_t = typename ::gluino::key_compare_type<T>::type;


    template <class T>
    struct value_compare_type {
        using type = typename T::value_compare;
    };

    template <class T>
    using value_compare_type_t = typename ::gluino::value_compare_type<T>::type;


    template <class T>
    struct hasher_type {
        using type = typename T::hasher;
    };

    template <class T>
    using hasher_type_t = typename ::gluino::hasher_type<T>::type;


    template <class T>
    struct size_type {
        using type = typename T::size_type;
    };

    template <class Char>
    struct size_type<Char*> {
        using type = ::std::size_t;
    };

    template <class Char, ::std::size_t Length>
    struct size_type<Char[Length]> {
        using type = ::std::size_t;
    };

    template <class T>
    using size_type_t = typename ::gluino::size_type<T>::type;


    template <class T>
    struct is_indirection : public ::std::false_type {
    };

    template <class T>
    struct is_indirection<T*> : public ::std::true_type {
    };

    template <class T>
    struct is_indirection<T* const> : public ::std::true_type {
    };

    template <class T>
    struct is_indirection<T* volatile> : public ::std::true_type {
    };

    template <class T>
    struct is_indirection<T* const volatile> : public ::std::true_type {
    };

    template <class T>
    struct is_indirection<T&> : public ::std::true_type {
    };

    template <class T>
    struct is_indirection<T&&> : public ::std::true_type {
    };

    template <class T>
    static constexpr bool is_indirection_v = ::gluino::is_indirection<T>::value;


    template <class T>
    struct is_smart_pointer : public ::std::false_type {
    };

    template <class T>
    struct is_smart_pointer<::std::unique_ptr<T>> : public ::std::true_type {
    };

    template <class T>
    struct is_smart_pointer<::std::shared_ptr<T>> : public ::std::true_type {
    };

    template <class T>
    static constexpr bool is_smart_pointer_v = ::gluino::is_smart_pointer<T>::value;


    template <class T>
    struct is_pointer_like {
        static constexpr bool value = ::std::disjunction_v<::gluino::is_smart_pointer<T>, ::std::is_pointer<T>>;
    };

    template <class T>
    static constexpr bool is_pointer_like_v = ::gluino::is_pointer_like<T>::value;


    template <class T>
    struct remove_pointer_like {
        using type = ::std::conditional_t<::gluino::is_pointer_like_v<T>,
                ::gluino::element_type_t<T>, ::std::remove_pointer_t<T>>;
    };

    template <class T>
    using remove_pointer_like_t = typename ::gluino::remove_pointer_like<T>::type;


    template <class T>
    struct remove_pointer_recursive {
        using type = T;
    };

    template <class T>
    struct remove_pointer_recursive<T*> : public ::gluino::remove_pointer_recursive<T> {
    };

    template <class T>
    struct remove_pointer_recursive<T* const> : public ::gluino::remove_pointer_recursive<T> {
    };

    template <class T>
    struct remove_pointer_recursive<T* volatile> : public ::gluino::remove_pointer_recursive<T> {
    };

    template <class T>
    struct remove_pointer_recursive<T* const volatile> : public ::gluino::remove_pointer_recursive<T> {
    };

    template <class T>
    using remove_pointer_recursive_t = typename ::gluino::remove_pointer_recursive<T>::type;


    template <class T>
    struct is_indirection_like {
        static constexpr bool value = ::std::disjunction_v<::gluino::is_indirection<T>, ::gluino::is_smart_pointer<T>>;
    };

    template <class T>
    static constexpr bool is_indirection_like_v = ::gluino::is_indirection_like<T>::value;


    template <class T>
    struct is_polymorphic_pointer : public ::std::false_type {
    };

    template <class T>
    struct is_polymorphic_pointer<T*> {
        static constexpr bool value = ::std::is_polymorphic_v<::std::remove_cv_t<::std::remove_pointer_t<T>>>;
    };

    template <class T>
    struct is_polymorphic_pointer<T* const> {
        static constexpr bool value = ::std::is_polymorphic_v<::std::remove_cv_t<::std::remove_pointer_t<T>>>;
    };

    template <class T>
    struct is_polymorphic_pointer<T* volatile> {
        static constexpr bool value = ::std::is_polymorphic_v<::std::remove_cv_t<::std::remove_pointer_t<T>>>;
    };

    template <class T>
    struct is_polymorphic_pointer<T* const volatile> {
        static constexpr bool value = ::std::is_polymorphic_v<::std::remove_cv_t<::std::remove_pointer_t<T>>>;
    };

    template <class T>
    static constexpr bool is_polymorphic_pointer_v = ::gluino::is_polymorphic_pointer<T>::value;


    template <class T>
    struct is_polymorphic_reference : public ::std::false_type {
    };

    template <class T>
    struct is_polymorphic_reference<T&> {
        static constexpr bool value = ::std::is_polymorphic_v<::std::remove_cvref_t<T>>;
    };

    template <class T>
    struct is_polymorphic_reference<T&&> {
        static constexpr bool value = ::std::is_polymorphic_v<::std::remove_cvref_t<T>>;
    };

    template <class T>
    static constexpr bool is_polymorphic_reference_v = ::gluino::is_polymorphic_reference<T>::value;


    template <class T>
    struct is_polymorphic_indirection : public ::std::false_type {
    };

    template <class T>
    struct is_polymorphic_indirection<T*> {
        static constexpr bool value = ::std::is_polymorphic_v<::std::remove_cv_t<::std::remove_pointer_t<T>>>;
    };

    template <class T>
    struct is_polymorphic_indirection<T* const> {
        static constexpr bool value = ::std::is_polymorphic_v<::std::remove_cv_t<::std::remove_pointer_t<T>>>;
    };

    template <class T>
    struct is_polymorphic_indirection<T* volatile> {
        static constexpr bool value = ::std::is_polymorphic_v<::std::remove_cv_t<::std::remove_pointer_t<T>>>;
    };

    template <class T>
    struct is_polymorphic_indirection<T* const volatile> {
        static constexpr bool value = ::std::is_polymorphic_v<::std::remove_cv_t<::std::remove_pointer_t<T>>>;
    };

    template <class T>
    struct is_polymorphic_indirection<T&> {
        static constexpr bool value = ::std::is_polymorphic_v<::std::remove_cvref_t<T>>;
    };

    template <class T>
    struct is_polymorphic_indirection<T&&> {
        static constexpr bool value = ::std::is_polymorphic_v<::std::remove_cvref_t<T>>;
    };

    template <class T>
    static constexpr bool is_polymorphic_indirection_v = ::gluino::is_polymorphic_indirection<T>::value;


    template <class T>
    struct is_ultimately_polymorphic {
        static constexpr bool value = ::std::is_polymorphic_v<::std::remove_cv_t<T>>;
    };

    template <class T>
    struct is_ultimately_polymorphic<T*> {
        static constexpr bool value = ::gluino::is_ultimately_polymorphic<
                ::std::remove_cv_t<::std::remove_pointer_t<T>>>::value;
    };

    template <class T>
    struct is_ultimately_polymorphic<T&> {
        static constexpr bool value = ::gluino::is_ultimately_polymorphic<::std::remove_cvref_t<T>>::value;
    };

    template <class T>
    static constexpr bool is_ultimately_polymorphic_v = ::gluino::is_ultimately_polymorphic<T>::value;


    template <class T>
    struct remove_indirection {
        using type = ::std::remove_pointer_t<::std::remove_reference_t<T>>;
    };

    template <class T>
    using remove_indirection_t = typename ::gluino::remove_indirection<T>::type;


    template <class T>
    struct remove_indirection_like {
        using type = ::std::conditional_t<
                ::gluino::is_smart_pointer_v<T>,
                ::gluino::element_type_t<T>, ::gluino::remove_indirection_t<T>>;
    };

    template <class T>
    using remove_indirection_like_t = typename ::gluino::remove_indirection_like<T>::type;


    template <class T>
    struct is_primitive_char {
        static constexpr bool value = ::std::disjunction_v<::std::is_same<::std::remove_cv_t<T>, char>,
                ::std::is_same<::std::remove_cv_t<T>, wchar_t>, ::std::is_same<::std::remove_cv_t<T>, char8_t>,
                ::std::is_same<::std::remove_cv_t<T>, char16_t>, ::std::is_same<::std::remove_cv_t<T>, char32_t>>;
    };

    template <class T>
    static constexpr bool is_primitive_char_v = ::gluino::is_primitive_char<T>::value;


    template <class T>
    struct is_char {
        static constexpr bool value = ::gluino::is_primitive_char_v<T>;
    };

    template <class T>
    static constexpr bool is_char_v = ::gluino::is_char<T>::value;


    template <class T>
    struct is_referencable {
        static constexpr bool value = !::std::disjunction_v<::std::is_array<T>, ::std::is_void<T>>;
    };

    template <class T>
    static constexpr bool is_referencable_v = ::gluino::is_referencable<T>::value;


    template <class T, class Enable = void>
    struct core_type {
        using type = ::std::remove_cv_t<T>;
    };

    template <class T>
    struct core_type<T, ::std::enable_if_t<::gluino::is_indirection_like_v<T>, void>> : public ::gluino::core_type<::gluino::remove_indirection_like_t<T>> {
    };

    template <class T>
    using core_type_t = typename ::gluino::core_type<T>::type;


    template <class T, class Enable = void>
    struct strict_core_type {
        using type = ::std::remove_cv_t<T>;
    };

    template <class T>
    struct strict_core_type<T, ::std::enable_if_t<::gluino::is_indirection_v<T>, void>> : public ::gluino::core_type<::gluino::remove_indirection_t<T>> {
    };

    template <class T>
    using strict_core_type_t = typename ::gluino::strict_core_type<T>::type;


    template <class T>
    struct unconst {
        using type = ::std::remove_const_t<T>;
    };

    template <class T>
    struct unconst<const T&> {
        using type = T&;
    };

    template <class T>
    struct unconst<const T&&> {
        using type = T&&;
    };

	template <class T>
	struct unconst<T*> {
		using type = ::std::add_pointer_t<typename ::gluino::unconst<T>::type>;
	};

	template <class T>
	struct unconst<const T*> {
		using type = ::std::add_pointer_t<typename ::gluino::unconst<
			::std::remove_const_t<::std::remove_pointer_t<T>>>::type>;
	};

	template <class T>
	struct unconst<T* const> {
		using type = typename ::gluino::unconst<
			::std::remove_const_t<::std::remove_pointer_t<::std::remove_const_t<T>>>>::type*;
	};

	template <class T>
	struct unconst<const T* const> {
		using type = typename ::gluino::unconst<
			::std::remove_const_t<::std::remove_pointer_t<::std::remove_const_t<T>>>>::type*;
	};

    template <class T>
    using unconst_t = typename ::gluino::unconst<T>::type;
	
	
	template <class T>
	struct uncv {
		using type = ::std::remove_cv_t<T>;
	};

	template <class T>
	struct uncv<const T&> {
		using type = T&;
	};

	template <class T>
	struct uncv<const T&&> {
		using type = T&&;
	};

	template <class T>
	struct uncv<volatile T&> {
		using type = T&;
	};

	template <class T>
	struct uncv<volatile T&&> {
		using type = T&&;
	};

	template <class T>
	struct uncv<const volatile T&> {
		using type = T&;
	};

	template <class T>
	struct uncv<const volatile T&&> {
		using type = T&&;
	};

	template <class T>
	struct uncv<T*> {
		using type = ::std::add_pointer_t<typename ::gluino::uncv<T>::type>;
	};

	template <class T>
	struct uncv<const T*> {
		using type = ::std::add_pointer_t<typename ::gluino::uncv<
			::std::remove_cv_t<::std::remove_pointer_t<T>>>::type>;
	};

	template <class T>
	struct uncv<T* const> {
		using type = typename ::gluino::uncv<
			::std::remove_cv_t<::std::remove_pointer_t<::std::remove_cv_t<T>>>>::type*;
	};

	template <class T>
	struct uncv<const T* const> {
		using type = typename ::gluino::uncv<
			::std::remove_cv_t<::std::remove_pointer_t<::std::remove_cv_t<T>>>>::type*;
	};

	template <class T>
	using uncv_t = typename ::gluino::uncv<T>::type;
	
	
	template <class T>
	struct uncvref {
		using type = ::std::remove_cv_t<T>;
	};

	template <class T>
	struct uncvref<T&> {
		using type = T;
	};

	template <class T>
	struct uncvref<T&&> {
		using type = T;
	};
	
	template <class T>
	struct uncvref<const T&> {
		using type = T;
	};

	template <class T>
	struct uncvref<const T&&> {
		using type = T;
	};

	template <class T>
	struct uncvref<volatile T&> {
		using type = T;
	};

	template <class T>
	struct uncvref<volatile T&&> {
		using type = T;
	};

	template <class T>
	struct uncvref<const volatile T&> {
		using type = T;
	};

	template <class T>
	struct uncvref<const volatile T&&> {
		using type = T;
	};

	template <class T>
	struct uncvref<T*> {
		using type = ::std::add_pointer_t<typename ::gluino::uncvref<T>::type>;
	};

	template <class T>
	struct uncvref<const T*> {
		using type = ::std::add_pointer_t<typename ::gluino::uncvref<
			::std::remove_cv_t<::std::remove_pointer_t<T>>>::type>;
	};

	template <class T>
	struct uncvref<T* const> {
		using type = typename ::gluino::uncvref<
			::std::remove_cv_t<::std::remove_pointer_t<::std::remove_cv_t<T>>>>::type*;
	};

	template <class T>
	struct uncvref<const T* const> {
		using type = typename ::gluino::uncvref<
			::std::remove_cv_t<::std::remove_pointer_t<::std::remove_cv_t<T>>>>::type*;
	};

	template <class T>
	using uncvref_t = typename ::gluino::uncvref<T>::type;


    // Strings
    template <class T, class Char = char>
    struct is_string : public ::std::false_type {
    };

    template <class Char, class Traits, class Alloc>
    struct is_string<::std::basic_string<Char, Traits, Alloc>> : public ::std::true_type {
    };

    template <class T>
    static constexpr bool is_string_v = ::gluino::is_string<T>::value;

    template <class T, class Char>
    static constexpr bool is_string_of_v = ::gluino::is_string<T, Char>::value;


    template <class T>
    struct is_primitive_string {
        static constexpr bool value = ::std::conjunction_v<::gluino::is_string<T>,
                ::gluino::is_primitive_char<::gluino::value_type_t<T>>>;
    };

    template <class T>
    static constexpr bool is_primitive_string_v = ::gluino::is_primitive_string<T>::value;


    template <class T, class Char = char>
    struct is_string_view : public ::std::false_type {
    };

    template <class Char, class Traits>
    struct is_string_view<::std::basic_string_view<Char, Traits>> : public ::std::true_type {
    };

    template <class T>
    static constexpr bool is_string_view_v = ::gluino::is_string_view<T>::value;

    template <class T, class Char>
    static constexpr bool is_string_view_of_v = ::gluino::is_string_view<T, Char>::value;


    template <class T>
    struct is_primitive_string_view {
        static constexpr bool value = ::std::conjunction_v<::gluino::is_string_view<T>,
                ::gluino::is_primitive_char<::gluino::value_type_t<T>>>;
    };

    template <class T>
    static constexpr bool is_primitive_string_view_v = ::gluino::is_primitive_string_view<T>::value;


    template <class T, class Char = char>
    struct is_string_like {
        static constexpr bool value = ::std::disjunction_v<::gluino::is_string<T, Char>,
                ::gluino::is_string_view<T, Char>>;
    };

    template <class T>
    static constexpr bool is_string_like_v = ::gluino::is_string_like<T>::value;

    template <class T, class Char>
    static constexpr bool is_string_like_of_v = ::gluino::is_string_like<T, Char>::value;


    template <class T>
    struct is_primitive_string_like {
        static constexpr bool value = ::std::conjunction_v<::gluino::is_string_like<T>,
                ::gluino::is_primitive_char<::gluino::value_type_t<T>>>;
    };

    template <class T>
    static constexpr bool is_primitive_string_like_v = ::gluino::is_primitive_string_like<T>::value;


    template <class T, class Char = char>
    struct is_cstring : public ::std::false_type {
    };

    template <class Char>
    struct is_cstring<Char*> {
        static constexpr bool value = ::gluino::is_char_v<Char>;
    };

    template <class Char, ::std::size_t Length>
    struct is_cstring<Char[Length]> {
        static constexpr bool value = ::gluino::is_char_v<Char>;
    };

    template <class T>
    static constexpr bool is_cstring_v = ::gluino::is_cstring<T>::value;

    template <class T, class Char>
    static constexpr bool is_cstring_of_v = ::gluino::is_cstring<T, Char>::value;


    template <class T>
    struct is_primitive_cstring {
        static constexpr bool value = ::std::conjunction_v<::gluino::is_cstring<T>,
                ::gluino::is_primitive_char<::gluino::value_type_t<T>>>;
    };

    template <class T>
    static constexpr bool is_primitive_cstring_v = ::gluino::is_primitive_cstring<T>::value;


    template <class T, class Char = char>
    struct is_any_string_type {
        static constexpr bool value = ::std::disjunction_v<::gluino::is_string_like<T, Char>,
                ::gluino::is_cstring<T, Char>>;
    };

    template <class T>
    static constexpr bool is_any_string_type_v = ::gluino::is_any_string_type<T>::value;

    template <class T, class Char>
    static constexpr bool is_any_string_type_of_v = ::gluino::is_any_string_type<T, Char>::value;


    template <class T>
    struct is_any_primitive_string_type {
        static constexpr bool value = ::std::disjunction_v<::gluino::is_primitive_string_like<T>,
                ::gluino::is_primitive_cstring<T>>;
    };

    template <class T>
    static constexpr bool is_any_primitive_string_type_v = ::gluino::is_any_primitive_string_type<T>::value;


    // Data width and relative selection
    template <class T>
    struct is_signed_integral {
        static constexpr bool value = ::std::conjunction_v<::std::is_integral<T>, ::std::is_signed<T>>;
    };

    template <class T>
    static constexpr bool is_signed_integral_v = ::gluino::is_signed_integral<T>::value;


    template <class T>
    struct is_unsigned_integral {
        static constexpr bool value = ::std::conjunction_v<::std::is_integral<T>, ::std::is_unsigned<T>>;
    };

    template <class T>
    static constexpr bool is_unsigned_integral_v = ::gluino::is_unsigned_integral<T>::value;


    template <class T, class U>
    struct is_same_kind {
        static constexpr bool value =
                ::std::disjunction_v<::std::conjunction<::std::is_floating_point<T>, ::std::is_floating_point<U>>,
                        ::std::conjunction<::gluino::is_signed_integral<T>, ::gluino::is_signed_integral<U>>,
                        ::std::conjunction<::gluino::is_unsigned_integral<T>, ::gluino::is_unsigned_integral<U>>>;
    };

    template <class T, class U>
    static constexpr bool is_same_kind_v = ::gluino::is_same_kind<T, U>::value;


    template <class T, class U>
    struct is_same_size {
        static constexpr bool value = sizeof(T) == sizeof(U);
    };

    template <class T, class U>
    static constexpr bool is_same_size_v = ::gluino::is_same_size<T, U>::value;


    template <class T, class U>
    struct is_different_size {
        static constexpr bool value = sizeof(T) != sizeof(U);
    };

    template <class T, class U>
    static constexpr bool is_different_size_v = ::gluino::is_different_size<T, U>::value;


    template <class T, class U>
    struct is_larger {
        static constexpr bool value = sizeof(T) > sizeof(U);
    };

    template <class T, class U>
    static constexpr bool is_larger_v = ::gluino::is_larger<T, U>::value;


    template <class T, class U>
    struct is_as_large_or_larger {
        static constexpr bool value = sizeof(T) >= sizeof(U);
    };

    template <class T, class U>
    static constexpr bool is_as_large_or_larger_v = ::gluino::is_as_large_or_larger<T, U>::value;


    template <class T, class U>
    struct is_smaller {
        static constexpr bool value = sizeof(T) < sizeof(U);
    };

    template <class T, class U>
    static constexpr bool is_smaller_v = ::gluino::is_smaller<T, U>::value;


    template <class T, class U>
    struct is_as_small_or_smaller {
        static constexpr bool value = sizeof(T) <= sizeof(U);
    };

    template <class T, class U>
    static constexpr bool is_as_small_or_smaller_v = ::gluino::is_as_small_or_smaller<T, U>::value;


    template <class T, class U>
    struct is_wider {
        static constexpr bool value = ::std::conjunction_v<::gluino::is_larger<T, U>, ::gluino::is_same_kind<T, U>>;
    };

    template <class T, class U>
    static constexpr bool is_wider_v = ::gluino::is_wider<T, U>::value;


    template <class T, class U>
    struct is_as_wide_or_wider {
        static constexpr bool value = ::std::conjunction_v<::gluino::is_as_large_or_larger<T, U>,
                ::gluino::is_same_kind<T, U>>;
    };

    template <class T, class U>
    static constexpr bool is_as_wide_or_wider_v = ::gluino::is_as_wide_or_wider<T, U>::value;


    template <class T, class U>
    struct is_narrower {
        static constexpr bool value = ::std::conjunction_v<::gluino::is_smaller<T, U>,
                ::gluino::is_same_kind<T, U>>;
    };

    template <class T, class U>
    static constexpr bool is_narrower_v = ::gluino::is_narrower<T, U>::value;


    template <class T, class U>
    struct is_as_narrow_or_narrower {
        static constexpr bool value = ::std::conjunction_v<::gluino::is_as_small_or_smaller<T, U>,
                ::gluino::is_same_kind<T, U>>;
    };

    template <class T, class U>
    static constexpr bool is_as_narrow_or_narrower_v = ::gluino::is_as_narrow_or_narrower<T, U>::value;


    template <class T>
    struct widest_type {
        using type = ::std::conditional_t<
                ::std::is_integral_v<T>,
                ::std::conditional_t<::std::is_signed_v<T>, intmax_t, uintmax_t>,
                ::std::conditional_t<::std::is_floating_point_v<T>, long double, T>>;
    };

    template <class T>
    using widest_type_t = typename ::gluino::widest_type<T>::type;


    template <class T>
    struct narrowest_type {
        using type = ::std::conditional_t<
                ::std::is_integral_v<T>,
                ::std::conditional_t<::std::is_signed_v<T>, int8_t, uint8_t>,
                ::std::conditional_t<::std::is_floating_point_v<T>, long double, T>>;
    };

    template <class T>
    using narrowest_type_t = typename ::gluino::narrowest_type<T>::type;


    template <class T>
    struct is_widest {
        static constexpr bool value = sizeof(T) == sizeof(::gluino::widest_type_t<T>);
    };

    template <class T>
    static constexpr bool is_widest_v = ::gluino::is_widest<T>::value;


    template <class T>
    struct is_narrowest {
        static constexpr bool value = sizeof(T) == sizeof(::gluino::narrowest_type_t<T>);
    };

    template <class T>
    static constexpr bool is_narrowest_v = ::gluino::is_narrowest<T>::value;


    template <class T>
    struct default_type {
        using type = ::std::conditional_t<
                ::std::is_integral_v<T>,
                ::std::conditional_t<::std::is_signed_v<T>, decltype(0), decltype(0u)>,
                ::std::conditional_t<::std::is_floating_point_v<T>, decltype(0.0), T>>;
    };

    template <class T>
    using default_type_t = typename ::gluino::default_type<T>::type;


    template <class T>
    struct is_default_type {
        static constexpr bool value = ::std::is_same_v<T, ::gluino::default_type_t<T>>;
    };

    template <class T>
    static constexpr bool is_default_type_v = ::gluino::is_default_type<T>::value;


    template <class T>
    struct best_type {
        using type = ::std::conditional_t<
                ::std::is_integral_v<T>,
                ::std::conditional_t<::std::is_signed_v<T>, ::std::make_signed_t<uintptr_t>, uintptr_t>,
                ::std::conditional_t<::std::is_floating_point_v<T>, double_t, T>>;
    };

    template <class T>
    using best_type_t = typename ::gluino::best_type<T>::type;


    template <class T>
    struct is_best_type {
        static constexpr bool value = ::std::is_same_v<T, ::gluino::best_type_t<T>>;
    };

    template <class T>
    static constexpr bool is_best_type_v = ::gluino::is_best_type<T>::value;


    // Type matching
    template <class T, class Match>
    struct match_cv {
        using type = T;
    };

    template <class T, class Match>
    struct match_cv<T, const Match> {
        using type = const T;
    };

    template <class T, class Match>
    struct match_cv<T, volatile Match> {
        using type = volatile T;
    };

    template <class T, class Match>
    struct match_cv<T, const volatile Match> {
        using type = const volatile T;
    };

    template <class T, class Match>
    using match_cv_t = typename ::gluino::match_cv<T, Match>::type;


    // Functions
    template <class T>
    struct return_type {
    };

    template <class Return, class... Args>
    struct return_type<Return(*)(Args...)> {
        using type = Return;
    };

    template <class Return, class Self, class... Args>
    struct return_type<Return(Self::*)(Args...)> {
        using type = Return;
    };

    template <class Return, class Self, class... Args>
    struct return_type<Return(Self::*)(Args...) const> {
        using type = Return;
    };

    template <class Return, class Self, class... Args>
    struct return_type<Return(Self::*)(Args...) noexcept> {
        using type = Return;
    };

    template <class Return, class Self, class... Args>
    struct return_type<Return(Self::*)(Args...) const noexcept> {
        using type = Return;
    };

    template <class Return, class... Args>
    struct return_type<::std::function<Return(Args...)>> {
        using type = Return;
    };

    template <class Return, class... Args>
    struct return_type<Return(Args...)> {
        using type = Return;
    };

    template <class T>
    using return_type_t = typename ::gluino::return_type<T>::type;

    template <class T>
    struct argument_count {
    };

    template <class Return, class... Args>
    struct argument_count<Return(*)(Args...)> {
        static constexpr auto value = sizeof...(Args);
    };

    template <class Return, class... Args>
    struct argument_count<Return(Args...)> {
        static constexpr auto value = sizeof...(Args);
    };

    template <class Return, class Self, class... Args>
    struct argument_count<Return(Self::*)(Args...)> {
        static constexpr auto value = sizeof...(Args);
    };

    template <class Return, class Self, class... Args>
    struct argument_count<Return(Self::*)(Args...) const> {
        static constexpr auto value = sizeof...(Args);
    };

    template <class Return, class Self, class... Args>
    struct argument_count<Return(Self::*)(Args...) noexcept> {
        static constexpr auto value = sizeof...(Args);
    };

    template <class Return, class Self, class... Args>
    struct argument_count<Return(Self::*)(Args...) const noexcept> {
        static constexpr auto value = sizeof...(Args);
    };

    template <class Return, class... Args>
    struct argument_count<::std::function<Return(Args...)>> {
        static constexpr auto value = sizeof...(Args);
    };

    template <class T>
    static constexpr auto argument_count_v = ::gluino::argument_count<T>::value;

    template <class T, ::std::size_t Index>
    struct argument_type {
    };

    template <::std::size_t Index, class Return, class... Args>
    struct argument_type<Return(*)(Args...), Index> {
        using type = typename ::std::tuple_element<Index, ::std::tuple<Args...>>::type;
    };

    template <::std::size_t Index, class Return, class... Args>
    struct argument_type<Return(Args...), Index> {
        using type = typename ::std::tuple_element<Index, ::std::tuple<Args...>>::type;
    };

    template <::std::size_t Index, class Return, class Self, class... Args>
    struct argument_type<Return(Self::*)(Args...), Index> {
        using type = typename ::std::tuple_element<Index, ::std::tuple<Args...>>::type;
    };

    template <::std::size_t Index, class Return, class Self, class... Args>
    struct argument_type<Return(Self::*)(Args...) const, Index> {
        using type = typename ::std::tuple_element<Index, ::std::tuple<Args...>>::type;
    };

    template <::std::size_t Index, class Return, class Self, class... Args>
    struct argument_type<Return(Self::*)(Args...) noexcept, Index> {
        using type = typename ::std::tuple_element<Index, ::std::tuple<Args...>>::type;
    };

    template <::std::size_t Index, class Return, class Self, class... Args>
    struct argument_type<Return(Self::*)(Args...) const noexcept, Index> {
        using type = typename ::std::tuple_element<Index, ::std::tuple<Args...>>::type;
    };

    template <::std::size_t Index, class Return, class... Args>
    struct argument_type<::std::function<Return(Args...)>, Index> {
        using type = typename ::std::tuple_element<Index, ::std::tuple<Args...>>::type;
    };

    template <class T, ::std::size_t Index>
    using argument_type_t = typename ::gluino::argument_type<T, Index>::type;

    template <class T>
    struct this_type {
    };

    template <class Return, class Self, class... Args>
    struct this_type<Return(Self::*)(Args...)> {
        using type = Self;
    };

    template <class Return, class Self, class... Args>
    struct this_type<Return(Self::*)(Args...) const> {
        using type = Self;
    };

    template <class Return, class Self, class... Args>
    struct this_type<Return(Self::*)(Args...) noexcept> {
        using type = Self;
    };

    template <class Return, class Self, class... Args>
    struct this_type<Return(Self::*)(Args...) const noexcept> {
        using type = Self;
    };

    template <class T>
    using this_type_t = typename this_type<T>::type;
}
