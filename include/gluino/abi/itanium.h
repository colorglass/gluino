#pragma once

#include <typeinfo>

#include <cxxabi.h>

#include "../flags.h"

namespace gluino::abi::itanium {
    namespace articuno::detail {
        [[nodiscard]] inline ::std::string demangle(const char* name) {
            int result;
            ::std::unique_ptr<char, void(*)(void*)> demangled{
                    ::abi::__cxa_demangle(name, nullptr, nullptr, &result), ::std::free};
            return result ? name : demangled.get();
        }
    }

    namespace detail {
        template <class T>
        struct base {
        };

        struct si_class : public ::gluino::abi::itanium::detail::base<int> {
        };

        struct vmi_class : public ::gluino::abi::itanium::detail::base<int>,
            ::gluino::abi::itanium::detail::base<double> {
        };
    }

    gluino_flags(inheritance_flags, unsigned int,
                 (non_diamond_repeat, 0x1),
                 (diamond_shaped, 0x2)
    );

    gluino_flags(offset_flags, long,
                 (is_virtual, 0x1),
                 (is_public, 0x2),
                 (offset_shift, 0x8)
    );

    gluino_flags(pointer_flags, unsigned int,
                 (is_const, 0x1),
                 (is_volatile, 0x2),
                 (restrict, 0x4),
                 (incomplete, 0x8),
                 (incomplete_class, 0x10),
                 (transaction_safe, 0x20),
                 (is_noexcept, 0x40)
    );

    class fundamental_type_info : public ::std::type_info {
    public:
        fundamental_type_info() = delete;
    };

    class array_type_info : public ::std::type_info {
    public:
        array_type_info() = delete;
    };

    class function_type_info : public ::std::type_info {
    public:
        function_type_info() = delete;
    };

    class enum_type_info : public ::std::type_info {
    public:
        enum_type_info() = delete;
    };

    class class_type_info : public ::std::type_info {
    public:
        class_type_info() = delete;

        template <::gluino::polymorphic T>
        static const class_type_info* from(T* polymorphic_instance) {
            return *reinterpret_cast<class_type_info**>(
                    *reinterpret_cast<uintptr_t**>(polymorphic_instance) - 1);
        }
    };

    class si_class_type_info : public ::gluino::abi::itanium::class_type_info {
    public:
        si_class_type_info() = delete;

        [[nodiscard]] inline const ::gluino::abi::itanium::class_type_info* base_type() const noexcept {
            return _base_type;
        }

    private:
        const ::gluino::abi::itanium::class_type_info* _base_type;
    };

    class base_class_type_info {
    public:
        base_class_type_info() = delete;

        [[nodiscard]] inline const class_type_info* base_type() const noexcept {
            return _base_type;
        }

        [[nodiscard]] inline offset_flags flags() const noexcept {
            return _flags;
        }

    private:
        const class_type_info* _base_type;
        offset_flags _flags;
    };

    class vmi_class_type_info : public ::gluino::abi::itanium::class_type_info {
    public:
        using size_type = unsigned int;

        vmi_class_type_info() = delete;

        [[nodiscard]] inline inheritance_flags flags() const noexcept {
            return _flags;
        }

        [[nodiscard]] inline unsigned int base_count() const noexcept {
            return _base_count;
        }

        [[nodiscard]] inline const ::gluino::abi::itanium::base_class_type_info* base_info(
                unsigned int index) const noexcept {
            return _base_info + index;
        }

    private:
        inheritance_flags _flags;
        unsigned int _base_count;
        const ::gluino::abi::itanium::base_class_type_info _base_info[1]
    };

    class pbase_type_info : public ::std::type_info {
    public:
        pbase_type_info() = delete;

        [[nodiscard]] inline pointer_flags flags() const noexcept {
            return _flags;
        }

        [[nodiscard]] inline const ::std::type_info* pointee() const noexcept {
            return _pointee;
        }

    private:
        pointer_flags _flags;
        const ::std::type_info* _pointee;
    };

    class pointer_type_info : public ::gluino::abi::itanium::pbase_type_info {
    public:
        pointer_type_info() = delete;
    };

    class pointer_to_member_type_info : public ::gluino::abi::itanium::pbase_type_info {
    public:
        pointer_to_member_type_info() = delete;

        [[nodiscard]] inline const class_type_info* context() const noexcept {
            return _context;
        }

    private:
        const class_type_info* _context;
    };
}
