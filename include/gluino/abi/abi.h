#pragma once

#include <vector>

#include "../concepts.h"
#include "../flags.h"
#include "../ptr.h"

#if defined(GLUINO_ABI_ITANIUM) && GLUINO_ABI_ITANIUM != 0
#define GLUINO_ABI_USE_ITANIUM 1
#include "itanium.h"

namespace gluino::abi {
    /**
     * Returns the demangled version of a mangled C++ name.
     */
    const auto demangle = ::gluino::abi::itanium::demangle;
}

#elif (defined(_MSC_VER) && (!defined(GLUINO_ABI_MSVC) || GLUINO_ABI_MSVC != 0)) || GLUINO_ABI_MSVC != 0
#define GLUINO_ABI_USE_MSVC 1
#include "msvc.h"

namespace gluino::abi {
    /**
     * Returns the demangled version of a mangled C++ name.
     */
    const auto demangle = ::gluino::abi::msvc::demangle;
}

#else
static_assert(false, "Gluino only supports the Itanium and MSVC ABIs.");
#endif

namespace gluino::abi {
    gluino_flags(inheritance_flags, uint8_t,
                 (is_virtual, 1 << 0),
                 (is_public, 1 << 1));

    class base_class_info;

    class class_info {
    public:
        [[nodiscard]] inline ::std::size_t num_base_classes() const noexcept;

        [[nodiscard]] inline bool is_root() const noexcept;

        [[nodiscard]] inline const auto& base_classes() const noexcept {
            initialize_base_classes();
            return _base_classes;
        }

        [[nodiscard]] inline ::std::string_view name() const noexcept {
            if (_name.empty()) {
                _name = ::gluino::abi::demangle(_type_info.name());
            }
            return _name;
        }

        [[nodiscard]] inline const ::std::type_info& type_descriptor() const noexcept {
            return _type_info;
        }

        template <::gluino::polymorphic T>
        [[nodiscard]] static inline class_info from(T& polymorphic_object) {
#ifdef GLUINO_ABI_USE_ITANIUM
            // TODO
#else
            return class_info(::gluino::abi::msvc::complete_object_locator::from(&polymorphic_object));
#endif
        }

    private:
        inline explicit class_info(::gluino::abi::msvc::complete_object_locator* col) noexcept;

        inline explicit class_info(const ::gluino::abi::msvc::class_hierarchy_descriptor& hierarchy,
                                   const ::std::type_info& type, ::std::size_t start, ::std::size_t end) noexcept;

        inline void initialize_base_classes() const;

        inline void read_bases(const ::gluino::abi::msvc::class_hierarchy_descriptor& hierarchy,
                        ::std::size_t start, ::std::size_t end) const;

#ifdef GLUINO_ABI_USE_ITANIUM
#else
        ::gluino::abi::msvc::complete_object_locator* _col{nullptr};
#endif
        const ::std::type_info& _type_info;
        mutable ::std::string _name;
        mutable ::std::vector<base_class_info> _base_classes;

        friend class ::gluino::abi::base_class_info;
    };

    class base_class_info {
    public:
        inline base_class_info(const ::gluino::abi::msvc::class_hierarchy_descriptor& hierarchy,
                               const ::std::type_info& type, ::std::size_t start, ::std::size_t end,
                               ::gluino::abi::inheritance_flags flags) noexcept
                : _flags(flags), _base_class(hierarchy, type, start, end) {
        }

        [[nodiscard]] inline inheritance_flags flags() const noexcept {
            return _flags;
        }

        [[nodiscard]] inline const class_info& base_class() const noexcept {
            return _base_class;
        }

    private:
        ::gluino::abi::inheritance_flags _flags;
        ::gluino::abi::class_info _base_class;

        friend class ::gluino::abi::class_info;
    };

    class_info::class_info(::gluino::abi::msvc::complete_object_locator* col) noexcept
            : _col(col), _type_info(col->type_descriptor()) {
    }

    class_info::class_info(const ::gluino::abi::msvc::class_hierarchy_descriptor& hierarchy,
                           const ::std::type_info& type, ::std::size_t start, ::std::size_t end) noexcept
            : _type_info(type) {
        read_bases(hierarchy, start, end);
    }

    void class_info::initialize_base_classes() const {
        if (_base_classes.empty() && _col) {
            auto hierarchy = _col->class_hierarchy_descriptor();
            auto num_classes = hierarchy.base_class_descriptor_count();
            read_bases(hierarchy, 1, num_classes);
        }
    }

    void class_info::read_bases(const ::gluino::abi::msvc::class_hierarchy_descriptor& hierarchy,
                                ::std::size_t start, ::std::size_t end) const {
        for (::std::size_t i = start; i < end; ++i) {
            auto base_class = hierarchy.base_class_descriptor(static_cast<uint32_t>(i));
            const auto& type_descriptor = base_class.type_descriptor();
            ::gluino::abi::inheritance_flags inheritance;
            if (base_class.attributes().has(::gluino::abi::msvc::base_class_flags::is_virtual)) {
                inheritance.set(::gluino::abi::inheritance_flags::is_virtual);
            }
            if (base_class.attributes().none(::gluino::abi::msvc::base_class_flags::is_private,
                                            ::gluino::abi::msvc::base_class_flags::not_visible,
                                            ::gluino::abi::msvc::base_class_flags::private_or_protected_base)) {
                inheritance.set(::gluino::abi::inheritance_flags::is_public);
            }
            _base_classes.emplace_back(hierarchy, type_descriptor, i + 1, i + base_class.contained_bases_count() + 1,
                                       inheritance);
            i += base_class.contained_bases_count();
        }
    }

    ::std::size_t class_info::num_base_classes() const noexcept {
        initialize_base_classes();
        return _base_classes.size();
    }

    [[nodiscard]] bool class_info::is_root() const noexcept {
        initialize_base_classes();
        return _base_classes.empty();
    }
}
