#pragma once

#include <Windows.h>
#include <DbgHelp.h>
#include <gluino/autorun.h>

#include "../concepts.h"
#include "../flags.h"

#pragma comment(lib, "DbgHelp.lib")

namespace gluino::abi::msvc {
    [[nodiscard]] inline ::std::string demangle(::std::string_view name) {
        static ::std::atomic_bool initialized{false};
        static ::std::latch initialization_state(1);
        if (initialized.exchange(true)) {
            initialization_state.wait();
        } else {
            ::SymSetOptions(SYMOPT_UNDNAME | SYMOPT_DEFERRED_LOADS);
            initialization_state.count_down();
        }
        // If the mangled name is under 1 KiB then we assume the unmanagled is too, and avoid heap allocation.
        if (name.size() < 1024) {
            char buffer[1024];
            auto size = UnDecorateSymbolName(name.data(), buffer, 1024, UNDNAME_COMPLETE);
            return size ? buffer : "";
        } else {
            ::std::string result;
            result.reserve(name.size());
            auto size = UnDecorateSymbolName(name.data(), result.data(), static_cast<DWORD>(name.size()), UNDNAME_COMPLETE);
            return size ? ::std::move(result) : "";
        }
    }

    class displacement_data {
    public:
        [[nodiscard]] inline int32_t member_displacement() const noexcept {
            return _member_displacement;
        }

        [[nodiscard]] inline int32_t vtable_displacement() const noexcept {
            return _vtable_displacement;
        }

        [[nodiscard]] inline int32_t displacement_within_vtable() const noexcept {
            return _displacement_within_vtable;
        }

    private:
        int32_t _member_displacement;
        int32_t _vtable_displacement;
        int32_t _displacement_within_vtable;
    };

    class class_hierarchy_descriptor;

    class complete_object_locator;

    gluino_flags(base_class_flags, uint32_t,
                 (not_visible, 1 << 0),
                 (ambiguous, 1 << 1),
                 (is_private, 1 << 2),
                 (private_or_protected_base, 1 << 3),
                 (is_virtual, 1 << 4),
                 (non_polymorphic, 1 << 5),
                 (has_hierarchy_descriptor, 1 << 6)
    );

    class base_class_descriptor {
    public:
        [[nodiscard]] inline const ::std::type_info& type_descriptor() const noexcept {
            return *reinterpret_cast<const ::std::type_info*>(_base + _data->_type_descriptor_offset_from_base);
        }

        [[nodiscard]] inline uint32_t contained_bases_count() const noexcept {
            return _data->_contained_bases_count;
        }

        [[nodiscard]] inline const displacement_data& displacement_data() const noexcept {
            return _data->_pmd;
        }

        [[nodiscard]] ::gluino::abi::msvc::base_class_flags attributes() const noexcept {
            return _data->_attributes;
        }

    private:
        struct _base_class_descriptor {
            const uint32_t _type_descriptor_offset_from_base;
            const uint32_t _contained_bases_count;
            const ::gluino::abi::msvc::displacement_data _pmd;
            const ::gluino::abi::msvc::base_class_flags _attributes;
        };

        const uintptr_t _base;
        const _base_class_descriptor* _data;

        inline base_class_descriptor(uintptr_t base, uint32_t offset_from_base) noexcept
                : _base(base),
                  _data(reinterpret_cast<const _base_class_descriptor*>(_base + offset_from_base)) {
        }

        friend class class_hierarchy_descriptor;
    };

    gluino_flags(class_hierarchy_flags, uint32_t,
                 (no_inheritance, 0),
                 (multiple_inheritance, 1 << 0),
                 (virtual_inheritance, 1 << 1),
                 (ambiguous_inheritance, 1 << 2)
    );

    class class_hierarchy_descriptor {
    public:
        [[nodiscard]] inline uint32_t signature() const noexcept {
            return _data->_signature;
        }

        [[nodiscard]] inline ::gluino::abi::msvc::class_hierarchy_flags attributes() const noexcept {
            return _data->_attributes;
        }

        [[nodiscard]] inline uint32_t base_class_descriptor_count() const noexcept {
            return _data->base_class_array_size;
        }

        [[nodiscard]] inline ::gluino::abi::msvc::base_class_descriptor
        base_class_descriptor(uint32_t index) const noexcept {
            return {_base, *reinterpret_cast<uint32_t*>(_base + _data->_base_class_array_offset_from_base +
                                                        index * sizeof(uint32_t))};
        }

    private:
        struct _class_hierarchy_descriptor {
            const uint32_t _signature;
            const ::gluino::abi::msvc::class_hierarchy_flags _attributes;
            const uint32_t base_class_array_size;
            const uint32_t _base_class_array_offset_from_base;
        };

        const uintptr_t _base;
        const _class_hierarchy_descriptor* _data;

        inline class_hierarchy_descriptor(uintptr_t base, uint32_t offset_from_base) noexcept
                : _base(base),
                  _data(reinterpret_cast<const _class_hierarchy_descriptor*>(_base + offset_from_base)) {
        }

        friend class complete_object_locator;
    };

    gluino_enum(type_signature, uint32_t,
                (x86, 0),
                (amd64, 1));

    class complete_object_locator {
    public:
        complete_object_locator() = delete;

        [[nodiscard]] type_signature signature() const noexcept {
            return _signature;
        }

        [[nodiscard]] uint32_t offset() const noexcept {
            return _offset;
        }

        [[nodiscard]] uint32_t constructor_offset() const noexcept {
            return _constructor_offset;
        }

        [[nodiscard]] const ::std::type_info& type_descriptor() const noexcept {
            return *reinterpret_cast<const ::std::type_info*>(reinterpret_cast<uintptr_t>(this) -
                                                            offset_from_base() +
                                                            _type_descriptor_offset_from_base);
        }

        [[nodiscard]] inline class_hierarchy_descriptor class_hierarchy_descriptor() const noexcept {
            return {module_base(), _class_descriptor_offset_from_base};
        }

        [[nodiscard]] inline uint32_t offset_from_base() const noexcept {
            return _offset_from_base;
        }

        [[nodiscard]] inline uintptr_t module_base() const noexcept {
            return reinterpret_cast<uintptr_t>(this) - offset_from_base();
        }

        template <::gluino::polymorphic T>
        [[nodiscard]] static inline complete_object_locator* from(T* polymorphic_instance) {
            return *reinterpret_cast<complete_object_locator**>(
                    *reinterpret_cast<uintptr_t**>(polymorphic_instance) - 1);
        }

    private:
        const type_signature _signature;
        const uint32_t _offset;
        const uint32_t _constructor_offset;
        const uint32_t _type_descriptor_offset_from_base;
        const uint32_t _class_descriptor_offset_from_base;
        const uint32_t _offset_from_base;
    };
}
