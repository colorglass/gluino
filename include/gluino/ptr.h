#pragma once

#include <concepts>
#include <memory>
#include <optional>
#include <stdexcept>

#include "concepts.h"

namespace gluino {
    template <class T>
    class raw_ptr {
    public:
        using element_type = ::std::remove_extent_t<T>;

        inline raw_ptr() noexcept = default;

        template <class U>
        requires ::gluino::subtype_of<U, T>
        inline raw_ptr(U* value) noexcept
                : _value(value) {
        }

        inline explicit raw_ptr(::std::nullopt_t) noexcept {
        }

        inline raw_ptr(::std::nullptr_t) noexcept {
        }

        template <class U>
        requires ::gluino::subtype_of<U, T>
        inline void reset(U* value = nullptr) noexcept {
            _value = value;
        }

        inline void swap(raw_ptr& other) noexcept {
            ::std::swap(_value, other._value);
        }

        inline void swap(element_type*& value) noexcept {
            ::std::swap(_value, value);
        }

        [[nodiscard]] inline element_type* operator->() const noexcept {
            return _value;
        }

        [[nodiscard]] inline element_type* get() const noexcept {
            return _value;
        }

        [[nodiscard]] inline element_type* get_or(T* alternative) const noexcept {
            return is_null() ? alternative : _value;
        }

        [[nodiscard]] inline bool is_null() const noexcept {
            return _value == nullptr;
        }

        template <class U = element_type, ::std::enable_if_t<::gluino::is_referencable_v<U>, int> = 0>
        [[nodiscard]] inline U& operator*() const noexcept {
            return *_value;
        }
        template <class U>
        requires ::gluino::subtype_of<U, T>
        [[nodiscard]] inline operator U*() const noexcept {
            return _value;
        }

        template <class U>
        requires ::gluino::subtype_of<U, T>
        inline raw_ptr& operator=(const raw_ptr<U>& other) noexcept {
            _value = other._value;
        }

        template <class U>
        requires ::gluino::subtype_of<U, T>
        inline raw_ptr& operator=(raw_ptr<U>&& other) noexcept {
            _value = ::std::move(other._value);
        }

        template <class U>
        requires ::gluino::subtype_of<U, T>
        inline raw_ptr& operator=(U*& value) noexcept {
            _value = value;
        }

        template <class U>
        requires ::gluino::subtype_of<U, T>
        inline raw_ptr& operator=(U*&& value) noexcept {
            _value = ::std::move(value);
        }

        template <class U>
        [[nodiscard]] inline auto operator<=>(::gluino::raw_ptr<U> other) const noexcept {
            return _value <=> other._value;
        }

        template <class U>
        [[nodiscard]] inline auto operator<=>(U* other) const noexcept {
            return _value <=> other;
        }

        [[nodiscard]] inline operator bool() const noexcept {
            return _value;
        }

    private:
        element_type* _value{nullptr};
    };

    template <class T>
    class safe_ptr {
    public:
        using element_type = ::std::remove_extent_t<T>;

        template <class U>
        requires ::gluino::subtype_of<U, T>
        inline explicit safe_ptr(U* value) : _value(value) {
            validate(value);
        }

        template <class U>
        requires ::gluino::subtype_of<U, T>
        inline void reset(U* value) {
            validate(value);
            _value = value;
        }

        inline void swap(safe_ptr& other) noexcept {
            ::std::swap(_value, other._value);
        }

        inline void swap(element_type*& value) {
            validate(value);
            ::std::swap(_value, value);
        }

        [[nodiscard]] inline element_type* operator->() const noexcept {
            return _value;
        }

        [[nodiscard]] inline element_type* get() const noexcept {
            return _value;
        }

        template <class U = element_type, ::std::enable_if_t<::gluino::is_referencable_v<U>, int> = 0>
        [[nodiscard]] inline U& operator*() const noexcept {
            return *_value;
        }

        template <class U>
        requires ::gluino::subtype_of<U, T>
        [[nodiscard]] inline operator U*() const noexcept {
            return _value;
        }

        template <class U>
        requires ::gluino::subtype_of<U, T>
        inline safe_ptr& operator=(const safe_ptr<U>& other) noexcept {
            _value = other._value;
            return *this;
        }

        template <class U>
        requires ::gluino::subtype_of<U, T>
        inline safe_ptr& operator=(safe_ptr<U>&& other) noexcept {
            _value = ::std::move(other._value);
            return *this;
        }

        template <class U>
        requires ::gluino::subtype_of<U, T>
        inline safe_ptr& operator=(U*& value) {
            validate(value);
            _value = value;
            return *this;
        }

        template <class U>
        requires ::gluino::subtype_of<U, T>
        inline safe_ptr& operator=(U*&& value) {
            validate(value);
            _value = ::std::move(value);
            return *this;
        }

        template <class U>
        [[nodiscard]] inline auto operator<=>(::gluino::safe_ptr<U> other) const noexcept {
            return _value <=> other._value;
        }

        template <class U>
        [[nodiscard]] inline auto operator<=>(U* other) const noexcept {
            return _value <=> other;
        }

        template <class U>
        [[nodiscard]] inline auto operator<=>(::gluino::raw_ptr<U> other) const noexcept {
            return _value <=> other._value;
        }

    private:
        template <class U>
        inline void validate(U* value) const {
            if (value == nullptr) {
                throw ::std::invalid_argument("safe_ptr cannot be assigned a pointer to null.");
            }
        }

        element_type* _value;
    };

    template <class T>
    class optional_ptr {
    public:
        using element_type = ::std::remove_extent_t<T>;

        constexpr optional_ptr() noexcept = default;

        template <class U>
        requires ::gluino::subtype_of<::std::remove_cv_t<U>, ::std::remove_cv_t<T>>
        constexpr optional_ptr(U* value) noexcept
                : _value(value) {
        }

        constexpr optional_ptr(nullptr_t) noexcept {
        }

        template <class U>
        requires ::gluino::subtype_of<U, T>
        inline void reset(U* value = nullptr) noexcept {
            _value = value;
        }

        inline void swap(optional_ptr& other) {
            validate();
            ::std::swap(_value, other._value);
        }

        inline void swap(element_type*& value) {
            validate();
            ::std::swap(_value, value);
        }

        [[nodiscard]] inline element_type* operator->() const {
            validate();
            return _value;
        }

        [[nodiscard]] inline element_type* get() const {
            validate();
            return _value;
        }

        [[nodiscard]] inline element_type* get_raw() const noexcept {
            return _value;
        }

        [[nodiscard]] inline element_type* get_or(T* alternative) const noexcept {
            return is_null() ? alternative : _value;
        }

        [[nodiscard]] constexpr bool is_null() const noexcept {
            return _value == nullptr;
        }

        [[nodiscard]] constexpr bool is_valid() const noexcept {
            return !is_null();
        }

        template <class U = element_type, ::std::enable_if_t<::gluino::is_referencable_v<U>, int> = 0>
        [[nodiscard]] constexpr U& operator*() const {
            validate();
            return *_value;
        }

        template <class U>
        requires ::gluino::subtype_of<U, T>
        [[nodiscard]] inline explicit operator U*() const {
            validate();
            return _value;
        }

        template <class U>
        requires ::gluino::subtype_of<U, T>
        inline optional_ptr& operator=(const optional_ptr<U>& other) noexcept {
            _value = other._value;
            return *this;
        }

        template <class U>
        requires ::gluino::subtype_of<U, T>
        inline optional_ptr& operator=(optional_ptr<U>&& other) noexcept {
            _value = ::std::move(other._value);
            return *this;
        }

        template <class U>
        requires ::gluino::subtype_of<U, T>
        inline optional_ptr& operator=(U*& value) noexcept {
            _value = value;
            return *this;
        }

        template <class U>
        requires ::gluino::subtype_of<U, T>
        inline optional_ptr& operator=(U*&& value) noexcept {
            _value = ::std::move(value);
            return *this;
        }

        inline optional_ptr& operator=(nullptr_t) noexcept {
            _value = nullptr;
            return *this;
        }

        template <class U>
        [[nodiscard]] inline auto operator<=>(::gluino::optional_ptr<U> other) const noexcept {
            return _value <=> other._value;
        }

        template <class U>
        [[nodiscard]] inline auto operator<=>(U* other) const noexcept {
            return _value <=> other;
        }

        template <class U>
        [[nodiscard]] inline auto operator<=>(::gluino::raw_ptr<U> other) const noexcept {
            return _value <=> other._value;
        }

        [[nodiscard]] inline operator bool() const noexcept {
            return _value;
        }

    private:
        inline void validate() const {
            if (is_null()) {
                throw ::std::bad_optional_access();
            }
        }

        element_type* _value{nullptr};
    };

    template <class T>
    struct is_smart_pointer<::gluino::raw_ptr<T>> {
        static constexpr bool value = true;
    };

    template <class T>
    struct is_smart_pointer<::gluino::safe_ptr<T>> {
        static constexpr bool value = true;
    };

    template <class T>
    struct is_smart_pointer<::gluino::optional_ptr<T>> {
        static constexpr bool value = true;
    };
}

namespace std {
    template <class T>
    struct hash<::gluino::raw_ptr<T>> {
        [[nodiscard]] inline ::std::size_t operator()(::gluino::raw_ptr<T> value) const noexcept {
            ::std::hash<T*> hasher;
            return hasher(value.get());
        }
    };

    template <class T>
    struct hash<::gluino::safe_ptr<T>> {
        [[nodiscard]] inline ::std::size_t operator()(::gluino::safe_ptr<T> value) const noexcept {
            ::std::hash<T*> hasher;
            return hasher(value.get());
        }
    };

    template <class T>
    struct hash<::gluino::optional_ptr<T>> {
        [[nodiscard]] inline ::std::size_t operator()(::gluino::optional_ptr<T> value) const noexcept {
            ::std::hash<T*> hasher;
            return hasher(value.get_raw());
        }
    };
}

namespace boost {
    template <class T>
    [[nodiscard]] inline ::std::size_t hash_value(::gluino::raw_ptr<T> value) noexcept {
        ::std::hash<decltype(value)> hasher;
        return hasher(value);
    }

    template <class T>
    [[nodiscard]] inline ::std::size_t hash_value(::gluino::safe_ptr<T> value) noexcept {
        ::std::hash<decltype(value)> hasher;
        return hasher(value);
    }

    template <class T>
    [[nodiscard]] inline ::std::size_t hash_value(::gluino::optional_ptr<T> value) noexcept {
        ::std::hash<decltype(value)> hasher;
        return hasher(value);
    }
}
