#pragma once

#include <span>
#include <string_view>
#include <vector>

namespace gluino {
    /**
     * Discards a value which is marked <code>nodiscard</code>.
     *
     * <p>
     * Traditionally C++ code that needs to discard a <code>nodiscard</code> result does so by doing a
     * <code>static_cast</code> to <code>void</code>. However, this is verbose and unintuitive. This trivial discard
     * function accomplishes the same while making it clearly explicit that the purpose is to allow the discard.
     * </p>
     *
     * @tparam T The type of the value.
     * @param value The value.
     */
    template <class T>
    constexpr void discard([[maybe_unused]] T&& value) {
    }

    template <class T>
    [[nodiscard]] inline T& claim(T& value) {
        return value;
    }

    template <class T>
    [[nodiscard]] inline T claim(T&& value) {
        return value;
    }

    template <class Alloc = void, class Char = char, class Traits = ::std::char_traits<Char>>
    [[nodiscard]] inline auto claim(::std::basic_string_view<Char, Traits> string) {
        return ::std::basic_string<Char, Traits,
                ::std::conditional_t<::std::is_void_v<Alloc>, ::std::allocator<Char>, Alloc>>(
                        string.data(), string.size());
    }

    template <class Alloc = void, class T = int, ::std::size_t Extent = 0>
    [[nodiscard]] inline auto claim(::std::span<T, Extent> span) {
        ::std::vector<T, ::std::conditional_t<::std::is_void_v<Alloc>, ::std::allocator<T>, Alloc>> result;
        if constexpr (Extent == ::std::dynamic_extent) {
            result.reserve(Extent);
        } else {
            result.reserve(span.size());
        }
        result.insert(result.begin(), span.begin(), span.end());
        return result;
    }

    template <class T>
    [[nodiscard]] inline ::std::remove_reference_t<T> try_move(T&& object) {
        return ::std::move(::std::forward<T>(object));
    }

    template <class Alloc = void, class Char = char, class Traits = ::std::char_traits<Char>>
    [[nodiscard]] inline auto try_move(::std::basic_string_view<Char, Traits> string) {
        return claim<Alloc, Char, Traits>(string);
    }

    template <class Alloc = void, class T = int, ::std::size_t Extent = 0>
    [[nodiscard]] inline auto try_move(::std::span<T, Extent> span) {
        return claim<Alloc, T, Extent>(span);
    }

    template <class Iter>
    [[nodiscard]] inline Iter advance(Iter iterator, ::std::size_t i) {
        for (; i > 0; --i) {
            ++iterator;
        }
        return iterator;
    }

    template <class Iter>
    [[nodiscard]] inline Iter advance(Iter iterator, Iter end, ::std::size_t i) {
        for (; i > 0 && iterator != end; --i) {
            ++iterator;
        }
        return iterator;
    }

    template <class Iter>
    [[nodiscard]] inline Iter retreat(Iter iterator, ::std::size_t i) {
        for (; i > 0; --i) {
            --iterator;
        }
        return iterator;
    }

    template <class Iter>
    [[nodiscard]] inline Iter retreat(Iter iterator, Iter begin, Iter end, ::std::size_t i) {
        for (; i > 0; --i) {
            if (iterator == begin) {
                return end;
            }
            --iterator;
        }
        return iterator;
    }
}

#define GLUINO_AUTO(variable, expression) decltype(expression) variable = expression
