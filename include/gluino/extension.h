#pragma once

#include <tuple>

#include "flags.h"

namespace gluino {
    gluino_flags(extension_ops, uint32_t,
                 (pointer_to_member, 1 << 0),
                 (pipe, 1 << 1),
                 (double_pipe, 1 << 2),
                 (shift_left, 1 << 3),
                 (shift_right, 1 << 4),
                 (forward_slash, 1 << 5));

    namespace detail {
        template <class F, ::gluino::extension_ops Ops>
        class closure : public F {
        public:
            template <class... Args>
            explicit closure(Args&& ... args) : F(::std::forward<Args>(args)...) {
            }
        };

        template <::gluino::extension_ops Ops, class F>
        auto create_closure(F functor) {
            return closure<F, Ops>(::std::move(functor));
        }

        template <::gluino::extension_ops T>
        concept member_pointable = requires {
            { T.has(::gluino::extension_ops::pointer_to_member) };
        };

        template <::gluino::extension_ops T>
        concept pipable = requires {
            { T.has(::gluino::extension_ops::pipe) };
        };

        template <::gluino::extension_ops T>
        concept double_pipable = requires {
            { T.has(::gluino::extension_ops::double_pipe) };
        };

        template <::gluino::extension_ops T>
        concept left_shiftable = requires {
            { T.has(::gluino::extension_ops::shift_left) };
        };

        template <::gluino::extension_ops T>
        concept right_shiftable = requires {
            { T.has(::gluino::extension_ops::shift_right) };
        };

        template <::gluino::extension_ops T>
        concept slashable = requires {
            { T.has(::gluino::extension_ops::forward_slash) };
        };
    }

    /**
     * Defines an extension method.
     *
     * <p>
     * The extension class makes it possible to invoke an extension function on an existing, closed type. It requires
     * a function-like class (implementing <code>operator()</code>) to provide the implementation. The
     * <code>Ops</code> flags define which operators can be used to invoke the function. By default the functions are
     * invoked by the pointer-to-member operator (<code>->*</code>). Care should be taken of the different order of
     * operations for the available operators from normal member access operators.
     * </p>
     *
     * @tparam F The function-like object type which implements the extension function.
     * @tparam Ops The operators which will be available for invoking the extension function.
     */
    template <class F, ::gluino::extension_ops Ops = ::gluino::extension_ops::pointer_to_member>
    class extension {
    public:
        template <class... Args>
        auto operator()(Args&& ... args) const {
            return ::gluino::detail::create_closure<Ops>([args = ::std::make_tuple(::std::forward<Args>(args)...)]() {
                return ::std::apply([=](auto&& ... wrappedArgs) {
                    return [=](auto&& receiver) -> decltype(auto) {
                        return F()(receiver, wrappedArgs...);
                    };
                }, ::std::move(args));
            }());
        }
    };

    template <class T, class F, ::gluino::extension_ops Ops>
    requires ::gluino::detail::member_pointable<Ops>
    decltype(auto) operator->*(T&& receiver, const ::gluino::detail::closure<F, Ops>& closure) {
        return closure(::std::forward<T>(receiver));
    }

    template <class T, class F, ::gluino::extension_ops Ops>
    requires ::gluino::detail::pipable<Ops>
    decltype(auto) operator|(T&& receiver, const ::gluino::detail::closure<F, Ops>& closure) {
        return closure(::std::forward<T>(receiver));
    }

    template <class T, class F, ::gluino::extension_ops Ops>
    requires ::gluino::detail::double_pipable<Ops>
    decltype(auto) operator||(T&& receiver, const ::gluino::detail::closure<F, Ops>& closure) {
        return closure(::std::forward<T>(receiver));
    }

    template <class T, class F, ::gluino::extension_ops Ops>
    requires ::gluino::detail::left_shiftable<Ops>
    decltype(auto) operator<<(T&& receiver, const ::gluino::detail::closure<F, Ops>& closure) {
        return closure(::std::forward<T>(receiver));
    }

    template <class T, class F, ::gluino::extension_ops Ops>
    requires ::gluino::detail::right_shiftable<Ops>
    decltype(auto) operator>>(T&& receiver, const ::gluino::detail::closure<F, Ops>& closure) {
        return closure(::std::forward<T>(receiver));
    }

    template <class T, class F, ::gluino::extension_ops Ops>
    requires ::gluino::detail::slashable<Ops>
    decltype(auto) operator/(T&& receiver, const ::gluino::detail::closure<F, Ops>& closure) {
        return closure(::std::forward<T>(receiver));
    }
}
