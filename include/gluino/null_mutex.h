#pragma once

#include <chrono>
#include <mutex>

namespace gluino {
    /**
     * A zero-overhead no-op mutex, for cases where a mutex template argument is needed and an option for unsynchronized
     * access is desired; this meets the requirements of being both a timed and shared mutex.
     */
    class null_mutex {
    public:
        constexpr void lock() const noexcept {
        }

        constexpr void lock_shared() const noexcept {
        }

        [[nodiscard]] constexpr bool try_lock() const noexcept {
            return true;
        }

        [[nodiscard]] constexpr bool try_lock_shared() const noexcept {
            return true;
        }

        template <class Rep, class Period>
        [[nodiscard]] constexpr bool try_lock_for(
                const ::std::chrono::duration<Rep, Period>&& timeout_duration) const noexcept {
            return true;
        }

        template <class Rep, class Period>
        [[nodiscard]] constexpr bool try_lock_shared_for(
                const ::std::chrono::duration<Rep, Period>&& timeout_duration) const noexcept {
            return true;
        }

        template <class Clock, class Duration>
        [[nodiscard]] constexpr bool try_lock_until(
                const ::std::chrono::time_point<Clock,Duration>&& timeout_time) const noexcept {
            return true;
        }

        template <class Clock, class Duration>
        [[nodiscard]] constexpr bool try_lock_shared_until(
                const ::std::chrono::time_point<Clock,Duration>&& timeout_time) const noexcept {
            return true;
        }

        constexpr void unlock() const noexcept {
        }
    };
}
