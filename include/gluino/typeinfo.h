#pragma once

#include <typeindex>

#include "concepts.h"
#include "type_traits.h"

namespace gluino {
    template <class T, class Enable = void>
    struct rtti {
        static constexpr const ::std::type_info& type_info = typeid(T);

        template <class U = T, ::std::enable_if_t<::std::is_polymorphic_v<::std::remove_cvref_t<U>>, int> = 0>
        static inline const ::std::type_info& dynamic_type_info(U&& object) noexcept {
            return typeid(::std::forward<U>(object));
        }
    };

    template <class T>
    concept dynamically_typed = requires(T&& object) {
        { ::gluino::rtti<::std::remove_cvref_t<T>>::dynamic_type_info(::std::forward<T>(object)) } ->
            ::std::convertible_to<const ::std::type_info&>;
    };

    template <class T>
    [[nodiscard]] inline const ::std::type_info& type_info() noexcept {
        return ::gluino::rtti<::std::remove_cvref_t<T>>::type_info;
    }

    template <class T>
    [[nodiscard]] inline ::std::type_index type_index() noexcept {
        return ::std::type_index(::gluino::type_info<T>());
    }

	template <class T>
	[[nodiscard]] inline const ::std::type_info& type_info(T&& object) noexcept {
		return typeid(::std::forward<T>(object));
	}

	template <class T>
	[[nodiscard]] inline ::std::type_index type_index(T&& object) noexcept {
		return typeid(::std::forward<T>(object));
	}

    template <::gluino::dynamically_typed T>
    [[nodiscard]] inline const ::std::type_info& type_info(T&& object) noexcept {
        return ::gluino::rtti<::std::remove_cvref_t<T>>::dynamic_type_info(::std::forward<T>(object));
    }

    template <::gluino::dynamically_typed T>
    [[nodiscard]] inline ::std::type_index type_index(T&& object) noexcept {
        return ::std::type_index(::gluino::type_info(::std::forward<T>(object)));
    }
}
