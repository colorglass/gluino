#pragma once

#include <array>
#include <random>
#include <type_traits>
#include <vector>

#include "concepts.h"
#include "null_mutex.h"
#include "preprocessor.h"

#ifndef GLUINO_DEFAULT_SEED_COUNT
#define GLUINO_DEFAULT_SEED_COUNT 8
#endif

#ifndef GLUINO_DEFAULT_MAX_FAST_SEED_COUNT
#define GLUINO_DEFAULT_MAX_FAST_SEED_COUNT 32
#endif

namespace gluino {
#ifdef PCG_RAND_HPP_INCLUDED
    // TODO: When the pcg64 stream bug is fixed, adopt that as the default.
    using default_random_engine = ::pcg32;
#else
    using default_random_engine = ::std::default_random_engine;
#endif

    namespace detail {
        template <class T>
        [[nodiscard]] constexpr bool is_multistream_engine() noexcept {
            return false;
        }

        template <multistream_engine T>
        [[nodiscard]] constexpr bool is_multistream_engine() noexcept {
            return true;
        }

        template <class T>
        [[nodiscard]] constexpr bool is_backstepping_engine() noexcept {
            return false;
        }

        template <backstepping_engine T>
        [[nodiscard]] constexpr bool is_backstepping_engine() noexcept {
            return true;
        }
    }

    template <class T, class Enable = void>
    struct is_multistream_engine {
        static constexpr bool value = ::gluino::detail::is_multistream_engine<T>();

        template <class Eng = T,
                ::std::enable_if_t<::gluino::detail::is_multistream_engine<Eng>(), int> = 0>
        requires ::std::same_as<Eng, T>
        using stream_type = typename Eng::stream_state;

        template <class Eng = T,
                ::std::enable_if_t<::gluino::detail::is_multistream_engine<Eng>(), int> = 0>
        requires ::std::same_as<Eng, T>
        static inline void set_stream(Eng& engine, stream_type<Eng> stream) {
            engine.set_stream(stream);
        }

        template <class Eng = T,
                ::std::enable_if_t<::gluino::detail::is_multistream_engine<Eng>(), int> = 0>
        requires ::std::same_as<Eng, T>
        static inline stream_type<Eng> stream(const Eng& engine) {
            return const_cast<Eng&>(engine).stream();
        }
    };

    template <class T>
    constexpr bool is_multistream_engine_v = ::gluino::is_multistream_engine<T>::value;

    template <class T, class Enable = void>
    struct is_backstepping_engine {
        static constexpr bool value = ::gluino::detail::is_backstepping_engine<T>();

        template <class Eng = T, ::std::enable_if_t<::gluino::detail::is_backstepping_engine<Eng>(), int> = 0>
        requires ::std::same_as<Eng, T>
        static inline void backstep(Eng& engine, auto delta) {
            engine.backstep(delta);
        }
    };

    template <class T>
    constexpr bool is_backstepping_engine_v = ::gluino::is_backstepping_engine<T>::value;

    /**
     * A random engine preconfigured for effective random number generation.
     *
     * <p>
     * The <code>random_engine</code> wraps an underlying engine and provides it with more convenient functionality for
     * seeding effectively and getting values with proper distribution. By default it will use the C++ implementation's
     * default random engine if no better alternative is available. If it detects that PCG is already included it will
     * instead use a PCG engine as its default.
     * </p>
     *
     * <p>
     * Based on the underlying engine there may be additional capabilities exposed. The random engine can support
     * multi-stream engines, in which each generated value may come from a different stream of values. It can also
     * support backstepping, in which the engine reverts backwards a certain number of values (the opposite of skipping
     * ahead).
     * </p>
     *
     * @tparam Engine The underlying random engine implementation.
     * @tparam Mutex The mutex type to use for thread safety; defaults to a null mutex for no synchronization.
     */
    template <class Engine = ::gluino::default_random_engine, class Mutex = gluino::null_mutex>
    class random_engine {
    public:
        using engine_type = Engine;
        using mutex_type = Mutex;
        using result_type = typename engine_type::result_type;
        static constexpr uint8_t default_seed_count = GLUINO_DEFAULT_SEED_COUNT;
        static constexpr uint8_t max_fast_seed_count = GLUINO_DEFAULT_MAX_FAST_SEED_COUNT;
        static constexpr bool is_multistream = ::gluino::is_multistream_engine_v<engine_type>;
        static constexpr bool is_backstepping = ::gluino::is_backstepping_engine_v<engine_type>;

        /**
         * Creates a random engine and seeds it with a list of seeds of the default length from the OS random device.
         */
        inline random_engine() {
            try_init_stream();
            seed();
        }

        /**
         * Creates a random engine and seeds it with a seed sequence of the values provided.
         *
         * @param args The seed values to use.
         */
        template <class... Args>
        requires(::std::convertible_to<Args, result_type> && ...)
        inline explicit random_engine(Args... args) {
            try_init_stream();
            seed(args...);
        }

        /**
         * Creates a random engine and seeds it with a specified number of seed values from the given seeder device.
         * @tparam Seeder The type of the seed device.
         * @param seeder An instance of the seed device.
         * @param seed_count The number of seed values to generate from <code>seeder</code>.
         */
        template <seeder Seeder>
        requires (!::std::is_same_v<Seeder, ::gluino::random_engine<Engine, Mutex>>)
        inline explicit random_engine(Seeder&& seeder, uint32_t seed_count = default_seed_count) noexcept {
            try_init_stream();
            seed(::std::forward<Seeder>(seeder), seed_count);
        }

        /**
         * Creates a random engine and seeds it with the given seed sequence.
         *
         * @param seeds The seed sequence to seed the engine from.
         */
        inline explicit random_engine(const ::std::seed_seq& seeds) noexcept
                : _engine(seeds) {
            try_init_stream();
        }

        /**
         * Reseeds the random engine with the default number of seed values generated from the OS random device.
         */
        inline void seed() {
            seed(::std::random_device(), default_seed_count);
        }

        /**
         * Reseeds a random engine and seeds it with a seed sequence of the values provided.
         *
         * @param args The seed values to use.
         */
        template <class... Args>
        requires(::std::convertible_to<Args, result_type> && ...)
        inline void seed(Args... args) {
            auto seeds = ::std::seed_seq{static_cast<result_type>(args)...};
            _engine.seed(seeds);
        }

        /**
         * Reseeds a random engine and seeds it with a specified number of seed values from the given seeder device.
         * @tparam Seeder The type of the seed device.
         * @param seeder An instance of the seed device.
         * @param seed_count The number of seed values to generate from <code>seeder</code>.
         */
        template <seeder Seeder>
        inline void seed(Seeder&& seeder, uint32_t seed_count = default_seed_count) noexcept {
            auto seeds = get_seeds(::std::forward<Seeder>(seeder), seed_count);
            _engine.seed(seeds);
        }

        /**
         * Reseeds the random engine with seed values from a seed sequence.
         *
         * @param seeds The seed sequence from which to reseed the engine.
         */
        inline void seed(const ::std::seed_seq& seeds) noexcept {
            _engine.seed(seeds);
        }

        /**
         * Get the next value from the engine as the default result type with a uniform distribution.
         *
         * @return The next value from the engine.
         */
        [[nodiscard]] inline result_type next() noexcept {
            return next((min)(), (max)());
        }

        /**
         * Gets the next value from the engine as a floating point with uniform distribution.
         *
         * @tparam T The floating point type to return.
         * @return The next value from the engine.
         */
        template <::std::floating_point T>
        [[nodiscard]] inline T next() noexcept {
            return next(static_cast<T>((min)()), static_cast<T>((max)()));
        }

        /**
         * Gets the next value from the engine as a floating point with uniform distribution between 0 and the max.
         *
         * @tparam T The floating point type to return.
         * @param maximum The maximum value to return.
         * @return The next value from the engine.
         */
        template <::std::floating_point T>
        [[nodiscard]] inline T next(T maximum) noexcept {
            return next(static_cast<T>(0.0), maximum);
        }

        /**
         * Gets the next value from the engine as a floating point with uniform distribution between the min and max.
         *
         * @tparam T The floating point type to return.
         * @param minimum The minimum value to return.
         * @param maximum The maximum value to return.
         * @return The next value from the engine.
         */
        template <::std::floating_point T>
        [[nodiscard]] inline T next(T minimum, T maximum) noexcept {
            return next(::std::uniform_real_distribution<T>(minimum, maximum));
        }

        /**
         * Gets the next value from the engine as an integer with uniform distribution.
         *
         * @tparam T The integer type to return.
         * @return The next value from the engine.
         */
        template <::std::integral T>
        [[nodiscard]] inline T next() noexcept {
            return next(static_cast<T>((min)()), static_cast<T>((max)()));
        }

        /**
         * Gets the next value from the engine as an integer with uniform distribution between 0 and the max.
         *
         * @tparam T The type of integer to return.
         * @param maximum The maximum value to return.
         * @return The next value from the engine.
         */
        template <::std::integral T>
        [[nodiscard]] inline T next(T maximum) noexcept {
            return next(static_cast<T>(0), maximum);
        }

        /**
         * Gets the next value from the engine as an integer with uniform distribution between the min and max.
         *
         * @tparam T The type of integer to return.
         * @param minimum The minimum value to return.
         * @param maximum The maximum value to return.
         * @return The next value from the engine.
         */
        template <::std::integral T>
        [[nodiscard]] inline T next(T minimum, T maximum) noexcept {
            return next(::std::uniform_int_distribution<T>(minimum, maximum));
        }

        /**
         * Gets the next value from the engine with a given distribution.
         *
         * @tparam Distribution The type of the distribution.
         * @param distribution The instance of the distribution.
         * @return The next value from the engine.
         */
        template <class Distribution>
        [[nodiscard]] inline typename Distribution::result_type next(Distribution&& distribution) noexcept {
            ::std::unique_lock lock(_lock);
            return distribution(_engine);
        }

        /**
         * Gets the next value from the engine in a given stream (if the engine is multi-stream-capable) with a given
         * distribution.
         *
         * @tparam Distribution The type of the distribution.
         * @param distribution The instance of the distribution.
         * @param stream The identifier of the stream from which to generate the next value.
         * @return The next value from the engine.
         */
        template <class Distribution, class Eng = Engine,
                ::std::enable_if_t<::gluino::is_multistream_engine_v<Eng>, int> = 0>
        requires ::std::same_as<Eng, Engine>
        [[nodiscard]] typename Distribution::result_type next(
                Distribution&& distribution,
                typename ::gluino::is_multistream_engine<Eng>::template stream_type<Eng> stream) noexcept {
            ::std::unique_lock lock(_lock);
            auto old_stream = ::gluino::is_multistream_engine<Eng>::stream(_engine);
            ::gluino::is_multistream_engine<Eng>::set_stream(_engine, stream);
            auto result = distribution(_engine);
            ::gluino::is_multistream_engine<Eng>::set_stream(_engine, old_stream);
            return result;
        }

        /**
         * Get the next value from the engine as the default result type with a uniform distribution.
         *
         * @return The next value from the engine.
         */
        [[nodiscard]] inline result_type operator()() noexcept {
            return next();
        }

        /**
         * Gets the next value from the engine as a floating point with uniform distribution between 0 and the max.
         *
         * @tparam T The floating point type to return.
         * @param maximum The maximum value to return.
         * @return The next value from the engine.
         */
        template <::std::floating_point T>
        [[nodiscard]] inline T operator()(T maximum) noexcept {
            return next(maximum);
        }

        /**
         * Gets the next value from the engine as a floating point with uniform distribution between the min and max.
         *
         * @tparam T The floating point type to return.
         * @param minimum The minimum value to return.
         * @param maximum The maximum value to return.
         * @return The next value from the engine.
         */
        template <::std::floating_point T>
        [[nodiscard]] inline T operator()(T minimum, T maximum) noexcept {
            return next(minimum, maximum);
        }

        /**
         * Gets the next value from the engine as an integer with uniform distribution between 0 and the max.
         *
         * @tparam T The type of integer to return.
         * @param maximum The maximum value to return.
         * @return The next value from the engine.
         */
        template <::std::integral T>
        [[nodiscard]] inline T operator()(T maximum) noexcept {
            return next(maximum);
        }

        /**
         * Gets the next value from the engine as an integer with uniform distribution between the min and max.
         *
         * @tparam T The type of integer to return.
         * @param minimum The minimum value to return.
         * @param maximum The maximum value to return.
         * @return The next value from the engine.
         */
        template <::std::integral T>
        [[nodiscard]] inline T operator()(T minimum, T maximum) noexcept {
            return next(minimum, maximum);
        }

        /**
         * Gets the next value from the engine with a given distribution.
         *
         * @tparam Distribution The type of the distribution.
         * @param distribution The instance of the distribution.
         * @return The next value from the engine.
         */
        template <class Distribution>
        [[nodiscard]] inline typename Distribution::result_type operator()(Distribution&& distribution) noexcept {
            return next(distribution);
        }

        /**
         * Gets the next value from the engine in a given stream (if the engine is multi-stream-capable) with a given
         * distribution.
         *
         * @tparam Distribution The type of the distribution.
         * @param distribution The instance of the distribution.
         * @param stream The identifier of the stream from which to generate the next value.
         * @return The next value from the engine.
         */
        template <class Distribution, class Eng = Engine,
                ::std::enable_if_t<::gluino::is_multistream_engine_v<Eng>, int> = 0>
        requires ::std::same_as<Eng, Engine>
        [[nodiscard]] inline typename Distribution::result_type operator()(Distribution&& distribution,
                                                                           typename Eng::stream_state stream) noexcept {
            return next(distribution, stream);
        }

        /**
         * Reverts the engine backward a given number of values (only available if the engine supports backstepping).
         *
         * @param delta The numer of values to revert.
         */
        template <class Eng = Engine, ::std::enable_if_t<::gluino::is_backstepping_engine_v<Eng>, int> = 0>
        requires ::std::same_as<Eng, Engine>
        inline void backstep(auto delta) {
            ::std::unique_lock lock(_lock);
            ::gluino::is_backstepping_engine<Eng>::backstep(_engine, delta);
        }

        /**
         * Skips the engine ahead a given number of values.
         *
         * @param delta The number of values to skip.
         */
        inline void advance(auto delta) {
            ::std::unique_lock lock(_lock);
            _engine.discard(delta);
        }

        /**
         * Returns the identifier of the current default stream for a multi-stream-capable engine.
         *
         * @return The identifier of the current default stream for a multi-stream-capable engine.
         */
        template <class Eng = Engine, ::std::enable_if_t<::gluino::is_multistream_engine_v<Eng>, int> = 0>
        requires ::std::same_as<Eng, Engine>
        [[nodiscard]] inline typename ::gluino::is_multistream_engine<Eng>::template stream_type<Eng>
                stream() const noexcept {
            ::std::unique_lock lock(_lock);
            return ::gluino::is_multistream_engine<Eng>::stream(_engine);
        }

        /**
         * Sets the default stream on a multi-stream-capable engine.
         *
         * @param stream The identifier for the stream to set as the default.
         */
        template <class Eng = Engine, ::std::enable_if_t<::gluino::is_multistream_engine_v<Eng>, int> = 0>
        requires ::std::same_as<Eng, Engine>
        inline void set_stream(
                typename ::gluino::is_multistream_engine<Eng>::template stream_type<Eng> stream) noexcept {
            ::std::unique_lock lock(_lock);
            ::gluino::is_multistream_engine<Eng>::set_stream(_engine, stream);
        }

        /**
         * Returns the minimum value that the engine can return.
         *
         * @return The minimum value that the engine can return.
         */
        [[nodiscard]] inline result_type (min)() const noexcept {
            return _engine.min();
        }

        /**
         * Returns the maximum value that the engine can return.
         *
         * @return The maximum value that the engine can return.
         */
        [[nodiscard]] inline result_type (max)() const noexcept {
            return _engine.max();
        }

    private:
        inline void try_init_stream() {
            if constexpr (::gluino::is_multistream_engine_v<Engine>) {
                _engine.set_stream(0);
            }
        }

        template <class Seeder>
        [[nodiscard]] static ::std::seed_seq get_seeds(Seeder&& seeder, uint32_t seed_count) noexcept {
            seed_count = (::std::max)(seed_count, 1u);
            if (seed_count <= max_fast_seed_count) {
                ::std::array<typename Seeder::result_type, max_fast_seed_count> seeds;
                uint32_t i = 0;
                typename decltype(seeds)::iterator end;
                for (end = seeds.begin(); i < seed_count; ++i, ++end) {
                    *end = seeder();
                }
                return ::std::seed_seq(seeds.begin(), end);
            } else {
                ::std::vector<typename Seeder::result_type> seeds;
                seeds.resize(seed_count);
                for (auto& seed: seeds) {
                    seed = seeder();
                }
                return ::std::seed_seq(seeds.begin(), seeds.end());
            }
        }

        Engine _engine;
        Mutex _lock;

        template <class Char, class Traits, class Engine, class Mutex>
        friend ::std::basic_ostream<Char, Traits>& operator<<(::std::basic_ostream<Char, Traits>&,
                                                              ::gluino::random_engine<Engine, Mutex>&);

        template <class Char, class Traits, class Engine, class Mutex>
        friend ::std::basic_istream<Char, Traits>& operator<<(::std::basic_istream<Char, Traits>&,
                                                              ::gluino::random_engine<Engine, Mutex>&);
    };

    template <class Engine = ::gluino::default_random_engine>
    using unsynchronized_random_engine = ::gluino::random_engine<Engine, ::gluino::null_mutex>;

    template <class Engine = ::gluino::default_random_engine>
    using synchronized_random_engine = ::gluino::random_engine<Engine, ::std::mutex>;

    template <class Char, class Traits, class Engine, class Mutex>
    ::std::basic_ostream<Char, Traits>& operator<<(::std::basic_ostream<Char, Traits>& out,
            ::gluino::random_engine<Engine, Mutex>& engine) {
        return out << engine._engine;
    }

    template <class Char, class Traits, class Engine, class Mutex>
    ::std::basic_istream<Char, Traits>& operator<<(::std::basic_istream<Char, Traits>& in,
                                                   ::gluino::random_engine<Engine, Mutex>& engine) {
        return in >> engine._engine;
    }
}
