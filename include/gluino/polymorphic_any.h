#pragma once

#include <any>
#include <typeindex>

#include "concepts.h"
#include "ptr.h"
#include "type_traits.h"
#include "virtual_cast.h"
#include "abi/abi.h"

namespace gluino {
    class polymorphic_any;
}

namespace std {
    template <class O>
    [[nodiscard]] O any_cast(const ::gluino::polymorphic_any& value);

    template <class O>
    [[nodiscard]] O any_cast(const ::gluino::polymorphic_any& value);

    template <class O>
    [[nodiscard]] O any_cast(const ::gluino::polymorphic_any&& value);

    template <class O>
    [[nodiscard]] ::gluino::optional_ptr<O> any_cast(const ::gluino::polymorphic_any* value);

    template <::gluino::polymorphic_pointer O>
    [[nodiscard]] ::gluino::optional_ptr<::std::remove_pointer_t<O>> any_cast(const ::gluino::polymorphic_any& value);

    template <::gluino::polymorphic_pointer O>
    [[nodiscard]] ::gluino::optional_ptr<::std::remove_pointer_t<O>> any_cast(const ::gluino::polymorphic_any&& value);

    template <::gluino::polymorphic_pointer O>
    [[nodiscard]] ::gluino::optional_ptr<::std::remove_pointer_t<O>> any_cast(const ::gluino::polymorphic_any* value);
}

namespace gluino {
    /**
     * Defines an "any" type, with type erasure, similar to <code>std::any</code> but with polymorphic access supported.
     *
     * <p>
     * Unlike <code>std::any</code>, the <code>polymorphic_any</code> understands the polymorphism of any polymorphic
     * pointers which it contains. This allows the contents to be read (via standard <code>std::any_cast</code>) as any
     * type to which the contents could be successfully <code>dynamic_cast</code>ed. To avoid ambiguity, it does not
     * allow polymorphic types to be inserted by reference or value; only pointers to polymorphic types should be
     * inserted.
     * </p>
     *
     * <p>
     * This functionality depends on Gluino's <code>virtual_cast</code>, which is not completely portable. It is known
     * to work on all platforms based on the Itanium ABI and MSVC.
     * </p>
     */
    class polymorphic_any {
    public:
        inline polymorphic_any() noexcept = default;

        inline polymorphic_any(polymorphic_any&&) noexcept = default;

        template <class T>
        inline polymorphic_any(T&& value) : _value(::std::forward<T>(value)) {
        }

        template <::gluino::polymorphic T>
        polymorphic_any(T& value) {
            static_assert(::gluino::dependent_false<T>, "Polymorphic types must be assigned by pointer.");
        }

        template <::gluino::polymorphic T>
        inline polymorphic_any(T* value) {
            _value = polymorphic_value{reinterpret_cast<void*>(const_cast<::std::remove_cv_t<T>*>(value))};
        }

        [[nodiscard]] inline bool is_polymorphic() const noexcept {
            return ::std::type_index(_value.type()) == ::std::type_index(typeid(polymorphic_value));
        }

        [[nodiscard]] inline bool empty() const noexcept {
            return !has_value();
        }

        [[nodiscard]] inline bool has_value() const noexcept {
            return _value.has_value();
        }

        inline void reset() noexcept {
            _value.reset();
        }

        inline void swap(polymorphic_any& other) noexcept {
            _value.swap(other._value);
        }

        template <class T, class... Args>
        inline ::std::decay_t<T>& emplace(Args&&... args) {
            return _value.emplace<T, Args...>(::std::forward<Args>(args)...);
        }

        template <class T, class U, class... Args>
        inline ::std::decay_t<T>& emplace(::std::initializer_list<U> il, Args&&... args) {
            return _value.emplace<T, U, Args...>(il, ::std::forward<Args>(args)...);
        }

        template <::gluino::polymorphic_pointer T>
        inline ::std::decay_t<T>& emplace(T&& pointer) {
            return _value.emplace<T>(::std::forward<T>(pointer));
        }

        template <class T>
        inline polymorphic_any& operator=(T&& value) {
            _value = ::std::forward<T>(value);
        }

        inline polymorphic_any& operator=(const polymorphic_any& other) {
            _value = other._value;
        }

        inline polymorphic_any& operator=(polymorphic_any&& other) noexcept {
            _value = ::std::move(other._value);
        }

    private:
        struct polymorphic_value {
            void* pointer{nullptr};
        };

        ::std::any _value;

        template <class O>
        friend O std::any_cast(const ::gluino::polymorphic_any&);

        template <class O>
        friend O std::any_cast(const ::gluino::polymorphic_any&&);

        template <class O>
        friend ::gluino::optional_ptr<O> std::any_cast(const ::gluino::polymorphic_any*);

        template <::gluino::polymorphic_pointer O>
        friend ::gluino::optional_ptr<::std::remove_pointer_t<O>> std::any_cast(const ::gluino::polymorphic_any&);

        template <::gluino::polymorphic_pointer O>
        friend ::gluino::optional_ptr<::std::remove_pointer_t<O>> std::any_cast(const ::gluino::polymorphic_any&&);

        template <::gluino::polymorphic_pointer O>
        friend ::gluino::optional_ptr<::std::remove_pointer_t<O>> std::any_cast(const ::gluino::polymorphic_any*);
    };
}

namespace std {
    template <class O>
    [[nodiscard]] O any_cast(const ::gluino::polymorphic_any& value) {
        return ::std::any_cast<O>(value._value);
    }

    template <class O>
    [[nodiscard]] O any_cast(const ::gluino::polymorphic_any&& value) {
        return ::std::any_cast<O>(value._value);
    }

    template <class O>
    [[nodiscard]] ::gluino::optional_ptr<O> any_cast(const ::gluino::polymorphic_any* value) {
        return ::std::any_cast<O>(value ? value->_value : nullptr);
    }

    template <::gluino::polymorphic_pointer O>
    [[nodiscard]] ::gluino::optional_ptr<::std::remove_pointer_t<O>> any_cast(
            const ::gluino::polymorphic_any& value) {
        return ::gluino::force_virtual_cast<O>(
                ::std::any_cast<::gluino::polymorphic_any::polymorphic_value>(value).pointer);
    }

    template <::gluino::polymorphic_pointer O>
    [[nodiscard]] ::gluino::optional_ptr<::std::remove_pointer_t<O>> any_cast(const ::gluino::polymorphic_any&& value) {
        return ::gluino::force_virtual_cast<O>(
                ::std::any_cast<::gluino::polymorphic_any::polymorphic_value>(value).pointer);
    }

    template <::gluino::polymorphic_pointer O>
    [[nodiscard]] ::gluino::optional_ptr<::std::remove_pointer_t<O>> any_cast(
            const ::gluino::polymorphic_any* value) {
        return value ? ::gluino::force_virtual_cast<O>(
                ::std::any_cast<::gluino::polymorphic_any::polymorphic_value>(value)->pointer) : nullptr;
    }
}
