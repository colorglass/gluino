#pragma once

#include <stdexcept>

#include "concepts.h"
#include "utf/utf.h"

namespace gluino {
    class bad_string_cast : public ::std::bad_cast {
    public:
        using ::std::bad_cast::bad_cast;
    };

    template <class CharOut, class TraitsOut = ::std::char_traits<CharOut>, class = ::std::allocator<CharOut>, class CharIn = char, class TraitsIn = ::std::char_traits<CharIn>>
    requires (sizeof(CharOut) == sizeof(CharIn))
    inline::std::basic_string_view<CharOut, TraitsOut> string_cast(std::basic_string_view<CharIn, TraitsIn> value, bool* result = nullptr) {
        if (result) {
            *result = true;
        }
        return reinterpret_cast<::std::basic_string_view<CharOut, TraitsOut>&>(value);
    }

    template <class CharOut, class TraitsOut = ::std::char_traits<CharOut>, class AllocOut = ::std::allocator<CharOut>, class CharIn = char, class TraitsIn = ::std::char_traits<CharIn>>
    requires (sizeof(CharOut) != sizeof(CharIn))
    ::std::basic_string<CharOut, TraitsOut, AllocOut> string_cast(std::basic_string_view<CharIn, TraitsIn> value, bool* result = nullptr) {
        ::std::basic_string<CharOut, TraitsOut, AllocOut> out;
        out.reserve(value.size());
        ::std::back_insert_iterator<decltype(out)> inserter(out);
        const CharIn* position = value.data();
        auto end = position + value.size();
        while (position != end) {
            char32_t codepoint = ::boost::locale::utf::utf_traits<CharIn>::
            template decode<const CharIn*>(position, end);
            if (codepoint == ::boost::locale::utf::illegal || codepoint == ::boost::locale::utf::incomplete) {
                if (result) {
                    *result = false;
                } else {
                    throw ::gluino::bad_string_cast();
                }
                continue;
            }
            ::boost::locale::utf::utf_traits<CharOut>::
            template encode<::std::back_insert_iterator<decltype(out)>>(codepoint, inserter);
        }
        if (result) {
            *result = true;
        }
        return ::std::move(out);
    }

    template <class CharOut, class TraitsOut = ::std::char_traits<CharOut>, class AllocOut = ::std::allocator<CharOut>, class CharIn = char>
    inline auto string_cast(const CharIn* value, bool* result = nullptr) {
        return ::gluino::string_cast<CharOut, TraitsOut>(::std::basic_string_view<CharIn>(value), result);
    }


    template <class CharOut, class TraitsOut = ::std::char_traits<CharOut>, class AllocOut = ::std::allocator<CharOut>, class CharIn = char, ::std::size_t Extent = 0>
    requires (sizeof(CharOut) == sizeof(CharIn))
    inline auto string_cast(const CharIn value[Extent], bool* result = nullptr) {
        return ::gluino::string_cast<CharOut, TraitsOut>(::std::basic_string_view<CharIn>(value, Extent), result);
    }

    template <class CharOut, class TraitsOut = ::std::char_traits<CharOut>, class AllocOut = ::std::allocator<CharOut>, class CharIn = char, class TraitsIn = ::std::char_traits<CharIn>, class AllocIn = ::std::allocator<CharIn>>
    requires (sizeof(CharOut) == sizeof(CharIn))
    inline ::std::basic_string<CharOut, TraitsOut, AllocOut>& string_cast(std::basic_string<CharIn, TraitsIn, AllocIn>& value, bool* result = nullptr) {
        if (result) {
            *result = true;
        }
        return reinterpret_cast<::std::basic_string<CharOut, TraitsOut, AllocOut>&>(value);
    }

    template <class CharOut, class TraitsOut = ::std::char_traits<CharOut>, class AllocOut = ::std::allocator<CharOut>, class CharIn = char, class TraitsIn = ::std::char_traits<CharIn>, class AllocIn = ::std::allocator<CharIn>>
    requires (sizeof(CharOut) == sizeof(CharIn))
    inline const ::std::basic_string<CharOut, TraitsOut, AllocOut>& string_cast(const std::basic_string<CharIn, TraitsIn, AllocIn>& value, bool* result = nullptr) {
        if (result) {
            *result = true;
        }
        return reinterpret_cast<const ::std::basic_string<CharOut, TraitsOut, AllocOut>&>(value);
    }

    template <class CharOut, class TraitsOut = ::std::char_traits<CharOut>, class AllocOut = ::std::allocator<CharOut>, class CharIn = char, class TraitsIn = ::std::char_traits<CharIn>, class AllocIn = ::std::allocator<CharIn>>
    requires (sizeof(CharOut) == sizeof(CharIn))
    inline ::std::basic_string<CharOut, TraitsOut, AllocOut> string_cast(const std::basic_string<CharIn, TraitsIn, AllocIn>&& value, bool* result = nullptr) {
        if (result) {
            *result = true;
        }
        return reinterpret_cast<::std::basic_string<CharOut, TraitsOut, AllocOut>&>(value);
    }

    template <class CharOut, class TraitsOut = ::std::char_traits<CharOut>, class AllocOut = ::std::allocator<CharOut>, class CharIn = char, class TraitsIn = ::std::char_traits<CharIn>, class AllocIn = ::std::allocator<CharIn>>
    requires (sizeof(CharOut) != sizeof(CharIn))
    inline auto string_cast(std::basic_string<CharIn, TraitsIn, AllocIn>& value, bool* result = nullptr) {
        return ::gluino::string_cast<CharOut, TraitsOut, AllocOut>(::std::basic_string_view<CharIn, TraitsIn>(value), result);
    }

    template <class CharOut, class TraitsOut = ::std::char_traits<CharOut>, class AllocOut = ::std::allocator<CharOut>, class CharIn = char, class TraitsIn = ::std::char_traits<CharIn>, class AllocIn = ::std::allocator<CharIn>>
    requires (sizeof(CharOut) != sizeof(CharIn))
    inline auto string_cast(const std::basic_string<CharIn, TraitsIn, AllocIn>&& value, bool* result = nullptr) {
        return ::gluino::string_cast<CharOut, TraitsOut, AllocOut>(::std::basic_string_view<CharIn, TraitsIn>(value), result);
    }
}

#define gluino_auto(var, expr)
