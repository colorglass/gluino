#pragma once

#include "map.h"

#ifndef GLUINO_CHAR_MIN_FAST_FIND
#define GLUINO_CHAR_MIN_FAST_FIND 32
#endif
#ifndef GLUINO_WCHAR_MIN_FAST_FIND
#define GLUINO_WCHAR_MIN_FAST_FIND 128
#endif
#ifndef GLUINO_WCHAR_FAST_FIND_MAP_TYPE
#define GLUINO_WCHAR_FAST_FIND_MAP_TYPE ::std::unordered_map
#endif

namespace gluino {
    template <class Char, class Traits = ::std::char_traits<Char>>
    class basic_case_insensitive_char {
    public:
        using case_sensitive_type = ::std::basic_string_view<Char, Traits>;
        using value_type = typename case_sensitive_type::value_type;
        using traits_type = typename case_sensitive_type::traits_type;

        constexpr basic_case_insensitive_char() noexcept = default;

        constexpr basic_case_insensitive_char(value_type value) noexcept
            : _value(value) {
        }

        [[nodiscard]] constexpr explicit operator value_type() const noexcept {
            return raw();
        }

        template <class OutChar, class OutTraits = ::std::char_traits<OutChar>>
        requires (::std::convertible_to<Char, OutChar>)
        [[nodiscard]] constexpr explicit operator
            ::gluino::basic_case_insensitive_char<OutChar, OutTraits>() const noexcept {
            return {static_cast<OutChar>(_value)};
        }

        template <class OutChar>
        requires (::std::convertible_to<Char, OutChar>)
            [[nodiscard]] constexpr explicit operator OutChar() const noexcept {
            return static_cast<OutChar>(_value);
        }

        [[nodiscard]] constexpr value_type raw() const noexcept {
            return _value;
        }

        [[nodiscard]] constexpr value_type upper() const noexcept {
            return static_cast<value_type>(::std::toupper(_value));
        }

        [[nodiscard]] constexpr value_type lower() const noexcept {
            return static_cast<value_type>(::std::tolower(_value));
        }

        [[nodiscard]] constexpr bool operator==(value_type other) const noexcept {
            return ::std::toupper(_value) == ::std::toupper(other);
        }

        [[nodiscard]] constexpr bool operator==(basic_case_insensitive_char other) const noexcept {
            return ::std::toupper(_value) == ::std::toupper(other._value);
        }

        [[nodiscard]] constexpr auto operator<=>(value_type other) const noexcept {
            return ::std::toupper(_value) <=> ::std::toupper(other);
        }

        [[nodiscard]] constexpr auto operator<=>(basic_case_insensitive_char other) const noexcept {
            return *this <=> other._value;
        }
        
    private:
        value_type _value;
    };
    
    template <class Char, class Traits = ::std::char_traits<Char>>
    class basic_case_insensitive_string_view {
    public:
        using case_sensitive_type = ::std::basic_string_view<Char, Traits>;
        using case_insensitive_value_type = basic_case_insensitive_char<Char, Traits>;
        using value_type = typename case_sensitive_type::value_type;
        using traits_type = typename case_sensitive_type::traits_type;
        using pointer = typename case_sensitive_type::pointer;
        using const_pointer = typename case_sensitive_type::const_pointer;
        using reference = typename case_sensitive_type::reference;
        using const_reference = typename case_sensitive_type::const_reference;
        using size_type = typename case_sensitive_type::size_type;
        using difference_type = typename case_sensitive_type::difference_type;

        static constexpr size_type npos = case_sensitive_type::npos;

        template <bool Const, bool Reverse>
        class detail_iterator {
        public:
            inline detail_iterator& operator++() noexcept {
                if constexpr (Reverse) {
                    --_value;
                } else {
                    ++_value;
                }
                return *this;
            }

            detail_iterator operator++(int) const noexcept {
                detail_iterator current = *this;
                ++(*this);
                return current;
            }

            inline detail_iterator& operator--() noexcept {
                if constexpr (Reverse) {
                    ++_value;
                } else {
                    --_value;
                }
                return *this;
            }

            detail_iterator operator--(int) const noexcept {
                detail_iterator current = *this;
                --(*this);
                return current;
            }

            [[nodiscard]] inline detail_iterator operator+(::std::size_t value) const noexcept {
                if constexpr (Reverse) {
                    return detail_iterator(_value - value);
                } else {
                    return detail_iterator(_value + value);
                }
            }

            [[nodiscard]] inline detail_iterator operator-(::std::size_t value) const noexcept {
                if constexpr (Reverse) {
                    return detail_iterator(_value + value);
                } else {
                    return detail_iterator(_value - value);
                }
            }

            [[nodiscard]] inline auto operator<=>(detail_iterator other) const noexcept {
                if constexpr (Reverse) {
                    return other._value <=> _value;
                } else {
                    return _value <=> other._value;
                }
            }

            [[nodiscard]] inline bool operator==(detail_iterator other) const noexcept {
                if constexpr (Reverse) {
                    return other._value == _value;
                } else {
                    return _value == other._value;
                }
            }

            template <bool U = Const, ::std::enable_if_t<!U, int> = 0>
            [[nodiscard]] inline ::std::conditional_t<Const, const case_insensitive_value_type*,
                                                      case_insensitive_value_type*> operator->() noexcept {
                return _value;
            }

            [[nodiscard]] inline const case_insensitive_value_type* operator->() const noexcept {
                return _value;
            }

            template <bool U = Const, ::std::enable_if_t<!U, int> = 0>
            [[nodiscard]] inline ::std::conditional_t<Const, const case_insensitive_value_type&,
                                                      case_insensitive_value_type&> operator*() noexcept {
                return _value;
            }

            [[nodiscard]] inline const case_insensitive_value_type& operator*() const noexcept {
                return *_value;
            }

        private:
            inline explicit detail_iterator(::std::conditional_t<Const, const case_insensitive_value_type*,
                                                                 case_insensitive_value_type*> character) noexcept
                : _value(character) {
            }

            ::std::conditional_t<Const, const case_insensitive_value_type*,
                                 case_insensitive_value_type*> _value{nullptr};

            friend class basic_case_insensitive_string_view;
        };

        using iterator = detail_iterator<true, false>;

        using const_iterator = detail_iterator<true, false>;

        using reverse_iterator = detail_iterator<true, true>;

        using const_reverse_iterator = detail_iterator<true, true>;

        constexpr basic_case_insensitive_string_view() noexcept = default;

        template <class T>
        constexpr basic_case_insensitive_string_view(T&& value) noexcept
            : _value(::std::forward<T>(value)) {
        }

        template <class T>
        requires (::std::same_as<::std::remove_cvref_t<T>, basic_case_insensitive_string_view>)
        constexpr basic_case_insensitive_string_view(const T& value) noexcept
            : _value(value._value) {
        }

        template <class T>
        requires (::std::same_as<::std::remove_cvref_t<T>, basic_case_insensitive_string_view>)
        constexpr basic_case_insensitive_string_view(T&& value) noexcept
            : _value(::std::move(value._value)) {
        }

        template <class T>
        constexpr basic_case_insensitive_string_view(T&& value, ::std::size_t length) noexcept
            : _value(::std::forward<T>(value), length) {
        }

        template <class T>
        requires (::std::same_as<::std::remove_cvref_t<T>, basic_case_insensitive_string_view>)
        constexpr basic_case_insensitive_string_view(const T& value,
                                                     ::std::size_t length) noexcept
            : _value(value._value, length) {
        }

        template <class T>
        requires (::std::same_as<::std::remove_cvref_t<T>, basic_case_insensitive_string_view>)
        constexpr basic_case_insensitive_string_view(T&& value,
                                                     ::std::size_t length) noexcept
            : _value(::std::move(value._value), length) {
        }

        [[nodiscard]] constexpr size_type length() const noexcept {
            return _value.length();
        }

        [[nodiscard]] constexpr size_type size() const noexcept {
            return _value.size();
        }

        [[nodiscard]] constexpr bool empty() const noexcept {
            return _value.empty();
        }

        [[nodiscard]] constexpr size_type max_size() const noexcept {
            return case_sensitive_type().max_size();
        }

        [[nodiscard]] inline const case_insensitive_value_type& operator[](size_type index) const noexcept {
            return reinterpret_cast<const case_insensitive_value_type&>(_value[index]);
        }

        [[nodiscard]] inline const case_insensitive_value_type& at(size_type index) const {
            return reinterpret_cast<const case_insensitive_value_type&>(_value.at(index));
        }

        [[nodiscard]] inline const case_insensitive_value_type& front() const {
            return reinterpret_cast<const case_insensitive_value_type&>(_value.front());
        }

        [[nodiscard]] inline const case_insensitive_value_type& back() const {
            return reinterpret_cast<const case_insensitive_value_type&>(_value.back());
        }

        [[nodiscard]] constexpr const_pointer data() const noexcept {
            return _value.data();
        }

        template <class T>
        [[nodiscard]] bool starts_with(const T& substring) const noexcept {
            basic_case_insensitive_string_view view(substring);
            auto it = begin();
            auto io = view.begin();
            for (; it != end() && io != view.end(); ++it, ++io) {
                if (*it != *io) {
                    return false;
                }
            }
            return true;
        }

        template <class T>
        [[nodiscard]] bool ends_with(const T& substring) const noexcept {
            basic_case_insensitive_string_view view(substring);
            auto it = begin() + size() - view.size();
            auto io = view.begin();
            for (; it != end() && io != view.end(); ++it, ++io) {
                if (*it != *io) {
                    return false;
                }
            }
            return true;
        }
        
        template <class T>
        [[nodiscard]] bool contains(const T& substring) const noexcept {
            return find(substring) != npos;
        }

        [[nodiscard]] basic_case_insensitive_string_view substr(size_type pos = 0, size_type count = npos) const {
            return {_value.substr(pos, count)};
        }

        template <class T>
        [[nodiscard]] size_type find(const T& substring, size_type pos = 0) const noexcept {
            basic_case_insensitive_string_view view(substring);
            if (view.size() > size() - pos) {
                return npos;
            }
            if constexpr (sizeof(value_type) == 1) {
                if (view.size() < GLUINO_CHAR_MIN_FAST_FIND) {
                    return basic_find(view, pos);
                }
                return advanced_find(view, pos);
            } else {
                if (view.size() < GLUINO_WCHAR_MIN_FAST_FIND) {
                    return basic_find(view, pos);
                }
                return advanced_hash_find(view, pos);
            }
        }

        [[nodiscard]] size_type find(const value_type* substring, size_type pos, size_type count) const noexcept {
            return find(basic_case_insensitive_string_view(substring, count), pos);
        }

        template <class T>
        [[nodiscard]] size_type rfind(const T& substring, size_type pos = npos) const noexcept {
            basic_case_insensitive_string_view view(substring);
            if (view.size() > size() - pos) {
                return npos;
            }
            return basic_find<true>(view, pos != npos ? pos : size() - 1);
        }

        template <class T>
        [[nodiscard]] size_type rfind(const value_type* substring, size_type pos, size_type count) const noexcept {
            return rfind(basic_case_insensitive_string_view(substring, count), pos);
        }

        [[nodiscard]] constexpr size_type find_first_of(case_sensitive_type chars, size_type pos = 0) const noexcept {
            return find_first_of(case_insensitive_string_view(chars), pos);
        }

        [[nodiscard]] constexpr size_type find_first_of(basic_case_insensitive_string_view chars,
                                                        size_type pos = 0) const noexcept {
            for (size_type i = pos; i < _value.size(); ++i) {
                if (chars.contains(_value[i])) {
                    return i;
                }
            }
            return npos;
        }

        constexpr size_type find_first_of(const value_type* chars, size_type pos, size_type count) const {
            return find_first_of(basic_case_insensitive_string_view(chars, count), pos);
        }

        constexpr size_type find_first_of(const value_type* chars, size_type pos) const {
            return find_first_of(case_insensitive_string_view(chars), pos);
        }

        [[nodiscard]] constexpr size_type find_last_of(case_sensitive_type chars, size_type pos = 0) const noexcept {
            return find_last_of(case_insensitive_string_view(chars), pos);
        }

        [[nodiscard]] constexpr size_type find_last_of(basic_case_insensitive_string_view chars,
                                                        size_type pos = 0) const noexcept {
            for (size_type i = _value.size() - 1; i >= 0; --i) {
                if (chars.contains(_value[i])) {
                    return i;
                }
            }
            return npos;
        }

        constexpr size_type find_last_of(const value_type* chars, size_type pos, size_type count) const {
            return find_last_of(basic_case_insensitive_string_view(chars, count), pos);
        }

        constexpr size_type find_last_of(const value_type* chars, size_type pos) const {
            return find_last_of(case_insensitive_string_view(chars), pos);
        }

        [[nodiscard]] constexpr size_type find_first_not_of(case_sensitive_type chars,
                                                            size_type pos = 0) const noexcept {
            return find_first_not_of(case_insensitive_string_view(chars), pos);
        }

        [[nodiscard]] constexpr size_type find_first_not_of(basic_case_insensitive_string_view chars,
                                                        size_type pos = 0) const noexcept {
            for (size_type i = pos; i < _value.size(); ++i) {
                if (!chars.contains(_value[i])) {
                    return i;
                }
            }
            return npos;
        }

        constexpr size_type find_first_not_of(const value_type* chars, size_type pos, size_type count) const {
            return find_first_of(basic_case_insensitive_string_view(chars, count), pos);
        }

        constexpr size_type find_first_not_of(const value_type* chars, size_type pos) const {
            return find_first_of(case_insensitive_string_view(chars), pos);
        }

        [[nodiscard]] constexpr size_type find_last_not_of(case_sensitive_type chars,
                                                               size_type pos = 0) const noexcept {
            return find_last_not_of(case_insensitive_string_view(chars), pos);
        }

        [[nodiscard]] constexpr size_type find_last_not_of(basic_case_insensitive_string_view chars,
                                                            size_type pos = 0) const noexcept {
            for (size_type i = _value.size() - 1; i >= 0; --i) {
                if (!chars.contains(_value[i])) {
                    return i;
                }
            }
            return npos;
        }

        constexpr size_type find_last_not_of(const value_type* chars, size_type pos, size_type count) const {
            return find_last_not_of(basic_case_insensitive_string_view(chars, count), pos);
        }

        constexpr size_type find_last_not_of(const value_type* chars, size_type pos) const {
            return find_last_not_of(case_insensitive_string_view(chars), pos);
        }

        constexpr void remove_prefix(size_type n) {
            _value.remove_prefix(n);
        }

        constexpr void remove_suffix(size_type n) {
            _value.remove_suffix(n);
        }

        constexpr void swap(case_sensitive_type other) noexcept {
            _value.swap(other);
        }

        constexpr void swap(basic_case_insensitive_string_view other) noexcept {
            _value.swap(other._value);
        }

        [[nodiscard]] const_iterator begin() noexcept {
            return const_iterator(reinterpret_cast<const case_insensitive_value_type*>(_value.data()));
        }

        [[nodiscard]] const_iterator begin() const noexcept {
            return const_iterator(reinterpret_cast<const case_insensitive_value_type*>(_value.data()));
        }

        [[nodiscard]] const_iterator end() noexcept {
            return const_iterator(reinterpret_cast<const case_insensitive_value_type*>(_value.data() + _value.size()));
        }

        [[nodiscard]] const_iterator end() const noexcept {
            return const_iterator(reinterpret_cast<const case_insensitive_value_type*>(_value.data() + _value.size()));
        }

        [[nodiscard]] const_iterator cbegin() const noexcept {
            return const_iterator(reinterpret_cast<const case_insensitive_value_type*>(_value.data()));
        }

        [[nodiscard]] const_iterator cend() const noexcept {
            return const_iterator(reinterpret_cast<const case_insensitive_value_type*>(_value.data() + _value.size()));
        }

        [[nodiscard]] const_reverse_iterator rbegin() noexcept {
            return const_reverse_iterator(reinterpret_cast<const case_insensitive_value_type*>(_value.data()));
        }

        [[nodiscard]] const_reverse_iterator rbegin() const noexcept {
            return const_reverse_iterator(reinterpret_cast<const case_insensitive_value_type*>(_value.data()));
        }

        [[nodiscard]] const_reverse_iterator rend() noexcept {
            return const_reverse_iterator(reinterpret_cast<const case_insensitive_value_type*>(
                _value.data() + _value.size()));
        }

        [[nodiscard]] const_reverse_iterator rend() const noexcept {
            return const_reverse_iterator(reinterpret_cast<const case_insensitive_value_type*>(
                _value.data() + _value.size()));
        }

        [[nodiscard]] const_reverse_iterator crbegin() const noexcept {
            return const_reverse_iterator(reinterpret_cast<const case_insensitive_value_type*>(_value.data()));
        }

        [[nodiscard]] const_reverse_iterator crend() const noexcept {
            return const_reverse_iterator(reinterpret_cast<const case_insensitive_value_type*>(
                _value.data() + _value.size()));
        }

        template <class T>
        [[nodiscard]] inline auto operator<=>(T&& other) const noexcept {
            basic_case_insensitive_string_view view(::std::forward<T>(other));
            for (::std::size_t i = 0; i < size(); ++i) {
                if (i >= view.size()) {
                    return ::std::strong_ordering::greater;
                }
                auto comparison = at(i) <=> view.at(i);
                switch (comparison) {
                    case ::std::strong_ordering::equal:
                        continue;
                    default:
                        return comparison;
                }
            }
            return size() < view.size() ? ::std::strong_ordering::less : ::std::strong_ordering::equal;
        }

        template <class T>
        [[nodiscard]] inline auto operator==(T&& other) const noexcept {
            basic_case_insensitive_string_view view(::std::forward<T>(other));
            if (size() != view.size()) {
                return false;
            }
            for (::std::size_t i = 0; i < size(); ++i) {
                if (at(i) != view.at(i)) {
                    return false;
                }
            }
            return true;
        }

        template <class T>
        [[nodiscard]] inline int compare(T&& other) const noexcept {
            switch (*this <=> ::std::forward<T>(other)) {
                case ::std::strong_ordering::less:
                    return -1;
                case ::std::strong_ordering::greater:
                    return 1;
                default:
                    return 0;
            }
        }

    private:
        template <bool Reverse = false>
        [[nodiscard]] ::std::size_t basic_find(basic_case_insensitive_string_view needle,
                                               ::std::size_t pos) const noexcept {
            for (::std::size_t i = pos; i < size();) {
                if (substr(i, needle.size()) == needle) {
                    return i;
                }
                if constexpr (Reverse) {
                    --i;
                } else {
                    ++i;
                }
            }
            return npos;
        }

        [[nodiscard]] ::std::size_t advanced_find(basic_case_insensitive_string_view needle,
                                                  ::std::size_t pos) const noexcept {
            ::std::size_t table[256];

            for (::std::size_t i = 0; i < 256; ++i) {
                table[i] = needle.size();
            }
            for (::std::size_t i = 0; i < needle.size() - 1; ++i) {
                table[needle[i].upper()] = needle.size() - 1 - i;
            }

            ::std::size_t skip = pos;
            while (size() - skip >= needle.size()) {
                if (substr(skip, needle.size()) == needle) {
                    return skip;
                }
                skip += table[at(skip + needle.size() - 1).upper()];
            }
            return npos;
        }

        [[nodiscard]] ::std::size_t advanced_hash_find(basic_case_insensitive_string_view needle,
                                                  ::std::size_t pos) const noexcept {
            GLUINO_WCHAR_FAST_FIND_MAP_TYPE<value_type, ::std::size_t> table;

            for (::std::size_t i = 0; i < needle.size() - 1; ++i) {
                table[needle[i].upper()] = needle.size() - 1 - i;
            }

            ::std::size_t skip = pos;
            while (size() - skip >= needle.size()) {
                if (substr(skip, needle.size()) == needle) {
                    return skip;
                }
                ::std::size_t extra_skip = needle.size();
                auto result = table.find(at(skip + needle.size() - 1).upper());
                if (result != table.end()) {
                    extra_skip = result->second;
                }
                skip += extra_skip;
            }
            return npos;
        }

        [[nodiscard]] static constexpr size_type size_of(const value_type* str) {
            return ::std::basic_string_view<value_type, traits_type>(str).size();
        }

        case_sensitive_type _value;
    };

    using case_insensitive_string_view = ::gluino::basic_case_insensitive_string_view<char>;
    using case_insensitive_wstring_view = ::gluino::basic_case_insensitive_string_view<wchar_t>;
    using case_insensitive_u8string_view = ::gluino::basic_case_insensitive_string_view<char8_t>;
    using case_insensitive_u16string_view = ::gluino::basic_case_insensitive_string_view<char16_t>;
    using case_insensitive_u32string_view = ::gluino::basic_case_insensitive_string_view<char32_t>;

    using case_insensitive_char = ::gluino::basic_case_insensitive_char<char>;
    using case_insensitive_wchar = ::gluino::basic_case_insensitive_char<wchar_t>;
    using case_insensitive_char8 = ::gluino::basic_case_insensitive_char<char8_t>;
    using case_insensitive_char16 = ::gluino::basic_case_insensitive_char<char16_t>;
    using case_insensitive_char32 = ::gluino::basic_case_insensitive_char<char32_t>;
}

namespace std {
    template <class Char, class Traits>
    struct hash<::gluino::basic_case_insensitive_char<Char, Traits>> {
        [[nodiscard]] inline ::std::size_t operator()(
            ::gluino::basic_case_insensitive_char<Char, Traits> value) const noexcept {
            return ::std::hash<Char>{}(value.upper());
        }
    };

    template <class Char, class Traits>
    struct hash<::gluino::basic_case_insensitive_string_view<Char, Traits>> {
        [[nodiscard]] inline ::std::size_t operator()(
            ::gluino::basic_case_insensitive_string_view<Char, Traits> value) const noexcept {
            return ::gluino::str_hash<false>{}(value);
        }
    };
}
