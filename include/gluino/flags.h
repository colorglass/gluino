#pragma once

#include <stdexcept>
#include <vector>

#include "concepts.h"
#include "container.h"
#include "enumeration.h"

namespace gluino {
    class flags_traits {
    };

    template <class T>
    struct is_flags : public ::std::is_base_of<::gluino::flags_traits, T> {
    };

    template <class T>
    static constexpr bool is_flags_v = ::gluino::is_flags<T>::value;

    template <class T>
    concept flags = ::gluino::is_flags_v<T> && requires(T& flags) {
        typename T::enum_type;
        typename T::underlying_type;
        { flags.names() } -> ::std::same_as<::std::vector<::std::string_view>>;
    };

    template <class T, class Flags>
    concept implicit_flag_source = ::std::same_as<T, Flags> || ::std::convertible_to<T, typename Flags::enum_type>;

    template <class T, class Flags>
    concept explicit_flag_source = ::gluino::implicit_flag_source<T, Flags> ||
            ::std::convertible_to<T, ::std::string_view> ||
                    ::gluino::as_narrow_or_narrower<T, ::std::underlying_type_t<typename Flags::enum_type>>;
}

namespace std {
    template <::gluino::flags T>
    [[nodiscard]] ::std::string to_string(T& flags) noexcept {
        auto names = flags.names();
        ::std::string result;
        for (auto iter = names.begin(); iter != names.end(); ++iter) {
            if (!iter.empty()) {
                iter += " | ";
            }
            iter += *iter;
        }
        return result;
    }

    template <>
    struct hash<::gluino::flags_traits> {
        template <::gluino::flags T>
        [[nodiscard]] inline ::std::size_t operator()(const T& value) const noexcept {
            ::std::hash<typename T::underlying_type> h;
            return h(static_cast<typename T::underlying_type>(value));
        }
    };
}

namespace boost {
    template <::gluino::flags T>
    [[nodiscard]] inline ::std::size_t hash_value(const T& value) noexcept {
        ::std::hash<T> h;
        return h(value);
    }
}

#define __GLUINO_FLAGS_MEMBER(pair, name) GLUINO_APPLY(GLUINO_FIRST, GLUINO_FLATTEN(pair)) = GLUINO_APPLY(GLUINO_REST, GLUINO_FLATTEN(pair))

#define __GLUINO_FLAGS_CONSTANT(pair, ctx) (GLUINO_APPLY(GLUINO_REST, GLUINO_FLATTEN(pair)))

#define __GLUINO_FLAGS_CONSTANT_DECL(pair, name) static const name GLUINO_APPLY(GLUINO_FIRST, GLUINO_FLATTEN(pair));

#define __GLUINO_FLAGS_CONSTANT_DEF(pair, name) inline constexpr name name::GLUINO_APPLY(GLUINO_FIRST, GLUINO_FLATTEN(pair)) = name(name::enum_type::GLUINO_APPLY(GLUINO_FIRST, GLUINO_FLATTEN(pair)), name::ignore_validation_tag_t{})

#define __GLUINO_FLAGS_ENTRY(pair, name) entry = ::gluino::insert(_entries, ::std::make_pair(GLUINO_APPLY(GLUINO_FIRST, GLUINO_FLATTEN(pair)), GLUINO_APPLY(GLUINO_DEFER(GLUINO_STRING), GLUINO_APPLY(GLUINO_FIRST, GLUINO_FLATTEN(pair))))); \
::gluino::emplace(_enum_entries, GLUINO_APPLY(GLUINO_FIRST, GLUINO_FLATTEN(pair)));                                                                                                                                                           \
::gluino::emplace(_names, entry->second, entry);

#define gluino_flags(enum_name, base, ...) class enum_name \
: public ::gluino::flags_traits {                          \
    struct ignore_validation_tag_t {                       \
    };                                                     \
                                                           \
public:                                                    \
    enum class enum_type : base {                          \
        GLUINO_MAP(__GLUINO_FLAGS_MEMBER, enum_name, __VA_ARGS__) \
    };                                                     \
                                                           \
    using underlying_type = ::std::underlying_type_t<enum_type>;  \
                                                           \
    constexpr enum_name() noexcept = default;              \
                                                           \
    template <class... Args>                               \
    requires ((::gluino::explicit_flag_source<Args, enum_name> && ...) && !(::gluino::implicit_flag_source<Args, enum_name> && ...)) \
    explicit inline enum_name(Args... values) {            \
        set(values...);                                    \
    }                                                      \
                                                           \
    template <class... Args>                               \
    requires(::gluino::implicit_flag_source<Args, enum_name> && ...)                                                                 \
    inline enum_name(Args... values) {                     \
        set(values...);                                    \
    }                                                      \
                                                           \
    constexpr enum_name(const enum_name& other) noexcept   \
            : value(other.value) {                         \
    }                                                      \
                                                           \
    inline enum_name(enum_name&& other) noexcept {         \
        *const_cast<enum_type*>(&value) = other.value;     \
        *const_cast<enum_type*>(&other.value) = static_cast<enum_type>(0);                                                           \
    }                                                      \
                                                           \
    template <class... Args>                               \
    requires(::gluino::explicit_flag_source<Args, enum_name> && ...)                                                                 \
    [[nodiscard]] constexpr bool any(Args... others) const noexcept {                                                                \
        return (static_cast<underlying_type>(value) & (static_cast<underlying_type>(others) | ...)) != static_cast<underlying_type>(0); \
    }                                                      \
                                                           \
    template <class... Args>                               \
    requires(::gluino::explicit_flag_source<Args, enum_name> && ...)                                                                 \
    [[nodiscard]] constexpr bool all(Args... others) const noexcept {                                                                \
        auto expected{(static_cast<underlying_type>(others) | ...)};                                                                 \
        return (static_cast<underlying_type>(value) & expected) == expected;                                                         \
    }                                                      \
                                                           \
    [[nodiscard]] constexpr bool has(enum_name val) const noexcept {                                                                 \
        return all(val);                                   \
    }                                                      \
                                                           \
    template <class... Args>                               \
    requires(::gluino::explicit_flag_source<Args, enum_name> && ...)                                                                 \
    [[nodiscard]] inline bool none(Args... others) const noexcept {                                                                  \
        return (static_cast<underlying_type>(value) & (static_cast<underlying_type>(others) | ...)) == 0;                            \
    }                                                      \
                                                           \
    template <class... Args> requires(::gluino::explicit_flag_source<Args, enum_name> && ...)                                        \
    inline enum_name& set(Args... others) {                \
        auto val = static_cast<underlying_type>(value);    \
        val |= (to_underlying(others) | ...);              \
        *const_cast<enum_type*>(&value) = static_cast<enum_type>(val);                                                               \
        return *this;                                      \
    }                                                      \
                                                           \
    template <class... Args> requires(::gluino::explicit_flag_source<Args, enum_name> && ...)                                        \
    inline enum_name& unset(Args... others) {              \
        auto val = static_cast<underlying_type>(value);    \
        val &= ~(to_underlying(others) | ...);             \
        *const_cast<enum_type*>(&value) = static_cast<enum_type>(val);                                                               \
        return static_cast<enum_name&>(*this);             \
    }                                                      \
                                                           \
    inline enum_name& reset() noexcept {                   \
        *const_cast<enum_type*>(&value) = static_cast<enum_type>(0);                                                                 \
        return *this;                                      \
    }                                                      \
                                                           \
    [[nodiscard]] static inline ::std::span<const enum_name> values() noexcept {                                                     \
        return ::gluino::lazy_singleton<static_state>::value()._enum_entries;                                                        \
    }                                                      \
                                                           \
    [[nodiscard]] constexpr enum_name operator&(enum_name other) const noexcept {                                                    \
        return enum_name(static_cast<enum_type>(static_cast<underlying_type>(value) & static_cast<underlying_type>(other)), ignore_validation_tag_t{}); \
    }                                                      \
                                                           \
    [[nodiscard]] constexpr enum_name operator|(enum_name other) const noexcept {                                                    \
        return enum_name(static_cast<enum_type>(static_cast<underlying_type>(value) | static_cast<underlying_type>(other.value)), ignore_validation_tag_t{}); \
    }                                                      \
                                                           \
    [[nodiscard]] constexpr enum_name operator~() const noexcept {\
        return enum_name(static_cast<enum_type>(~static_cast<underlying_type>(value) & _all_set), ignore_validation_tag_t{});        \
    }                                                      \
                                                           \
    [[nodiscard]] constexpr enum_name operator-(enum_name other) const noexcept {                                                    \
        return operator&(~other);                          \
    }                                                      \
                                                           \
    [[nodiscard]] constexpr enum_name operator+(enum_name other) const noexcept {                                                    \
        return operator|(other);                           \
    }                                                      \
                                                           \
    template <class T>                                     \
    requires ::std::convertible_to<underlying_type, T>     \
    [[nodiscard]] constexpr explicit operator T() const noexcept {\
        return static_cast<T>(value);                      \
    }                                                      \
                                                           \
    [[nodiscard]] constexpr bool operator==(enum_name other) const noexcept {                                                        \
        return value == other.value;                       \
    }                                                      \
                                                           \
    inline enum_name& operator=(const enum_name& other) noexcept {\
        *const_cast<enum_type*>(&value) = other.value;     \
        return *this;                                      \
    }                                                      \
                                                           \
    inline enum_name& operator=(enum_name&& other) noexcept {     \
        *const_cast<enum_type*>(&value) = other.value;     \
        *const_cast<enum_type*>(&other.value) = static_cast<enum_type>(0);                                                           \
        return *this;                                      \
    }                                                      \
                                                           \
    [[nodiscard]] inline ::std::vector<::std::string_view> names() const noexcept {                                                  \
        ::std::vector<::std::string_view> results;         \
        for (auto& entry : ::gluino::lazy_singleton<static_state>::value()._entries) {                                               \
            if (has(entry.first)) {                        \
                results.push_back(entry.second);           \
            }                                              \
        }                                                  \
        return results;                                    \
    }                                                      \
                                                           \
    [[nodiscard]] inline ::std::vector<enum_name> set_flags() const {                                                                \
        ::std::vector<enum_name> results;                  \
        for (auto& entry : ::gluino::lazy_singleton<static_state>::value()._entries) {                                               \
            if (has(entry.first)) {                        \
                results.push_back(entry.first);            \
            }                                              \
        }                                                  \
        return results;                                    \
    }                                                      \
                                                           \
    GLUINO_MAP_CONCAT(__GLUINO_FLAGS_CONSTANT_DECL, enum_name, __VA_ARGS__)                                                          \
                                                           \
    const enum_type value{0};                              \
                                                           \
private:                                                   \
    struct static_state {                                  \
        static_state() noexcept {                          \
            _entries.reserve(GLUINO_COUNT(__VA_ARGS__));   \
            ::gluino::reserve(_names, GLUINO_COUNT(__VA_ARGS__)); \
            decltype(_entries)::iterator entry;            \
            GLUINO_MAP_CONCAT(__GLUINO_FLAGS_ENTRY, enum_name, __VA_ARGS__)                                                          \
        }                                                  \
                                                           \
        ::std::vector<::std::pair<enum_name, ::std::string_view>> _entries;                                                          \
        ::std::vector<enum_name> _enum_entries;            \
        GLUINO_OVERRIDE_ENUM_MAP_TYPE<::std::string_view, decltype(_entries)::iterator> _names;                                     \
    };                                                     \
                                                           \
    constexpr explicit enum_name(enum_type val, ignore_validation_tag_t) noexcept                                                    \
            : value(val) {                                 \
    }                                                      \
                                                           \
    [[nodiscard]] static inline underlying_type to_underlying(underlying_type val) {                                                 \
        if ((_all_set & val) != val) {                     \
            throw ::std::invalid_argument("Value included bits not defined to be set.");                                             \
        }                                                  \
        return val;                                        \
    }                                                      \
                                                           \
    [[nodiscard]] static inline underlying_type to_underlying(::std::string_view name) {                                             \
        auto& names = ::gluino::lazy_singleton<static_state>::value()._names;                                                        \
        auto result = names.find(name.data());             \
        if (result == names.end()) {                       \
            throw ::std::invalid_argument("Flag name is not defined on flag enumeration.");                                          \
        }                                                  \
        return static_cast<underlying_type>(result->second->first.value);                                                            \
    }                                                      \
                                                           \
    [[nodiscard]] static inline underlying_type to_underlying(const char* name) {                                                    \
        return to_underlying(::std::string_view(name));    \
    }                                                      \
                                                           \
    [[nodiscard]] static inline underlying_type to_underlying(enum_type val) {                                                       \
        if ((_all_set & static_cast<underlying_type>(val)) != static_cast<underlying_type>(val)) {                                   \
            throw ::std::invalid_argument("Value included bits not defined to be set.");                                             \
        }                                                  \
        return static_cast<underlying_type>(val);          \
    }                                                      \
                                                           \
    [[nodiscard]] static inline underlying_type to_underlying(enum_name val) {                                                       \
        if ((_all_set & static_cast<underlying_type>(val)) != static_cast<underlying_type>(val)) {                                   \
            throw ::std::invalid_argument("Value included bits not defined to be set.");                                             \
        }                                                  \
        return static_cast<underlying_type>(val);          \
    }                                                      \
                                                           \
    static constexpr underlying_type _all_set = GLUINO_MAP_JOIN(__GLUINO_FLAGS_CONSTANT, _, |, __VA_ARGS__);                         \
};                                                         \
                                                           \
GLUINO_MAP_JOIN(__GLUINO_FLAGS_CONSTANT_DEF, enum_name, ;, __VA_ARGS__)
