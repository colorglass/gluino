#pragma once

#include <mutex>

#include "concepts.h"
#include "mix.h"

namespace gluino {
    /**
     * Defines a type that mixes a mutex into another type.
     *
     * <p>
     * This is a simple alias for <code>gluino::mixin&lt;T, Mutex&gt;</code>, with added template parameter validation
     * to ensure the second type meets the C++ lockable concept. It's utility is primarily to simplify code where you
     * require a container type and an associated lock specific to that container.
     * </p>
     *
     * @tparam T The base type in which to add locking.
     * @tparam Mutex The mutex type to mix in (defaults to <code>std::mutex</code>).
     */
    template <class T, ::gluino::basic_lockable Mutex = ::std::mutex>
    using synchronized = ::gluino::mix<T, Mutex>;
}
