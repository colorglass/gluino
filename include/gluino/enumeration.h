#pragma once

#include <optional>
#include <span>
#include <stdexcept>
#include <string_view>

#include "container.h"
#include "preprocessor.h"
#include "singleton.h"

#ifndef GLUINO_OVERRIDE_ENUM_MAP_TYPE
#include <unordered_map>
#define GLUINO_OVERRIDE_ENUM_MAP_TYPE ::std::unordered_map
#endif

namespace gluino {
    /**
     * A base class for Gluino enumerations.
     */
    class enumeration_traits {
    };

    /**
     * A type trait which matches to types which are Gluino enumerations.
     *
     * @tparam T The type to test.
     */
    template <class T>
    struct is_enumeration : public ::std::is_base_of<::gluino::enumeration_traits, T> {
    };

    /**
     * A type trait which matches to types which are Gluino enumerations.
     */
    template <class T>
    static constexpr bool is_enumeration_v = ::gluino::is_enumeration<T>::value;

    /**
     * A concept which matches to types which are Gluino enumerations.
     */
    template <class T>
    concept enumeration = ::gluino::is_enumeration_v<T> && requires(T& enumeration) {
        typename T::underlying_type;
        { enumeration.name() } -> ::std::same_as<::std::string_view>;
    };
}

namespace std {
    template <::gluino::enumeration T>
    [[nodiscard]] inline ::std::string to_string(T& enumeration) noexcept {
        return enumeration.name();
    }

    template <>
    struct hash<::gluino::enumeration_traits> {
        template <::gluino::enumeration T>
        [[nodiscard]] inline ::std::size_t operator()(const T& value) const noexcept {
            ::std::hash<typename T::underlying_type> h;
            return h(static_cast<typename T::underlying_type>(value));
        }
    };
}

namespace boost {
    template <::gluino::enumeration T>
    [[nodiscard]] inline ::std::size_t hash_value(const T& value) noexcept {
        ::std::hash<T> h;
        return h(value);
    }
}

#define __GLUINO_ENUM_MEMBER(pair, name) GLUINO_APPLY(GLUINO_FIRST, GLUINO_FLATTEN(pair)) = GLUINO_APPLY(GLUINO_REST, GLUINO_FLATTEN(pair))

#define __GLUINO_ENUM_CONSTANT_DECL(pair, name) static const name GLUINO_APPLY(GLUINO_FIRST, GLUINO_FLATTEN(pair));

#define __GLUINO_ENUM_CONSTANT_DEF(pair, name) inline constexpr name name::GLUINO_APPLY(GLUINO_FIRST, GLUINO_FLATTEN(pair)) = name(name::enum_type::GLUINO_APPLY(GLUINO_FIRST, GLUINO_FLATTEN(pair)), name::ignore_validation_tag_t{})

#define __GLUINO_ENUM_ENTRY(pair, name) entry = ::gluino::insert(_entries, ::std::make_pair(GLUINO_APPLY(GLUINO_FIRST, GLUINO_FLATTEN(pair)), GLUINO_APPLY(GLUINO_DEFER(GLUINO_STRING), GLUINO_APPLY(GLUINO_FIRST, GLUINO_FLATTEN(pair))))); \
::gluino::emplace(_enum_entries, GLUINO_APPLY(GLUINO_FIRST, GLUINO_FLATTEN(pair)));                                                                                                                                                          \
::gluino::emplace(_names, entry->second, entry);                                                                                                                                                                                             \
::gluino::emplace(_values, name::enum_type::GLUINO_APPLY(GLUINO_FIRST, GLUINO_FLATTEN(pair)), entry);

/**
 * Defines an enhanced enumeration.
 *
 * <p>
 * Enumerations are defined by providing the name of the enumeration type, it's base type (the integer type that
 * underlies it), and a sequence of one or more pairs of values (in parentheses) which define the name of the enum
 * value and it's underlying integer value. Implicit enum values are not allowed.
 * </p>
 *
 * <p>
 * Gluino enumerations can function in most respects like standard C++ enums, and provide an internal
 * <code>enum_type</code> type which is an actual enum. The Gluino type itself is not an enumeration, but rather defines
 * constants for each value. It is possible to reflect on these values to get their name, or to get an enum instance
 * from a string name. It is also possible to iterate over the possible enum values.
 * </p>
 */
#define gluino_enum(enum_name, base, ...) class enum_name \
: public ::gluino::enumeration_traits {                   \
public:                                                   \
    enum class enum_type : base {                         \
        GLUINO_MAP(__GLUINO_ENUM_MEMBER, enum_name, __VA_ARGS__) \
    };                                                    \
    using underlying_type = ::std::underlying_type_t<enum_type>; \
                                                          \
    constexpr enum_name() noexcept = default;             \
                                                          \
    template <class T>                                    \
    requires ::std::convertible_to<T, underlying_type>    \
    inline explicit enum_name(T val)                      \
            : value(static_cast<enum_type>(val)) {        \
        validate(static_cast<enum_type>(val));            \
    }                                                     \
                                                          \
    inline enum_name(enum_type val)                       \
            : value(val) {                                \
        validate(val);                                    \
    }                                                     \
                                                          \
    inline explicit enum_name(::std::string_view name) {  \
        auto& names = ::gluino::lazy_singleton<static_state>::value()._names; \
        auto result = names.find(name.data());            \
        if (result == names.end()) {                      \
            throw ::std::invalid_argument("member name is not defined on enumeration."); \
        }                                                 \
        *const_cast<enum_type*>(&value) = result->second->first.value;        \
    }                                                     \
                                                          \
    constexpr enum_name(const enum_name& other) noexcept  \
            : value(other.value) {                        \
    }                                                     \
                                                          \
    inline enum_name(enum_name&& other) noexcept {     \
        *const_cast<enum_type*>(&value) = other.value;    \
        *const_cast<enum_type*>(&other.value) = enum_type::GLUINO_APPLY(GLUINO_FIRST, GLUINO_FLATTEN(GLUINO_FIRST(__VA_ARGS__))); \
    }                                                     \
                                                          \
    [[nodiscard]] inline ::std::string_view name() const noexcept {           \
        auto& values = ::gluino::lazy_singleton<static_state>::value()._values;          \
        auto result = values.find(value);                 \
        if (result == values.end()) {                     \
            return "";                                    \
        }                                                 \
        return result->second->second;                    \
    }                                                     \
                                                          \
    [[nodiscard]] constexpr operator enum_type() const noexcept {\
        return value;                                     \
    }                                                     \
                                                          \
    template <class T>                                    \
    requires ::std::convertible_to<underlying_type, T>    \
    [[nodiscard]] constexpr explicit operator T() const noexcept {            \
        return static_cast<T>(value);                     \
    }                                                     \
                                                          \
    [[nodiscard]] inline typename ::std::span<const enum_name>::iterator current() const noexcept {                                     \
        auto& state = ::gluino::lazy_singleton<static_state>::value();        \
        return values().begin() + (state._values.find(value)->second - state._entries.begin()); \
    }                                                     \
                                                          \
    [[nodiscard]] inline ::std::optional<enum_name> next() const noexcept {   \
        auto iter = current() + 1;                        \
        if (iter == values().end()) {                     \
            return {};                                    \
        }                                                 \
        return *iter;                                     \
    }                                                     \
                                                          \
    [[nodiscard]] inline ::std::optional<enum_name> previous() const noexcept {       \
        auto iter = current();                            \
        if (iter == values().begin()) {                   \
            return {};                                    \
        }                                                 \
        --iter;                                           \
        return *iter;                                     \
    }                                                     \
                                                          \
    [[nodiscard]] constexpr bool operator==(enum_name other) const noexcept { \
        return value == other.value;                      \
    }                                                     \
                                                          \
    inline enum_name& operator=(const enum_name& other) noexcept {            \
        *const_cast<enum_type*>(&value) = other.value;    \
        return *this;                                     \
    }                                                     \
                                                          \
    inline enum_name& operator=(enum_name&& other) noexcept {    \
        *const_cast<enum_type*>(&value) = other.value;    \
        *const_cast<enum_type*>(&other.value) = enum_type::GLUINO_APPLY(GLUINO_FIRST, GLUINO_FLATTEN(GLUINO_FIRST(__VA_ARGS__))); \
        return *this;                                     \
    }                                                     \
                                                          \
    [[nodiscard]] static inline ::std::span<const enum_name> values() noexcept {         \
        return ::gluino::lazy_singleton<static_state>::value()._enum_entries; \
    }                                                     \
                                                          \
    GLUINO_MAP_CONCAT(__GLUINO_ENUM_CONSTANT_DECL, enum_name, __VA_ARGS__)    \
                                                          \
    const enum_type value{enum_type::GLUINO_APPLY(GLUINO_FIRST, GLUINO_FLATTEN(GLUINO_FIRST(__VA_ARGS__)))}; \
                                                          \
protected:                                                \
    struct ignore_validation_tag_t {                      \
    };                                                    \
                                                          \
    constexpr enum_name(enum_type val, ignore_validation_tag_t) noexcept      \
            : value(val) {                                \
    }                                                     \
                                                          \
    static inline void validate(enum_type val) {          \
        if (!::gluino::lazy_singleton<static_state>::value()._values.contains(val)) { \
            throw ::std::invalid_argument("No matching enum member value exists.");      \
        }                                                 \
    }                                                     \
                                                          \
private:                                                  \
    struct static_state {                                 \
        static_state() noexcept {                         \
            _entries.reserve(GLUINO_COUNT(__VA_ARGS__));  \
            _enum_entries.reserve(GLUINO_COUNT(__VA_ARGS__));    \
            ::gluino::reserve(_names, GLUINO_COUNT(__VA_ARGS__));\
            ::gluino::reserve(_values, GLUINO_COUNT(__VA_ARGS__));            \
            decltype(_entries)::iterator entry;           \
            GLUINO_MAP_CONCAT(__GLUINO_ENUM_ENTRY, enum_name, __VA_ARGS__)    \
        }                                                 \
                                                          \
        ::std::vector<::std::pair<enum_name, ::std::string_view>> _entries;   \
        ::std::vector<enum_name> _enum_entries;           \
        GLUINO_OVERRIDE_ENUM_MAP_TYPE<::std::string_view, decltype(_entries)::iterator> _names; \
        GLUINO_OVERRIDE_ENUM_MAP_TYPE<enum_type, decltype(_entries)::iterator> _values;       \
    };                                                    \
};                                                        \
                                                          \
GLUINO_MAP_JOIN(__GLUINO_ENUM_CONSTANT_DEF, enum_name, ;, __VA_ARGS__)
