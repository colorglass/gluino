#pragma once

#include <cstdint>
#include <optional>
#include <regex>
#include <sstream>
#include <string>
#include <variant>

#include "enumeration.h"

namespace gluino {
    /**
     * A value type for representing standard semantic version numbers.
     *
     * @tparam Char The character type to use for string elements of the semantic version.
     * @tparam Traits The character traits to use for the string elements of the semantic version.
     * @tparam Alloc The allocator to use for the string elements of the semantic version.
     */
    template <class Char, class Traits = ::std::char_traits<Char>, class Alloc = ::std::allocator<Char>>
    class basic_semantic_version {
    public:
        using part_type = uint16_t;
        using identifier_type = ::std::variant<part_type, ::std::basic_string<Char, Traits, Alloc>>;
        using build_type = ::std::optional<identifier_type>;

        inline basic_semantic_version(part_type major, part_type minor, part_type patch,
                                         ::std::vector<identifier_type>&& prerelease_identifiers,
                                         build_type build = {}) noexcept
                : _major(major), _minor(minor), _patch(patch),
                  _prerelease_identifiers(::std::move(prerelease_identifiers)), _build(build) {
        }

        inline basic_semantic_version(part_type major, part_type minor, part_type patch,
                                ::std::vector<identifier_type>& prerelease_identifiers,
                                build_type build = {})
                : _major(major), _minor(minor), _patch(patch),
                  _prerelease_identifiers(prerelease_identifiers), _build(build) {
        }

        inline explicit basic_semantic_version(part_type major = 0, part_type minor = 0, part_type patch = 0,
                                         ::std::vector<identifier_type> prerelease_identifiers = {},
                                         build_type build = {}) noexcept
                : _major(major), _minor(minor), _patch(patch),
                  _prerelease_identifiers(::std::move(prerelease_identifiers)), _build(build) {
        }

		inline explicit basic_semantic_version(::std::basic_string_view<Char, Traits> version) {
            static ::std::basic_regex<Char> pattern(R"(([0-9]+)\.([0-9]+)\.([0-9]+)(?:-([0-9]+|[a-z\-][a-z\-0-9]*)(?:(\.)([0-9]+|[a-z\-][a-z\-0-9]*))*)?(?:(\+)([0-9]+|[a-z\-][a-z\-0-9]*))?)");
            ::std::match_results<const Char*> match;
			if (!::std::regex_match(version.data(), match, pattern)) {
                throw ::std::invalid_argument("String does not match the semantic version format.");
            }
            _major = Parse<part_type>(match.str(1));
            _minor = Parse<part_type>(match.str(2));
            _patch = Parse<part_type>(match.str(3));
            bool is_build = false;
            for (::std::size_t i = 4; i < match.size(); ++i) {
                if (match.str(i) == "-") {
                    continue;
                }
                if (match.str(i) == "+") {
                    is_build = true;
                    continue;
                }
                if (is_build) {
                    // TODO:
                } else {
                    // TODO:
                }
            }
		}

        [[nodiscard]] inline part_type major() const noexcept {
            return _major;
        }

        [[nodiscard]] inline part_type minor() const noexcept {
            return _minor;
        }

        [[nodiscard]] inline part_type patch() const noexcept {
            return _patch;
        }

        [[nodiscard]] inline const ::std::vector<identifier_type> prerelease_identifiers() const noexcept {
            return _prerelease_identifiers;
        }

        [[nodiscard]] inline const build_type& build() const noexcept {
            return _build;
        }

        [[nodiscard]] ::std::strong_ordering operator<=>(const basic_semantic_version& other) const noexcept {
            if (major() < other.major()) {
                return ::std::strong_ordering::less;
            } else if (major() > other.major()) {
                return ::std::strong_ordering::greater;
            }

            if (minor() < other.minor()) {
                return ::std::strong_ordering::less;
            } else if (minor() > other.minor()) {
                return ::std::strong_ordering::greater;
            }

            if (patch() < other.patch()) {
                return ::std::strong_ordering::less;
            } else if (patch() > other.patch()) {
                return ::std::strong_ordering::greater;
            }

            return build() == other.build() ? ::std::strong_ordering::equal : ::std::strong_ordering::equivalent;
        }

    private:
        template <::std::integral T>
        T Parse(const ::std::string&& string) {
            ::std::basic_stringstream<Char, Traits, Alloc> stream;
            stream << string;
            stream.seekg(0);
            T out;
            stream >> out;
            return out;
        }

        part_type _major{0};
        part_type _minor{0};
        part_type _patch{0};
        ::std::vector<identifier_type> _prerelease_identifiers;
        build_type _build;
    };

    using semantic_version = ::gluino::basic_semantic_version<char>;
    using wsemantic_version = ::gluino::basic_semantic_version<wchar_t>;
    using u8semantic_version = ::gluino::basic_semantic_version<char8_t>;
    using u16semantic_version = ::gluino::basic_semantic_version<char16_t>;
    using u32semantic_version = ::gluino::basic_semantic_version<char32_t>;

    inline ::gluino::semantic_version operator "" _semver(const char* version) {
        return ::gluino::semantic_version(version);
    }
}

namespace std {
    template <class Char, class Traits, class Alloc>
    ::std::string to_string(const ::gluino::basic_semantic_version<Char, Traits, Alloc>& version) {
        ::std::stringstream stream;
        stream << version.major() << "." << version.minor() << "." << version.patch();
        if (!version.prerelease_identifiers().empty()) {
            stream << "-";
            bool first = true;
            for (auto& identifier : version.prerelease_identifiers()) {
                if (!first) {
                    stream << ".";
                }
                first = false;
                ::std::visit([&stream](auto&& value) {
                    stream << value;
                }, identifier);
            }
        }
        if (version.build().has_value()) {
            stream << "+";
            ::std::visit([&stream](auto&& value) {
                stream << value;
            }, version.build().value());
        }
        return stream.str();
    }
}
