#pragma once

#define GLUINO_COMMA() ,
#define GLUINO_NIL(...)
#define GLUINO_DEFER(x) x GLUINO_NIL()
#define GLUINO_NOP(...) __VA_ARGS__
#define GLUINO_PARENS() ()
#define __GLUINO_IS_EMPTY_STRING(macro) macro ## 1
#define GLUINO_IS_EMPTY_TEXT(macro) __GLUINO_IS_EMPTY_STRING(macro)
#define GLUINO_CONCAT(x, y) x ## y

#define GLUINO_APPLY(f, ...) f(__VA_ARGS__)
#define __GLUINO_EXPAND(arg) __GLUINO_EXPAND1(__GLUINO_EXPAND1(__GLUINO_EXPAND1(__GLUINO_EXPAND1(arg))))
#define __GLUINO_EXPAND1(arg) __GLUINO_EXPAND2(__GLUINO_EXPAND2(__GLUINO_EXPAND2(__GLUINO_EXPAND2(arg))))
#define __GLUINO_EXPAND2(arg) __GLUINO_EXPAND3(__GLUINO_EXPAND3(__GLUINO_EXPAND3(__GLUINO_EXPAND3(arg))))
#define __GLUINO_EXPAND3(arg) __GLUINO_EXPAND4(__GLUINO_EXPAND4(__GLUINO_EXPAND4(__GLUINO_EXPAND4(arg))))
#define __GLUINO_EXPAND4(arg) __GLUINO_EXPAND5(__GLUINO_EXPAND5(__GLUINO_EXPAND5(__GLUINO_EXPAND5(arg))))
#define __GLUINO_EXPAND5(arg) arg

#define GLUINO_STRING(str) #str
#define GLUINO_WSTRING(str) L ## GLUINO_STRING(str)
#define GLUINO_U8STRING(str) u8 ## GLUINO_STRING(str)
#define GLUINO_U16STRING(str) u ## GLUINO_STRING(str)
#define GLUINO_U32STRING(str) U ## GLUINO_STRING(str)

#define __GLUINO_FLATTEN(...) __VA_ARGS__
#define GLUINO_FLATTEN(x) __GLUINO_FLATTEN x

#define GLUINO_TRUE(x, y) x
#define GLUINO_FALSE(x, y) y
#define GLUINO_IF_ELSE(b, t, e) b(t, e)
#define GLUINO_IF(b, t) GLUINO_IF_ELSE(b, t,)
#define GLUINO_NOT(x) x(GLUINO_FALSE, GLUINO_TRUE)
#define __GLUINO_TRUE(x, y) GLUINO_TRUE(x, y)
#define __GLUINO_TRUE_NOT(x, y) GLUINO_FALSE(x, y)
#define GLUINO_IS_EMPTY(...) __GLUINO_TRUE ## __VA_OPT__(_NOT)

#define GLUINO_FIRST(x, ...) x
#define GLUINO_REST(x, ...) __VA_ARGS__

#define __GLUINO_MAP_00F(f, c, x, ...) f(x, c) __VA_OPT__(, __GLUINO_MAP_010(f, c, __VA_ARGS__))
#define __GLUINO_MAP_00E(f, c, x, ...) f(x, c) __VA_OPT__(, __GLUINO_MAP_00F(f, c, __VA_ARGS__))
#define __GLUINO_MAP_00D(f, c, x, ...) f(x, c) __VA_OPT__(, __GLUINO_MAP_00E(f, c, __VA_ARGS__))
#define __GLUINO_MAP_00C(f, c, x, ...) f(x, c) __VA_OPT__(, __GLUINO_MAP_00D(f, c, __VA_ARGS__))
#define __GLUINO_MAP_00B(f, c, x, ...) f(x, c) __VA_OPT__(, __GLUINO_MAP_00C(f, c, __VA_ARGS__))
#define __GLUINO_MAP_00A(f, c, x, ...) f(x, c) __VA_OPT__(, __GLUINO_MAP_00B(f, c, __VA_ARGS__))
#define __GLUINO_MAP_009(f, c, x, ...) f(x, c) __VA_OPT__(, __GLUINO_MAP_00A(f, c, __VA_ARGS__))
#define __GLUINO_MAP_008(f, c, x, ...) f(x, c) __VA_OPT__(, __GLUINO_MAP_009(f, c, __VA_ARGS__))
#define __GLUINO_MAP_007(f, c, x, ...) f(x, c) __VA_OPT__(, __GLUINO_MAP_008(f, c, __VA_ARGS__))
#define __GLUINO_MAP_006(f, c, x, ...) f(x, c) __VA_OPT__(, __GLUINO_MAP_007(f, c, __VA_ARGS__))
#define __GLUINO_MAP_005(f, c, x, ...) f(x, c) __VA_OPT__(, __GLUINO_MAP_006(f, c, __VA_ARGS__))
#define __GLUINO_MAP_004(f, c, x, ...) f(x, c) __VA_OPT__(, __GLUINO_MAP_005(f, c, __VA_ARGS__))
#define __GLUINO_MAP_003(f, c, x, ...) f(x, c) __VA_OPT__(, __GLUINO_MAP_004(f, c, __VA_ARGS__))
#define __GLUINO_MAP_002(f, c, x, ...) f(x, c) __VA_OPT__(, __GLUINO_MAP_003(f, c, __VA_ARGS__))
#define __GLUINO_MAP_001(f, c, x, ...) f(x, c) __VA_OPT__(, __GLUINO_MAP_002(f, c, __VA_ARGS__))
#define __GLUINO_MAP_000(f, c, x, ...) f(x, c) __VA_OPT__(, __GLUINO_MAP_001(f, c, __VA_ARGS__))
#define GLUINO_MAP(f, c, ...) __VA_OPT__(__GLUINO_MAP_000(f, c, __VA_ARGS__))

#define __GLUINO_MAP_JOIN_00F(f, c, d, x, ...) f(x, c) __VA_OPT__(d __GLUINO_MAP_JOIN_010(f, c, d, __VA_ARGS__))
#define __GLUINO_MAP_JOIN_00E(f, c, d, x, ...) f(x, c) __VA_OPT__(d __GLUINO_MAP_JOIN_00F(f, c, d, __VA_ARGS__))
#define __GLUINO_MAP_JOIN_00D(f, c, d, x, ...) f(x, c) __VA_OPT__(d __GLUINO_MAP_JOIN_00E(f, c, d, __VA_ARGS__))
#define __GLUINO_MAP_JOIN_00C(f, c, d, x, ...) f(x, c) __VA_OPT__(d __GLUINO_MAP_JOIN_00D(f, c, d, __VA_ARGS__))
#define __GLUINO_MAP_JOIN_00B(f, c, d, x, ...) f(x, c) __VA_OPT__(d __GLUINO_MAP_JOIN_00C(f, c, d, __VA_ARGS__))
#define __GLUINO_MAP_JOIN_00A(f, c, d, x, ...) f(x, c) __VA_OPT__(d __GLUINO_MAP_JOIN_00B(f, c, d, __VA_ARGS__))
#define __GLUINO_MAP_JOIN_009(f, c, d, x, ...) f(x, c) __VA_OPT__(d __GLUINO_MAP_JOIN_00A(f, c, d, __VA_ARGS__))
#define __GLUINO_MAP_JOIN_008(f, c, d, x, ...) f(x, c) __VA_OPT__(d __GLUINO_MAP_JOIN_009(f, c, d, __VA_ARGS__))
#define __GLUINO_MAP_JOIN_007(f, c, d, x, ...) f(x, c) __VA_OPT__(d __GLUINO_MAP_JOIN_008(f, c, d, __VA_ARGS__))
#define __GLUINO_MAP_JOIN_006(f, c, d, x, ...) f(x, c) __VA_OPT__(d __GLUINO_MAP_JOIN_007(f, c, d, __VA_ARGS__))
#define __GLUINO_MAP_JOIN_005(f, c, d, x, ...) f(x, c) __VA_OPT__(d __GLUINO_MAP_JOIN_006(f, c, d, __VA_ARGS__))
#define __GLUINO_MAP_JOIN_004(f, c, d, x, ...) f(x, c) __VA_OPT__(d __GLUINO_MAP_JOIN_005(f, c, d, __VA_ARGS__))
#define __GLUINO_MAP_JOIN_003(f, c, d, x, ...) f(x, c) __VA_OPT__(d __GLUINO_MAP_JOIN_004(f, c, d, __VA_ARGS__))
#define __GLUINO_MAP_JOIN_002(f, c, d, x, ...) f(x, c) __VA_OPT__(d __GLUINO_MAP_JOIN_003(f, c, d, __VA_ARGS__))
#define __GLUINO_MAP_JOIN_001(f, c, d, x, ...) f(x, c) __VA_OPT__(d __GLUINO_MAP_JOIN_002(f, c, d, __VA_ARGS__))
#define __GLUINO_MAP_JOIN_000(f, c, d, x, ...) f(x, c) __VA_OPT__(d __GLUINO_MAP_JOIN_001(f, c, d, __VA_ARGS__))
#define GLUINO_MAP_JOIN(f, c, d, ...) __VA_OPT__(__GLUINO_MAP_JOIN_000(f, c, d, __VA_ARGS__))
#define GLUINO_MAP_CONCAT(f, c, ...) GLUINO_MAP_JOIN(f, c,, __VA_ARGS__)

#define GLUINO_COUNT(...) (0 __VA_OPT__(+ __GLUINO_EXPAND(__GLUINO_COUNT_NEXT(__VA_ARGS__))))
#define __GLUINO_COUNT_NEXT(x, ...) 1 __VA_OPT__(+ __GLUINO_COUNT GLUINO_PARENS() (__VA_ARGS__))
#define __GLUINO_COUNT() __GLUINO_COUNT_NEXT
