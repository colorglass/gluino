#pragma once

#include <atomic>

#include "flags.h"
#include "sleep_policy.h"

namespace gluino {
    template <::gluino::sleep_policy SleepPolicy = ::gluino::null_sleep_policy,
            ::std::unsigned_integral Data = uint16_t>
    class shared_spin_mutex {
    public:
        inline shared_spin_mutex() noexcept = default;

        shared_spin_mutex(const shared_spin_mutex&) = delete;

        shared_spin_mutex(shared_spin_mutex&&) = delete;

        void lock() noexcept {
            SleepPolicy sleeper;
            while (true) {
                Data state = _data.load(::std::memory_order_relaxed);
                if (!(state & (writer_locked | ~(writer_locked | writer_waiting)))) {
                    if (_data.compare_exchange_strong(state, writer_locked)) {
                        return;
                    }
                    sleeper = SleepPolicy{};
                } else if (!(state & writer_waiting)) {
                    _data |= writer_waiting;
                }
                sleeper();
            }
        }

        void lock_shared() noexcept {
            SleepPolicy sleeper;
            while (!try_lock_shared()) {
                sleeper();
            }
        }

        [[nodiscard]] bool try_lock() noexcept {
            Data state = _data.load(::std::memory_order_relaxed);
            if (!(state & (writer_locked | ~(writer_locked | writer_waiting))) &&
                _data.compare_exchange_strong(state, writer_locked)) {
                return true;
            }
            return false;
        }

        [[nodiscard]] bool try_lock_shared() noexcept {
            Data state = _data.load(::std::memory_order_relaxed);
            if (!(state & (writer_waiting | writer_locked))) {
                auto result = _data.fetch_add(reader_unit);
                if (result & writer_locked) {
                    _data.fetch_add(static_cast<Data>(-reader_unit));
                    return false;
                }
                return true;
            }
            return false;
        }

        void unlock() noexcept {
            _data &= static_cast<Data>(~(writer_locked | writer_waiting));
        }

        void unlock_shared() noexcept {
            _data.fetch_add(static_cast<Data>(-reader_unit));
        }

        [[nodiscard]] static constexpr ::std::size_t max_readers() noexcept {
            return (~Data(0)) >> 2;
        }

    private:
        static constexpr Data writer_locked{1 << 0};
        static constexpr Data writer_waiting{1 << 1};
        static constexpr Data reader_unit{1 << 2};

        ::std::atomic<Data> _data;
    };
}
