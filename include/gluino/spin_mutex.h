#pragma once

#include <atomic>

#include "sleep_policy.h"

namespace gluino {
    /**
     * A mutex which performs mutual exclusion by having waiting threads spin rather than await notification.
     *
     * <p>
     * The spin mutex is best used when the wait time is expected to be very short and the number of threads waiting
     * expected to be few. The mutex can be customized to use a
     * </p>
     *
     * @tparam SleepPolicy A sleep policy which can handle sleeping on long spin cycles.
     */
    template <::gluino::sleep_policy SleepPolicy = ::gluino::null_sleep_policy>
    class spin_mutex {
    public:
        inline spin_mutex() noexcept = default;

        spin_mutex(const spin_mutex&) = delete;

        spin_mutex(spin_mutex&&) = delete;

        inline void lock() noexcept {
            SleepPolicy sleeper;
            while (_locked.exchange(true)) {
                sleeper();
            }
        }

        [[nodiscard]] inline bool try_lock() noexcept {
            return !_locked.exchange(true);
        }

        inline void unlock() noexcept {
            _locked.store(false, ::std::memory_order_release);
        }

    private:
        ::std::atomic<bool> _locked{false};
    };
}
