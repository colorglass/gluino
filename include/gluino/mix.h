#pragma once

namespace gluino {
    /**
     * Produces an ad-hoc class which combines a number of other classes and exposes all their public constructors.
     *
     * <p>
     * This class is a convenient way to combine multiple classes without needing to define a new class to inherit from
     * all of them. It should be used cautiously, as it exposes all the public constructors of its base classes. This
     * can result in unusable classes. It is best used to mix types which are default constructible and which have no
     * name conflicts.
     * </p>
     *
     * @tparam T The first type to mix in.
     * @tparam Rest The other types to mix in.
     */
    template <class T, class... Rest>
    class mix : public T, public mix<Rest...> {
    public:
        using T::T;
        using mix<Rest...>::mix;
    };

    template <class T>
    class mix<T> : public T {
    public:
        using T::T;
    };
}
