#pragma once

#include <functional>

#include "preprocessor.h"

#define __GLUINO_AUTORUN(counter) struct GLUINO_CONCAT(__gluino_autorun, counter) { \
    inline GLUINO_CONCAT(__gluino_autorun, counter)() noexcept;                     \
private:                                                                            \
    static GLUINO_CONCAT(__gluino_autorun, counter) _instance;                      \
};                                                                                  \
GLUINO_CONCAT(__gluino_autorun, counter) GLUINO_CONCAT(__gluino_autorun, counter)::_instance; \
GLUINO_CONCAT(__gluino_autorun, counter)::GLUINO_CONCAT(__gluino_autorun, counter)() noexcept

/**
 * Defines a code block to be executed when the program or module starts.
 *
 * <p>
 * Autorun blocks execute synchronously when the module loads. The macro effectively initializes a static variable that
 * is constructed via the code block, and so behaves with that semantics. <b>To avoid name conflicts at link time
 * autorun blocks should always be placed in an anonymous namespace, and only ever placed in source files and never
 * headers.</b>
 * </p>
 */
#define gluino_autorun __GLUINO_AUTORUN(__COUNTER__)
