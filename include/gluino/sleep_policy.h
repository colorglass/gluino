#pragma once

#include <concepts>
#include <limits>
#include <thread>

namespace gluino {
    /**
     * A concept for types which can act as a sleep policy for spin locks.
     */
    template <class T>
    concept sleep_policy = ::std::default_initializable<T> && requires(T& value) {
        { value() };
        { value = T{} };
    };

    /**
     * A backoff policy for spin locks, which cycles a given amount before sleeping, and can gradually reduce
     * the number of cycles it spins for subsequent failed attempts to acquire the lock.
     *
     * @tparam InitialYieldCycles The number of cycles the thread will spin before sleeping on the first failure to
     * acquire the lock.
     * @tparam MinYieldCycles The least number of spin cycles a thread will wait before sleeping.
     * @tparam BackoffFactor The factor by which the number of cycles a thread can spin before sleeping is reduced after
     * each sleep. For example, if set to 2, after each failed spin cycle, the thread will spin only half as much as the
     * previous cycle, until it reaches the minimum.
     */
    template <::std::size_t InitialYieldCycles = ::std::numeric_limits<::std::size_t>::max(),
            ::std::size_t MinYieldCycles = 1, uint64_t BackoffFactor = 2>
    class backoff_sleep_policy {
    public:
        inline void operator()() noexcept {
            if (--cycles == 0) {
                ::std::this_thread::yield();
                cycles = ::std::max(MinYieldCycles, last_cycles / BackoffFactor);
                last_cycles = cycles;
            }
        }

    private:
        ::std::size_t cycles{InitialYieldCycles};
        ::std::size_t last_cycles{InitialYieldCycles};
    };
    static_assert(::gluino::sleep_policy<::gluino::backoff_sleep_policy<>>);

    /**
     * A zero-overhead no-op sleep policy, which never allows a spin lock to sleep, only spin continuously until the
     * lock is acquired.
     */
    class null_sleep_policy {
    public:
        constexpr void operator()() noexcept {
        }
    };
    static_assert(::gluino::sleep_policy<::gluino::null_sleep_policy>);
}
