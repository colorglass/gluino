#pragma once

#include "type_traits.h"

namespace gluino {
    /**
     * A class which is a structural super-type of all types, i.e. it has absolutely no operations.
     *
     * <p>The <code>top</code> type can be utilized in concept definitions in certain cases to act as an any-type-here
     * indicator, where normally a specific template parameter would be required. This allows for a generic concept to
     * be defined which does not need to complete all template parameters of the types involved.</p>
     */
    class top {
    public:
        top() = delete;

        top(const top&) = delete;

        top(top&&) = delete;

        ~top() = delete;

        top& operator=(const top&) = delete;

        top& operator=(top&&) = delete;
    };


    // Type traits
    template <class T, class Base>
    concept subtype_of = ::gluino::is_subtype_v<Base, T>;

    template <class T>
    concept enum_type = ::std::is_enum_v<T>;

    template <class T>
    concept none = false;

    template <class T>
    concept non_void = !::std::is_void_v<T>;

    template <class T>
    concept referencable = ::gluino::is_referencable_v<T>;

    template <class T>
    concept arithmetic = ::std::is_arithmetic_v<T>;

    template <class T>
    concept signed_arithmetic = ::gluino::arithmetic<T> && ::std::is_signed_v<T>;

    template <class T>
    concept unsigned_arithmetic = ::gluino::arithmetic<T> && ::std::is_unsigned_v<T>;

    template <class T>
    concept pointer = ::std::is_pointer_v<T>;

    template <class T>
    concept reference = ::std::is_reference_v<T>;

    template <class T>
    concept smart_pointer = ::gluino::is_smart_pointer_v<T>;

    template <class T>
    concept pointer_like = ::gluino::is_pointer_like_v<T>;

    template <class T>
    concept indirection = ::gluino::is_indirection_v<T>;

    template <class T>
    concept indirection_like = ::gluino::is_indirection_like_v<T>;

    template <class T>
    concept primitive_char = ::gluino::is_primitive_char_v<T>;

    template <class T>
    concept char_like = ::gluino::is_char_v<T>;

    template <class T>
    concept polymorphic = ::std::is_polymorphic_v<::std::remove_cvref_t<T>>;

    template <class T>
    concept polymorphic_pointer = ::gluino::polymorphic<::std::remove_pointer_t<T>> && ::gluino::pointer<T>;

    template <class T>
    concept polymorphic_pointer_like = ::gluino::polymorphic<::gluino::remove_pointer_like_t<T>> &&
            ::gluino::pointer_like<T>;

    template <class T>
    concept polymorphic_reference = ::gluino::polymorphic<::std::remove_reference_t<T>> && ::gluino::reference<T>;

    template <class T>
    concept polymorphic_indirection = ::gluino::polymorphic<::gluino::remove_indirection_t<T>> &&
            ::gluino::indirection<T>;

    template <class T>
    concept polymorphic_indirection_like = ::gluino::polymorphic<::gluino::remove_indirection_like_t<T>> &&
                                           ::gluino::indirection_like<T>;

    template <class T>
    concept polymorphic_core = ::gluino::polymorphic<::gluino::core_type_t<T>>;

    template <class T>
    concept polymorphic_strict_core = ::gluino::polymorphic<::gluino::strict_core_type_t<T>>;

	template <class T>
	concept ultimately_polymorphic = ::gluino::is_ultimately_polymorphic_v<T>;

    template <class T, class U>
    concept related = ::std::derived_from<T, U> || ::std::derived_from<U, T>;


    // String concepts
    template <class T>
    concept string = ::gluino::is_string_v<T>;

    template <class T, class Char>
    concept string_of = ::gluino::is_string_of_v<T, Char>;

    template <class T>
    concept primitive_string = ::gluino::is_primitive_string_v<T>;

    template <class T>
    concept string_view = ::gluino::is_string_view_v<T>;

    template <class T, class Char>
    concept string_view_of = ::gluino::is_string_view_of_v<T, Char>;

    template <class T>
    concept primitive_string_view = ::gluino::is_primitive_string_view_v<T>;

    template <class T>
    concept string_like = ::gluino::is_string_like_v<T>;

    template <class T, class Char>
    concept string_like_of = ::gluino::is_string_like_of_v<T, Char>;

    template <class T>
    concept primitive_string_like = ::gluino::is_primitive_string_like_v<T>;

    template <class T>
    concept cstring = ::gluino::is_cstring_v<T>;

    template <class T, class Char>
    concept cstring_of = ::gluino::is_cstring_of_v<T, Char>;

    template <class T>
    concept primitive_cstring = ::gluino::is_primitive_cstring_v<T>;

    template <class T>
    concept any_string_type = ::gluino::is_any_string_type_v<T>;

    template <class T, class Char>
    concept any_string_type_of = ::gluino::is_any_string_type_of_v<T, Char>;

    template <class T>
    concept any_primitive_string_type = ::gluino::is_any_primitive_string_type_v<T>;


    // Data type sizes and selections.
    template <class T, class U>
    concept same_size = ::gluino::is_same_size_v<T, U>;

    template <class T, class U>
    concept different_size = ::gluino::is_different_size_v<T, U>;

    template <class T, class Than>
    concept larger = ::gluino::is_larger_v<T, Than>;

    template <class T, class Than>
    concept as_large_or_larger = ::gluino::is_as_large_or_larger_v<T, Than>;

    template <class T, class Than>
    concept smaller = ::gluino::is_smaller_v<T, Than>;

    template <class T, class Than>
    concept as_small_or_smaller = ::gluino::is_as_small_or_smaller_v<T, Than>;

    template <class T, class Than>
    concept wider = ::gluino::is_wider_v<T, Than>;

    template <class T, class Than>
    concept narrower = ::gluino::is_narrower_v<T, Than>;

    template <class T, class Than>
    concept as_wide_or_wider = ::gluino::is_as_wide_or_wider_v<T, Than>;

    template <class T, class Than>
    concept as_narrow_or_narrower = ::gluino::is_as_narrow_or_narrower_v<T, Than>;

    template <class T>
    concept widest = ::gluino::is_widest_v<T>;

    template <class T>
    concept narrowest = ::gluino::is_narrowest_v<T>;

    template <class T>
    concept standard = ::gluino::is_default_type_v<T>;

    template <class T>
    concept best = ::gluino::is_best_type_v<T>;


    // Concepts related to C++ standard requirements.
    template <class T, class Result, class... Args>
    concept function_like = requires(T& value, Args&& ... args) {
        { value(args...) } -> ::std::convertible_to<Result>;
    };

    template <class T, class Result, class... Args>
    concept function_object = ::std::is_object_v<T> && ::gluino::function_like<T, Result, Args...>;

    template <class T, class A>
    concept predicate = ::gluino::function_like<T, bool, A>;

    template <class T, class A>
    concept predicate_object = ::gluino::function_object<T, bool, A>;

    template <class T, class A, class B>
    concept binary_predicate = ::gluino::function_like<T, bool, A, B>;

    template <class T, class A, class B>
    concept binary_predicate_object = ::gluino::function_object<T, bool, A, B>;


    template <class T, class U>
    concept equality_comparer = ::std::copy_constructible<T> && ::std::destructible<T> &&
                                ::gluino::function_object<T, bool, U&, U&>;

    template <class T, class U>
    concept transparent_equality_comparer = ::std::copy_constructible<T> && ::std::destructible<T> &&
                                            (::gluino::function_object<T, bool, U&, ::gluino::top&> ||
                                             ::gluino::function_object<T, bool, ::gluino::top&, U&>) && requires() {
        typename T::is_transparent;
    };

    template <class T, class U>
    concept ordered_comparer = ::gluino::equality_comparer<T, U>;

    template <class T, class U>
    concept transparent_ordered_comparer = ::gluino::transparent_equality_comparer<T, U>;

    template <class T, class U>
    concept hasher = ::gluino::function_object<T, ::std::size_t, U> && ::std::copy_constructible<T> &&
                     ::std::destructible<T>;

    template <class T, class U>
    concept transparent_hash = ::gluino::hasher<T, U> && requires {
        typename T::is_transparent;
        typename T::transparent_key_equal;
    };

    template <class T>
    concept basic_lockable = requires(T& mutex) {
        { mutex.lock() };
        { mutex.unlock() };
    };

    template <class T>
    concept lockable = ::gluino::basic_lockable<T> && requires(T& mutex) {
        { mutex.try_lock() } -> ::std::convertible_to<bool>;
    };

    template <class T>
    concept shared_lockable = requires(T& mutex) {
        { mutex.lock_shared() };
        { mutex.try_lock_shared() } -> ::std::convertible_to<bool>;
    } && ::gluino::lockable<T>;

    template <class T>
    concept timed_lockable = requires(T& mutex, ::gluino::top time) {
        { mutex.try_lock_for(time) };
        { mutex.try_lock_until(time) };
    } && ::gluino::lockable<T>;

    template <class T>
    concept shared_timed_lockable = requires(T& mutex, ::gluino::top time) {
        { mutex.try_lock_shared_until(time) } -> ::std::convertible_to<bool>;
        { mutex.try_lock_shared_for(time) } -> ::std::convertible_to<bool>;
    } && ::gluino::shared_lockable<T> && ::gluino::timed_lockable<T>;


    // Type members
    template <class T>
    concept has_value_type = requires {
        typename T::value_type;
    };

    template <class T>
    concept has_element_type = requires {
        typename T::element_type;
    };

    template <class T>
    concept has_key_type = requires {
        typename T::key_type;
    };

    template <class T>
    concept has_traits_type = requires {
        typename T::traits_type;
    };

    template <class T>
    concept has_allocator_type = requires {
        typename T::allocator;
    };

    template <class T>
    concept has_key_equals_type = requires {
        typename T::key_equals;
    };

    template <class T>
    concept has_hasher_type = requires {
        typename T::hasher;
    };

    template <class T>
    concept has_value_compare_type = requires {
        typename T::value_compare;
    };

    template <class T>
    concept has_key_compare_type = requires {
        typename T::key_compare;
    };

    template <class T>
    concept has_size_type = requires {
        typename T::size_type;
    };


    // Containers
    template <class T>
    concept iterable = requires(T& value) {
        { value.begin() } -> ::std::convertible_to<typename T::iterator>;
        { value.cbegin() } -> ::std::convertible_to<typename T::const_iterator>;
    } && requires(const T& value) {
        { value.begin() } -> ::std::convertible_to<typename T::const_iterator>;
        { value.cbegin() } -> ::std::convertible_to<typename T::const_iterator>;
    };

    template <class T>
    concept reverse_iterable = requires(T& value) {
        { value.rbegin() } -> ::std::convertible_to<typename T::reverse_iterator>;
        { value.crbegin() } -> ::std::convertible_to<typename T::const_reverse_iterator>;
    } && requires(const T& value) {
        { value.rbegin() } -> ::std::convertible_to<typename T::const_reverse_iterator>;
        { value.crbegin() } -> ::std::convertible_to<typename T::const_reverse_iterator>;
    };

    template <class T>
    concept reservable = requires(T& value, ::std::size_t size) {
        { value.reserve(size) };
    };

    template <class T>
    concept resizable = requires(T& value, ::std::size_t size) {
        { value.resize(size) };
    };

    template <class T>
    concept rehashable = requires(T& value, ::std::size_t size) {
        { value.rehash(size) };
    };

    template <class T>
    concept clearable = requires(T& value) {
        { value.clear() };
    };

    template <class T>
    concept sized = requires(T& value) {
        { value.size() } -> ::std::convertible_to<::std::size_t>;
    };

    template <class T, class... Args>
    concept set_emplaceable = requires (T& object, Args... args) {
        { object.emplace(args...) };
    };

    template <class T, class... Args>
    concept back_emplaceable = requires (T& object, Args... args) {
        { object.emplace_back(args...) };
    };

    template <class T, class... Args>
    concept front_emplaceable = requires (T& object, Args... args) {
        { object.emplace_front(args...) };
    };

    template <class T, class... Args>
    concept set_insertable = requires (T& object, Args... args) {
        { object.insert({ args... }) } -> ::std::same_as<typename T::iterator>;
    } && ::gluino::iterable<T>;

    template <class T, class Arg>
    concept back_insertable = requires (T& object, Arg arg) {
        { object.insert(object.end(), arg) } -> ::std::same_as<typename T::iterator>;
    } && ::gluino::iterable<T>;

    template <class T, class Arg>
    concept front_insertable = requires (T& object, Arg arg) {
        { object.push_front(arg) };
    } && ::gluino::iterable<T>;

    template <class T>
    concept set_container = ::gluino::iterable<T> && ::gluino::set_emplaceable<T> && requires() {
        typename T::key_type;
    };

    template <class T>
    concept map_container = ::gluino::iterable<T> && ::gluino::set_emplaceable<T> && requires() {
        typename T::key_type;
        typename T::mapped_type;
    };

    template <class T>
    concept sequence_container = ::gluino::iterable<T> &&
        (::gluino::set_emplaceable<T> || ::gluino::front_emplaceable<T> || ::gluino::back_emplaceable<T>) &&
        requires() {
            typename T::value_type;
        } && !::gluino::set_container<T> && !::gluino::map_container<T>;


    // Random number engine concepts
    template <class T>
    concept seeder = requires(T& object) {
        typename T::result_type;
        { object() } -> ::std::convertible_to<typename T::result_type>;
    };

    template <class T, class Seed>
    concept seeded = requires(T& object, Seed& seed) {
        { object.seed(seed) };
    };

    template <class T>
    concept multistream_engine = requires(T& self) {
        typename T::stream_state;
        { self.set_stream(0u) };
        { self.stream() } -> ::std::same_as<typename T::stream_state>;
    };

    template <class T>
    concept backstepping_engine = requires(T& self) {
        { self.backstep(0u) };
    };
}
